%top{
  #include <cstdio>
  #include <iostream>
  #include <iomanip>
  using namespace std;
%}

%class{
  int ch, wd, nl;
 public:
  void results() {
    cout << setw(8) << nl << setw(8) << wd << setw(8) << ch << endl;
  }
%}

%init{
  ch = wd = nl = 0;
%}

%option unicode

line    \r?\n
word    (\w|\p{Punctuation})+

%%

{line}  ch += size(); ++nl;
{word}  ch += size(); ++wd;
.       ch += size();

%%

int main(int argc, char **argv) {
  FILE *fd = stdin;
  if (argc > 1 && (fd = fopen(argv[1], "r")) == NULL)
    exit(EXIT_FAILURE);
  // create a lexer that consumes a file or reads stdin
  Lexer lexer(fd);
  // here we go!
  lexer.lex();
  // display the results
  lexer.results();
  if (fd != stdin)
    fclose(fd);
  return 0;
}
