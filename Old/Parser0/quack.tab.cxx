// A Bison parser, made by GNU Bison 3.1.

// Skeleton implementation for Bison LALR(1) parsers in C++

// Copyright (C) 2002-2015, 2018 Free Software Foundation, Inc.

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

// As a special exception, you may create a larger work that contains
// part or all of the Bison parser skeleton and distribute that work
// under terms of your choice, so long as that work isn't itself a
// parser generator using the skeleton or a modified version thereof
// as a parser skeleton.  Alternatively, if you modify or redistribute
// the parser skeleton itself, you may (at your option) remove this
// special exception, which will cause the skeleton and the resulting
// Bison output files to be licensed under the GNU General Public
// License without this special exception.

// This special exception was added by the Free Software Foundation in
// version 2.2 of Bison.


// First part of user declarations.
#line 1 "quack.yxx" // lalr1.cc:407

#include <cstdio>
#include <iostream>
using namespace std;

extern int yylex();
extern int yyparse();
FILE *yyin;

void yyerror(const char *s);

#line 48 "quack.tab.cxx" // lalr1.cc:407

# ifndef YY_NULLPTR
#  if defined __cplusplus && 201103L <= __cplusplus
#   define YY_NULLPTR nullptr
#  else
#   define YY_NULLPTR 0
#  endif
# endif

#include "quack.tab.hxx"

// User implementation prologue.

#line 62 "quack.tab.cxx" // lalr1.cc:415
// Unqualified %code blocks.
#line 26 "quack.yxx" // lalr1.cc:416

  #include "lex.yy.h"
  #undef yylex
  #define yylex lexer.yylex

#line 70 "quack.tab.cxx" // lalr1.cc:416


#ifndef YY_
# if defined YYENABLE_NLS && YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> // FIXME: INFRINGES ON USER NAME SPACE.
#   define YY_(msgid) dgettext ("bison-runtime", msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(msgid) msgid
# endif
#endif

// Whether we are compiled with exception support.
#ifndef YY_EXCEPTIONS
# if defined __GNUC__ && !defined __EXCEPTIONS
#  define YY_EXCEPTIONS 0
# else
#  define YY_EXCEPTIONS 1
# endif
#endif

#define YYRHSLOC(Rhs, K) ((Rhs)[K].location)
/* YYLLOC_DEFAULT -- Set CURRENT to span from RHS[1] to RHS[N].
   If N is 0, then set CURRENT to the empty location which ends
   the previous symbol: RHS[0] (always defined).  */

# ifndef YYLLOC_DEFAULT
#  define YYLLOC_DEFAULT(Current, Rhs, N)                               \
    do                                                                  \
      if (N)                                                            \
        {                                                               \
          (Current).begin  = YYRHSLOC (Rhs, 1).begin;                   \
          (Current).end    = YYRHSLOC (Rhs, N).end;                     \
        }                                                               \
      else                                                              \
        {                                                               \
          (Current).begin = (Current).end = YYRHSLOC (Rhs, 0).end;      \
        }                                                               \
    while (/*CONSTCOND*/ false)
# endif


// Suppress unused-variable warnings by "using" E.
#define YYUSE(E) ((void) (E))

// Enable debugging if requested.
#if YYDEBUG

// A pseudo ostream that takes yydebug_ into account.
# define YYCDEBUG if (yydebug_) (*yycdebug_)

# define YY_SYMBOL_PRINT(Title, Symbol)         \
  do {                                          \
    if (yydebug_)                               \
    {                                           \
      *yycdebug_ << Title << ' ';               \
      yy_print_ (*yycdebug_, Symbol);           \
      *yycdebug_ << '\n';                       \
    }                                           \
  } while (false)

# define YY_REDUCE_PRINT(Rule)          \
  do {                                  \
    if (yydebug_)                       \
      yy_reduce_print_ (Rule);          \
  } while (false)

# define YY_STACK_PRINT()               \
  do {                                  \
    if (yydebug_)                       \
      yystack_print_ ();                \
  } while (false)

#else // !YYDEBUG

# define YYCDEBUG if (false) std::cerr
# define YY_SYMBOL_PRINT(Title, Symbol)  YYUSE (Symbol)
# define YY_REDUCE_PRINT(Rule)           static_cast<void> (0)
# define YY_STACK_PRINT()                static_cast<void> (0)

#endif // !YYDEBUG

#define yyerrok         (yyerrstatus_ = 0)
#define yyclearin       (yyla.clear ())

#define YYACCEPT        goto yyacceptlab
#define YYABORT         goto yyabortlab
#define YYERROR         goto yyerrorlab
#define YYRECOVERING()  (!!yyerrstatus_)


namespace yy {
#line 165 "quack.tab.cxx" // lalr1.cc:491

  /// Build a parser object.
  parser::parser (yy::Lexer& lexer_yyarg)
    :
#if YYDEBUG
      yydebug_ (false),
      yycdebug_ (&std::cerr),
#endif
      lexer (lexer_yyarg)
  {}

  parser::~parser ()
  {}


  /*---------------.
  | Symbol types.  |
  `---------------*/

  parser::syntax_error::syntax_error (const location_type& l, const std::string& m)
    : std::runtime_error (m)
    , location (l)
  {}

  // basic_symbol.
  template <typename Base>
  parser::basic_symbol<Base>::basic_symbol ()
    : value ()
    , location ()
  {}

  template <typename Base>
  parser::basic_symbol<Base>::basic_symbol (const basic_symbol& other)
    : Base (other)
    , value (other.value)
    , location (other.location)
  {
  }

  template <typename Base>
  parser::basic_symbol<Base>::basic_symbol (typename Base::kind_type t, const semantic_type& v, const location_type& l)
    : Base (t)
    , value (v)
    , location (l)
  {}


  /// Constructor for valueless symbols.
  template <typename Base>
  parser::basic_symbol<Base>::basic_symbol (typename Base::kind_type t, const location_type& l)
    : Base (t)
    , value ()
    , location (l)
  {}

  template <typename Base>
  parser::basic_symbol<Base>::~basic_symbol ()
  {
    clear ();
  }

  template <typename Base>
  void
  parser::basic_symbol<Base>::clear ()
  {
    Base::clear ();
  }

  template <typename Base>
  bool
  parser::basic_symbol<Base>::empty () const
  {
    return Base::type_get () == empty_symbol;
  }

  template <typename Base>
  void
  parser::basic_symbol<Base>::move (basic_symbol& s)
  {
    super_type::move (s);
    value = s.value;
    location = s.location;
  }

  // by_type.
  parser::by_type::by_type ()
    : type (empty_symbol)
  {}

  parser::by_type::by_type (const by_type& other)
    : type (other.type)
  {}

  parser::by_type::by_type (token_type t)
    : type (yytranslate_ (t))
  {}

  void
  parser::by_type::clear ()
  {
    type = empty_symbol;
  }

  void
  parser::by_type::move (by_type& that)
  {
    type = that.type;
    that.clear ();
  }

  int
  parser::by_type::type_get () const
  {
    return type;
  }


  // by_state.
  parser::by_state::by_state ()
    : state (empty_state)
  {}

  parser::by_state::by_state (const by_state& other)
    : state (other.state)
  {}

  void
  parser::by_state::clear ()
  {
    state = empty_state;
  }

  void
  parser::by_state::move (by_state& that)
  {
    state = that.state;
    that.clear ();
  }

  parser::by_state::by_state (state_type s)
    : state (s)
  {}

  parser::symbol_number_type
  parser::by_state::type_get () const
  {
    if (state == empty_state)
      return empty_symbol;
    else
      return yystos_[state];
  }

  parser::stack_symbol_type::stack_symbol_type ()
  {}

  parser::stack_symbol_type::stack_symbol_type (const stack_symbol_type& that)
    : super_type (that.state, that.location)
  {
    value = that.value;
  }

  parser::stack_symbol_type::stack_symbol_type (state_type s, symbol_type& that)
    : super_type (s, that.value, that.location)
  {
    // that is emptied.
    that.type = empty_symbol;
  }

  parser::stack_symbol_type&
  parser::stack_symbol_type::operator= (const stack_symbol_type& that)
  {
    state = that.state;
    value = that.value;
    location = that.location;
    return *this;
  }


  template <typename Base>
  void
  parser::yy_destroy_ (const char* yymsg, basic_symbol<Base>& yysym) const
  {
    if (yymsg)
      YY_SYMBOL_PRINT (yymsg, yysym);

    // User destructor.
    YYUSE (yysym.type_get ());
  }

#if YYDEBUG
  template <typename Base>
  void
  parser::yy_print_ (std::ostream& yyo,
                                     const basic_symbol<Base>& yysym) const
  {
    std::ostream& yyoutput = yyo;
    YYUSE (yyoutput);
    symbol_number_type yytype = yysym.type_get ();
    // Avoid a (spurious) G++ 4.8 warning about "array subscript is
    // below array bounds".
    if (yysym.empty ())
      std::abort ();
    yyo << (yytype < yyntokens_ ? "token" : "nterm")
        << ' ' << yytname_[yytype] << " ("
        << yysym.location << ": ";
    YYUSE (yytype);
    yyo << ')';
  }
#endif

  void
  parser::yypush_ (const char* m, state_type s, symbol_type& sym)
  {
    stack_symbol_type t (s, sym);
    yypush_ (m, t);
  }

  void
  parser::yypush_ (const char* m, stack_symbol_type& s)
  {
    if (m)
      YY_SYMBOL_PRINT (m, s);
    yystack_.push (s);
  }

  void
  parser::yypop_ (unsigned n)
  {
    yystack_.pop (n);
  }

#if YYDEBUG
  std::ostream&
  parser::debug_stream () const
  {
    return *yycdebug_;
  }

  void
  parser::set_debug_stream (std::ostream& o)
  {
    yycdebug_ = &o;
  }


  parser::debug_level_type
  parser::debug_level () const
  {
    return yydebug_;
  }

  void
  parser::set_debug_level (debug_level_type l)
  {
    yydebug_ = l;
  }
#endif // YYDEBUG

  parser::state_type
  parser::yy_lr_goto_state_ (state_type yystate, int yysym)
  {
    int yyr = yypgoto_[yysym - yyntokens_] + yystate;
    if (0 <= yyr && yyr <= yylast_ && yycheck_[yyr] == yystate)
      return yytable_[yyr];
    else
      return yydefgoto_[yysym - yyntokens_];
  }

  bool
  parser::yy_pact_value_is_default_ (int yyvalue)
  {
    return yyvalue == yypact_ninf_;
  }

  bool
  parser::yy_table_value_is_error_ (int yyvalue)
  {
    return yyvalue == yytable_ninf_;
  }

  int
  parser::parse ()
  {
    // State.
    int yyn;
    /// Length of the RHS of the rule being reduced.
    int yylen = 0;

    // Error handling.
    int yynerrs_ = 0;
    int yyerrstatus_ = 0;

    /// The lookahead symbol.
    symbol_type yyla;

    /// The locations where the error started and ended.
    stack_symbol_type yyerror_range[3];

    /// The return value of parse ().
    int yyresult;

#if YY_EXCEPTIONS
    try
#endif // YY_EXCEPTIONS
      {
    YYCDEBUG << "Starting parse\n";


    /* Initialize the stack.  The initial state will be set in
       yynewstate, since the latter expects the semantical and the
       location values to have been already stored, initialize these
       stacks with a primary value.  */
    yystack_.clear ();
    yypush_ (YY_NULLPTR, 0, yyla);

    // A new symbol was pushed on the stack.
  yynewstate:
    YYCDEBUG << "Entering state " << yystack_[0].state << '\n';

    // Accept?
    if (yystack_[0].state == yyfinal_)
      goto yyacceptlab;

    goto yybackup;

    // Backup.
  yybackup:
    // Try to take a decision without lookahead.
    yyn = yypact_[yystack_[0].state];
    if (yy_pact_value_is_default_ (yyn))
      goto yydefault;

    // Read a lookahead token.
    if (yyla.empty ())
      {
        YYCDEBUG << "Reading a token: ";
#if YY_EXCEPTIONS
        try
#endif // YY_EXCEPTIONS
          {
            yyla.type = yytranslate_ (yylex (&yyla.value, &yyla.location));
          }
#if YY_EXCEPTIONS
        catch (const syntax_error& yyexc)
          {
            error (yyexc);
            goto yyerrlab1;
          }
#endif // YY_EXCEPTIONS
      }
    YY_SYMBOL_PRINT ("Next token is", yyla);

    /* If the proper action on seeing token YYLA.TYPE is to reduce or
       to detect an error, take that action.  */
    yyn += yyla.type_get ();
    if (yyn < 0 || yylast_ < yyn || yycheck_[yyn] != yyla.type_get ())
      goto yydefault;

    // Reduce or error.
    yyn = yytable_[yyn];
    if (yyn <= 0)
      {
        if (yy_table_value_is_error_ (yyn))
          goto yyerrlab;
        yyn = -yyn;
        goto yyreduce;
      }

    // Count tokens shifted since error; after three, turn off error status.
    if (yyerrstatus_)
      --yyerrstatus_;

    // Shift the lookahead token.
    yypush_ ("Shifting", yyn, yyla);
    goto yynewstate;

  /*-----------------------------------------------------------.
  | yydefault -- do the default action for the current state.  |
  `-----------------------------------------------------------*/
  yydefault:
    yyn = yydefact_[yystack_[0].state];
    if (yyn == 0)
      goto yyerrlab;
    goto yyreduce;

  /*-----------------------------.
  | yyreduce -- Do a reduction.  |
  `-----------------------------*/
  yyreduce:
    yylen = yyr2_[yyn];
    {
      stack_symbol_type yylhs;
      yylhs.state = yy_lr_goto_state_ (yystack_[yylen].state, yyr1_[yyn]);
      /* If YYLEN is nonzero, implement the default value of the
         action: '$$ = $1'.  Otherwise, use the top of the stack.

         Otherwise, the following line sets YYLHS.VALUE to garbage.
         This behavior is undocumented and Bison users should not rely
         upon it.  */
      if (yylen)
        yylhs.value = yystack_[yylen - 1].value;
      else
        yylhs.value = yystack_[0].value;

      // Default location.
      {
        slice<stack_symbol_type, stack_type> slice (yystack_, yylen);
        YYLLOC_DEFAULT (yylhs.location, slice, yylen);
        yyerror_range[1].location = yylhs.location;
      }

      // Perform the reduction.
      YY_REDUCE_PRINT (yyn);
#if YY_EXCEPTIONS
      try
#endif // YY_EXCEPTIONS
        {
          switch (yyn)
            {
  case 2:
#line 51 "quack.yxx" // lalr1.cc:870
    { cout << "bison found an empty file." << endl; }
#line 588 "quack.tab.cxx" // lalr1.cc:870
    break;


#line 592 "quack.tab.cxx" // lalr1.cc:870
            default:
              break;
            }
        }
#if YY_EXCEPTIONS
      catch (const syntax_error& yyexc)
        {
          error (yyexc);
          YYERROR;
        }
#endif // YY_EXCEPTIONS
      YY_SYMBOL_PRINT ("-> $$ =", yylhs);
      yypop_ (yylen);
      yylen = 0;
      YY_STACK_PRINT ();

      // Shift the result of the reduction.
      yypush_ (YY_NULLPTR, yylhs);
    }
    goto yynewstate;

  /*--------------------------------------.
  | yyerrlab -- here on detecting error.  |
  `--------------------------------------*/
  yyerrlab:
    // If not already recovering from an error, report this error.
    if (!yyerrstatus_)
      {
        ++yynerrs_;
        error (yyla.location, yysyntax_error_ (yystack_[0].state, yyla));
      }


    yyerror_range[1].location = yyla.location;
    if (yyerrstatus_ == 3)
      {
        /* If just tried and failed to reuse lookahead token after an
           error, discard it.  */

        // Return failure if at end of input.
        if (yyla.type_get () == yyeof_)
          YYABORT;
        else if (!yyla.empty ())
          {
            yy_destroy_ ("Error: discarding", yyla);
            yyla.clear ();
          }
      }

    // Else will try to reuse lookahead token after shifting the error token.
    goto yyerrlab1;


  /*---------------------------------------------------.
  | yyerrorlab -- error raised explicitly by YYERROR.  |
  `---------------------------------------------------*/
  yyerrorlab:

    /* Pacify compilers like GCC when the user code never invokes
       YYERROR and the label yyerrorlab therefore never appears in user
       code.  */
    if (false)
      goto yyerrorlab;
    /* Do not reclaim the symbols of the rule whose action triggered
       this YYERROR.  */
    yypop_ (yylen);
    yylen = 0;
    goto yyerrlab1;

  /*-------------------------------------------------------------.
  | yyerrlab1 -- common code for both syntax error and YYERROR.  |
  `-------------------------------------------------------------*/
  yyerrlab1:
    yyerrstatus_ = 3;   // Each real token shifted decrements this.
    {
      stack_symbol_type error_token;
      for (;;)
        {
          yyn = yypact_[yystack_[0].state];
          if (!yy_pact_value_is_default_ (yyn))
            {
              yyn += yyterror_;
              if (0 <= yyn && yyn <= yylast_ && yycheck_[yyn] == yyterror_)
                {
                  yyn = yytable_[yyn];
                  if (0 < yyn)
                    break;
                }
            }

          // Pop the current state because it cannot handle the error token.
          if (yystack_.size () == 1)
            YYABORT;

          yyerror_range[1].location = yystack_[0].location;
          yy_destroy_ ("Error: popping", yystack_[0]);
          yypop_ ();
          YY_STACK_PRINT ();
        }

      yyerror_range[2].location = yyla.location;
      YYLLOC_DEFAULT (error_token.location, yyerror_range, 2);

      // Shift the error token.
      error_token.state = yyn;
      yypush_ ("Shifting", error_token);
    }
    goto yynewstate;

    // Accept.
  yyacceptlab:
    yyresult = 0;
    goto yyreturn;

    // Abort.
  yyabortlab:
    yyresult = 1;
    goto yyreturn;

  yyreturn:
    if (!yyla.empty ())
      yy_destroy_ ("Cleanup: discarding lookahead", yyla);

    /* Do not reclaim the symbols of the rule whose action triggered
       this YYABORT or YYACCEPT.  */
    yypop_ (yylen);
    while (1 < yystack_.size ())
      {
        yy_destroy_ ("Cleanup: popping", yystack_[0]);
        yypop_ ();
      }

    return yyresult;
  }
#if YY_EXCEPTIONS
    catch (...)
      {
        YYCDEBUG << "Exception caught: cleaning lookahead and stack\n";
        // Do not try to display the values of the reclaimed symbols,
        // as their printers might throw an exception.
        if (!yyla.empty ())
          yy_destroy_ (YY_NULLPTR, yyla);

        while (1 < yystack_.size ())
          {
            yy_destroy_ (YY_NULLPTR, yystack_[0]);
            yypop_ ();
          }
        throw;
      }
#endif // YY_EXCEPTIONS
  }

  void
  parser::error (const syntax_error& yyexc)
  {
    error (yyexc.location, yyexc.what ());
  }

  // Generate an error message.
  std::string
  parser::yysyntax_error_ (state_type, const symbol_type&) const
  {
    return YY_("syntax error");
  }


  const signed char parser::yypact_ninf_ = -34;

  const signed char parser::yytable_ninf_ = -1;

  const short
  parser::yypact_[] =
  {
     205,    54,    54,   140,   -14,    54,    54,   -34,    54,    -2,
     -34,   -34,    54,    54,    17,   205,    15,    73,   205,    -8,
     -34,   176,   -34,   159,   -34,    81,    19,   176,   176,   281,
     176,   287,   170,   176,   -34,   -34,   223,    54,    54,    54,
      54,    54,    54,    54,    54,    54,    54,    20,   -34,   -34,
      21,    54,   -12,   -34,    22,   281,   281,   -34,   281,   -34,
     176,   -23,   -34,    37,    38,   223,   223,   176,   176,   176,
     176,   176,   176,   176,   176,   176,   176,    36,    28,   111,
      40,   -34,     4,    41,    43,    42,   -34,    54,    39,   -34,
     -34,   -34,   298,    54,   -34,    51,    50,   -34,    65,    75,
      72,   176,    22,   -34,     9,   120,   -34,    76,   -34,    77,
     -34,    66,    82,   -34,   -34,   -34,   -34,    78,     3,   -34,
     -34,   103,   241,   101,   -34,   -34,   261,   -34
  };

  const unsigned char
  parser::yydefact_[] =
  {
       2,     0,     0,     0,     0,     0,     0,    18,     0,    50,
      21,    20,     0,     0,     0,     2,     0,     0,     2,    22,
      49,    35,    22,     0,    47,     0,     0,    18,    18,    42,
      18,     0,     0,    27,     1,     3,    13,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,    46,     4,
       0,     0,     0,    48,     9,    43,    40,    19,    41,    38,
      52,     0,    28,     0,     0,    13,    13,    33,    34,    29,
      32,    30,    23,    24,    25,    26,    31,    51,     0,     0,
       0,    54,     0,     0,     0,    10,    39,     0,     0,     5,
      15,    14,     0,     0,    44,     0,     0,    55,     0,     7,
       0,    53,     9,    36,     0,     0,    56,     0,    11,     0,
       6,     0,     0,    37,    45,    57,     8,     0,     0,    12,
      18,     0,     0,     0,    16,    18,     0,    17
  };

  const signed char
  parser::yypgoto_[] =
  {
     -34,    16,   -34,   -34,   -34,    29,   -34,   -33,   -34,   -26,
       8,     7,     0,    44,   -34,   -34
  };

  const signed char
  parser::yydefgoto_[] =
  {
      -1,    14,    15,    16,   110,    84,    85,    64,    65,    29,
      17,    57,    22,    61,    20,    82
  };

  const unsigned char
  parser::yytable_[] =
  {
      19,    55,    56,    86,    58,    87,    26,    18,    80,    21,
      23,    25,    81,    27,    28,    19,    30,    34,    19,    50,
      32,    33,    18,    31,    96,    18,   120,    51,    97,    19,
     121,    35,    90,    91,    49,   113,    19,    87,    36,    60,
      77,    78,    83,    66,    54,    67,    68,    69,    70,    71,
      72,    73,    74,    75,    76,    19,    19,    88,    19,    79,
       1,    92,    89,    93,   102,    19,    19,    95,    98,    99,
     100,   106,    66,    66,     9,    10,    11,   107,    37,    12,
      38,    39,    40,    41,    13,   108,    37,   109,    38,    39,
      40,    41,   111,   117,   122,   101,   115,   116,   119,   126,
      60,   105,    42,    43,    44,    45,    46,    47,   118,    48,
      42,    43,    44,    45,    46,    47,    37,    53,    38,    39,
      40,    41,    19,   123,   125,    37,    19,    38,    39,    40,
      41,   112,     0,     0,     0,     0,   104,     0,     0,     0,
      42,    43,    44,    45,    46,    47,     1,    94,     0,    42,
      43,    44,    45,    46,    47,     0,   114,     0,     0,     0,
       9,    10,    11,     0,    37,    12,    38,    39,    40,    41,
      13,     0,     0,     0,     0,    37,    24,    38,    39,    40,
      41,    37,    52,    38,    39,    40,    41,     0,    42,    43,
      44,    45,    46,    47,     0,     0,    62,     0,     0,    42,
      43,    44,    45,    46,    47,    42,    43,    44,    45,    46,
      47,     1,     0,     0,     0,     0,     2,     0,     3,     4,
       0,     5,     6,     7,     8,     9,    10,    11,     0,     1,
      12,     0,     0,     0,     2,    13,     3,     0,    63,     5,
       6,     7,     8,     9,    10,    11,     0,     1,    12,     0,
       0,     0,     2,    13,     3,     0,     0,     5,     6,     7,
       8,     9,    10,    11,     0,   124,    12,     1,     0,     0,
       0,    13,     2,     0,     3,     0,     0,     5,     6,     7,
       8,     9,    10,    11,     0,   127,    12,     1,     0,     0,
       0,    13,     2,     1,     3,     0,     0,     5,     6,     7,
       8,     9,    10,    11,     1,     0,    12,     9,    10,    11,
       0,    13,    12,    59,     0,     0,     0,    13,     9,    10,
      11,     0,     0,    12,   103,     0,     0,     0,    13
  };

  const signed char
  parser::yycheck_[] =
  {
       0,    27,    28,    26,    30,    28,    20,     0,    20,     1,
       2,     3,    24,     5,     6,    15,     8,     0,    18,    27,
      12,    13,    15,    25,    20,    18,    23,    35,    24,    29,
      27,    15,    65,    66,    18,    26,    36,    28,    23,    31,
      20,    20,    20,    36,    25,    37,    38,    39,    40,    41,
      42,    43,    44,    45,    46,    55,    56,    20,    58,    51,
       6,    25,    24,    35,    25,    65,    66,    27,    27,    26,
      28,    20,    65,    66,    20,    21,    22,    27,     5,    25,
       7,     8,     9,    10,    30,    20,     5,    12,     7,     8,
       9,    10,    20,    27,   120,    87,    20,    20,    20,   125,
      92,    93,    29,    30,    31,    32,    33,    34,    26,    36,
      29,    30,    31,    32,    33,    34,     5,    36,     7,     8,
       9,    10,   122,    20,    23,     5,   126,     7,     8,     9,
      10,   102,    -1,    -1,    -1,    -1,    92,    -1,    -1,    -1,
      29,    30,    31,    32,    33,    34,     6,    36,    -1,    29,
      30,    31,    32,    33,    34,    -1,    36,    -1,    -1,    -1,
      20,    21,    22,    -1,     5,    25,     7,     8,     9,    10,
      30,    -1,    -1,    -1,    -1,     5,    36,     7,     8,     9,
      10,     5,    23,     7,     8,     9,    10,    -1,    29,    30,
      31,    32,    33,    34,    -1,    -1,    26,    -1,    -1,    29,
      30,    31,    32,    33,    34,    29,    30,    31,    32,    33,
      34,     6,    -1,    -1,    -1,    -1,    11,    -1,    13,    14,
      -1,    16,    17,    18,    19,    20,    21,    22,    -1,     6,
      25,    -1,    -1,    -1,    11,    30,    13,    -1,    15,    16,
      17,    18,    19,    20,    21,    22,    -1,     6,    25,    -1,
      -1,    -1,    11,    30,    13,    -1,    -1,    16,    17,    18,
      19,    20,    21,    22,    -1,    24,    25,     6,    -1,    -1,
      -1,    30,    11,    -1,    13,    -1,    -1,    16,    17,    18,
      19,    20,    21,    22,    -1,    24,    25,     6,    -1,    -1,
      -1,    30,    11,     6,    13,    -1,    -1,    16,    17,    18,
      19,    20,    21,    22,     6,    -1,    25,    20,    21,    22,
      -1,    30,    25,    26,    -1,    -1,    -1,    30,    20,    21,
      22,    -1,    -1,    25,    26,    -1,    -1,    -1,    30
  };

  const unsigned char
  parser::yystos_[] =
  {
       0,     6,    11,    13,    14,    16,    17,    18,    19,    20,
      21,    22,    25,    30,    38,    39,    40,    47,    48,    49,
      51,    47,    49,    47,    36,    47,    20,    47,    47,    46,
      47,    25,    47,    47,     0,    38,    23,     5,     7,     8,
       9,    10,    29,    30,    31,    32,    33,    34,    36,    38,
      27,    35,    23,    36,    25,    46,    46,    48,    46,    26,
      47,    50,    26,    15,    44,    45,    48,    47,    47,    47,
      47,    47,    47,    47,    47,    47,    47,    20,    20,    47,
      20,    24,    52,    20,    42,    43,    26,    28,    20,    24,
      44,    44,    25,    35,    36,    27,    20,    24,    27,    26,
      28,    47,    25,    26,    50,    47,    20,    27,    20,    12,
      41,    20,    42,    26,    36,    20,    20,    27,    26,    20,
      23,    27,    46,    20,    24,    23,    46,    24
  };

  const unsigned char
  parser::yyr1_[] =
  {
       0,    37,    38,    38,    38,    39,    40,    41,    41,    42,
      42,    43,    43,    44,    44,    44,    45,    45,    46,    46,
      47,    47,    47,    47,    47,    47,    47,    47,    47,    47,
      47,    47,    47,    47,    47,    47,    47,    47,    47,    47,
      48,    48,    48,    48,    48,    48,    48,    48,    48,    48,
      49,    49,    50,    50,    51,    51,    52,    52
  };

  const unsigned char
  parser::yyr2_[] =
  {
       0,     2,     0,     2,     2,     4,     6,     0,     2,     0,
       1,     3,     5,     0,     2,     2,     8,    10,     0,     2,
       1,     1,     1,     3,     3,     3,     3,     2,     3,     3,
       3,     3,     3,     3,     3,     2,     5,     6,     3,     4,
       3,     3,     2,     3,     4,     6,     2,     2,     3,     1,
       1,     3,     1,     3,     4,     5,     3,     4
  };


#if YYDEBUG
  // YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
  // First, the terminals, then, starting at \a yyntokens_, nonterminals.
  const char*
  const parser::yytname_[] =
  {
  "$end", "error", "$undefined", "CONST_NUMBER", "CONST_STRING", "AND",
  "NOT", "OR", "EQUALS", "ATLEAST", "ATMOST", "TYPECASE", "EXTENDS",
  "RETURN", "CLASS", "DEF", "WHILE", "IF", "ELSE", "ELIF", "IDENT",
  "INT_LIT", "STRING_LIT", "'{'", "'}'", "'('", "')'", "':'", "','", "'+'",
  "'-'", "'*'", "'/'", "'<'", "'.'", "'='", "';'", "$accept", "quack",
  "class_", "class_signature", "options", "formal_args", "args",
  "class_body", "method", "statement_block", "r_expr", "statement",
  "l_expr", "actual_args", "typecase_", "type_alternative", YY_NULLPTR
  };


  const unsigned char
  parser::yyrline_[] =
  {
       0,    51,    51,    52,    53,    57,    61,    65,    66,    70,
      71,    75,    76,    80,    81,    82,    86,    87,    91,    92,
      96,    97,    98,    99,   100,   101,   102,   103,   104,   105,
     106,   107,   108,   109,   110,   111,   112,   113,   114,   115,
     119,   120,   121,   122,   123,   124,   125,   126,   127,   128,
     132,   133,   137,   138,   142,   143,   147,   148
  };

  // Print the state stack on the debug stream.
  void
  parser::yystack_print_ ()
  {
    *yycdebug_ << "Stack now";
    for (stack_type::const_iterator
           i = yystack_.begin (),
           i_end = yystack_.end ();
         i != i_end; ++i)
      *yycdebug_ << ' ' << i->state;
    *yycdebug_ << '\n';
  }

  // Report on the debug stream that the rule \a yyrule is going to be reduced.
  void
  parser::yy_reduce_print_ (int yyrule)
  {
    unsigned yylno = yyrline_[yyrule];
    int yynrhs = yyr2_[yyrule];
    // Print the symbols being reduced, and their result.
    *yycdebug_ << "Reducing stack by rule " << yyrule - 1
               << " (line " << yylno << "):\n";
    // The symbols being reduced.
    for (int yyi = 0; yyi < yynrhs; yyi++)
      YY_SYMBOL_PRINT ("   $" << yyi + 1 << " =",
                       yystack_[(yynrhs) - (yyi + 1)]);
  }
#endif // YYDEBUG

  // Symbol number corresponding to token number t.
  parser::token_number_type
  parser::yytranslate_ (int t)
  {
    static
    const token_number_type
    translate_table[] =
    {
     0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
      25,    26,    31,    29,    28,    30,    34,    32,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,    27,    36,
      33,    35,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,    23,     2,    24,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22
    };
    const unsigned user_token_number_max_ = 277;
    const token_number_type undef_token_ = 2;

    if (static_cast<int> (t) <= yyeof_)
      return yyeof_;
    else if (static_cast<unsigned> (t) <= user_token_number_max_)
      return translate_table[t];
    else
      return undef_token_;
  }


} // yy
#line 1038 "quack.tab.cxx" // lalr1.cc:1181
#line 151 "quack.yxx" // lalr1.cc:1182


int main(int argc, char **argv) {
  ++argv, --argc;

  if(argc > 0) {
    yyin = fopen(argv[0], "r");
  } else {
    yyin = stdin;
  }

  yyparse();
}

void yyerror(const char *s) {
  printf("Error: %s\n", s);
  exit(-1);
}

void yy::parser::error(const location_type& loc, const std::string& msg)
{
  std::cerr << msg << " at " << loc << std::endl;
}
