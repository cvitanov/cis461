// Symbol Table for visitor objects (Type checking, code gen, etc.)

#ifndef SYMTAB_H
#define SYMTAB_H

#define PROGRAM "Program"

#include <unordered_map>
#include <vector>
#include <string>

#define INT "Int"
#define STR "String"
#define OBJ "Obj"
#define BOOL "Boolean"
#define NOTHING "Nothing"
#define VOID "VOID" // for unassigned types in typechecker
#define FAIL "FAIL" // keeps track of where typechecker fails

// datatypes for symbol entries
enum SymbolType { IDENT, METHOD, CLASS, NONE };


// data for symtab (a class hierarchy)
class SymData {
public:
	SymData* parent; // pointer to the method's parent
	std::string type = "Obj";
	std::string name;
	SymbolType symtype = NONE;
	virtual void addArg(std::string id, std::string type) = 0;
};

class identData: public SymData {
public:
	
	void addArg(std::string id, std::string type) {}
	
	identData(std::string name, std::string type) {
		this->name = name;
		this->type = type;
		this->symtype = IDENT;
	}
	
	identData(std::string name, std::string type, SymData* parent) {
		this->name = name;
		this->type = type;
		this->parent = parent;
		this->symtype = IDENT;
	}

};

class methodData: public SymData {
public:
	
	std::vector<std::vector<std::string> > args; // arguments as a vector of vectors (inner vector is ["id","type"])
	int nArgs = 0;
	std::unordered_map<std::string, identData*> vars;
	int nVars = 0;
	
	// initialize symbol table entry for method
	methodData(std::string name, std::string return_type, SymData* parent) {
		this->name = name;
		this->type = return_type;
		this->parent = parent;
		this->symtype = METHOD;
	}
	
	// add an argument for this method
	void addArg(std::string id, std::string type) {
		std::vector<std::string> theArg;
		theArg.push_back(id);
		theArg.push_back(type);
		args.push_back(theArg); // add this argument to method data
		nArgs++;
		return;
	}
	
	// add an instance variable
	void addVar(identData* v) {
		this->vars.insert(std::make_pair(v->name,v));
		nVars++;
		return;
	}

};

class classData: public SymData {
public:
	std::string type = "Obj"; // superclass (defaults to type Obj)
	std::vector<std::vector<std::string> > args; // arguments as a vector of vectors (inner vector is ["id","type"])
	int nArgs = 0;
	std::vector<methodData> methods;
	int nMethods = 0;
	std::unordered_map<std::string, classData*> inherited_methods;
	std::unordered_map<std::string, identData*> vars;
	int nVars = 0;
	std::vector<std::vector<std::string> > typealts; // for each typealt, store name, type AND return val
	int nAlts = 0;
	
	// initialize class
	classData(std::string name, std::string type) {
		this->name = name;
		this->type = type;
		this->symtype = CLASS;
	}
	
	// add an argument for this method
	void addArg(std::string id, std::string type) {
		std::vector<std::string> theArg;
		theArg.push_back(id);
		theArg.push_back(type);
		args.push_back(theArg); // add this argument to method data
		nArgs++;
		return;
	}
	
	// add a method
	void addMethod(methodData& m) {
		methods.push_back(m);
		nMethods++;
		return;
	}

	// add an instance variable
	void addVar(identData* v) {
		this->vars.insert(std::make_pair(v->name,v));
		nVars++;
		return;
	}
	
	// add a type alternative
	void addTypealt(std::string id, std::string type, std::string return_val) {
		std::vector<std::string> theAlt;
		theAlt.push_back(id);
		theAlt.push_back(type);
		theAlt.push_back(return_val);
		typealts.push_back(theAlt);
		nAlts++;
		return;
	}
	
    void addInheritMethod(std::string name, classData* data) {
    	this->inherited_methods.insert(std::make_pair(name,data));
    }

};

// symbol table class (an unordered map of symbols and their attributes)
class Symtab {
public:
    
	std::unordered_map<std::string, SymData*> symtab;
	classData* Obj;
	classData* String;
	classData* Boolean;
	classData* Nothing;
	classData* Int;
	
	Symtab() {
		
		// initialize symbol table with builtin classes
		
		// initialize Obj data
		Obj = new classData("Obj","Obj");
		Obj->parent = Obj;
		std::vector<methodData> Obj_methods;
		methodData* temp;
		
		temp = new methodData("STR",STR,Obj);
		temp->addArg("this", OBJ);
		Obj_methods.push_back(*temp);
		
		temp = new methodData("PRINT",OBJ,Obj);
		temp->addArg("this", OBJ);
		Obj_methods.push_back(*temp);

		temp = new methodData("EQUALS",BOOL,Obj);
		temp->addArg("this", OBJ);
		temp->addArg("other", OBJ);
		Obj_methods.push_back(*temp);

		Obj->methods = Obj_methods;
		this->symtab.insert(std::make_pair("Obj",Obj));
		
		// initialize String data
		String = new classData("String","Obj");
		String->parent = Obj;
		std::vector<methodData> String_methods;
		
		temp = new methodData("STR",STR,String);
		temp->addArg("this", STR);
		String_methods.push_back(*temp);
		
		temp = new methodData("PRINT",STR,String);
		temp->addArg("this", STR);
		String_methods.push_back(*temp);

		temp = new methodData("EQUALS",BOOL,String);
		temp->addArg("this", STR);
		temp->addArg("other", OBJ);
		String_methods.push_back(*temp);

		String->methods = String_methods;
		this->symtab.insert(std::make_pair("String",String));
		
		
		// initialize Boolean data
		Boolean = new classData("Boolean","Obj");
		Boolean->parent = Obj;
		std::vector<methodData> Boolean_methods;
		
		temp = new methodData("STR",STR,Boolean);
		temp->addArg("this", BOOL);
		Boolean_methods.push_back(*temp);
		
		temp = new methodData("AND",BOOL,Boolean);
		temp->addArg("this", BOOL);
		temp->addArg("other", BOOL);
		Boolean_methods.push_back(*temp);
		
		temp = new methodData("OR",BOOL,Boolean);
		temp->addArg("this", BOOL);
		temp->addArg("other", BOOL);
		Boolean_methods.push_back(*temp);
		
		temp = new methodData("NOT",BOOL,Boolean);
		temp->addArg("this", BOOL);
		Boolean_methods.push_back(*temp);
		
		Boolean->methods = Boolean_methods;
		this->symtab.insert(std::make_pair("Boolean",Boolean));
		
		// initialize Nothing data
		Nothing = new classData("Nothing","Obj");
		Nothing->parent = Obj;
		std::vector<methodData> Nothing_methods;
		
		temp = new methodData("STR",STR,Nothing);
		temp->addArg("this", NOTHING);
		Nothing_methods.push_back(*temp);
		
		Nothing->methods = Nothing_methods;
		this->symtab.insert(std::make_pair("Nothing",Nothing));
		
		// initialize Int data
		Int = new classData("Int","Obj");
		Int->parent = Obj;
		std::vector<methodData> Int_methods;
		
		temp = new methodData("STR",STR,Int);
		temp->addArg("this", INT);
		Int_methods.push_back(*temp);
		
		temp = new methodData("EQUALS",BOOL,Int);
		temp->addArg("this", INT);
		temp->addArg("other", INT);
		Int_methods.push_back(*temp);
		
		temp = new methodData("LESS",BOOL,Int);
		temp->addArg("this", INT);
		temp->addArg("other", INT);
		Int_methods.push_back(*temp);

		temp = new methodData("MORE",BOOL,Int);
		temp->addArg("this", INT);
		temp->addArg("other", INT);
		Int_methods.push_back(*temp);
		
		temp = new methodData("ATLEAST",BOOL,Int);
		temp->addArg("this", INT);
		temp->addArg("other", INT);
		Int_methods.push_back(*temp);
		
		temp = new methodData("ATMOST",BOOL,Int);
		temp->addArg("this", INT);
		temp->addArg("other", INT);
		Int_methods.push_back(*temp);

		temp = new methodData("PLUS",INT,Int);
		temp->addArg("this", INT);
		temp->addArg("other", INT);
		Int_methods.push_back(*temp);
		
		temp = new methodData("PLUS",INT,Int);
		temp->addArg("this", INT);
		temp->addArg("other", INT);
		Int_methods.push_back(*temp);
		
		temp = new methodData("MINUS",INT,Int);
		temp->addArg("this", INT);
		temp->addArg("other", INT);
		Int_methods.push_back(*temp);
		
		temp = new methodData("DIVIDE",INT,Int);
		temp->addArg("this", INT);
		temp->addArg("other", INT);
		Int_methods.push_back(*temp);
		
		temp = new methodData("TIMES",INT,Int);
		temp->addArg("this", INT);
		temp->addArg("other", INT);
		Int_methods.push_back(*temp);
		
		temp = new methodData("NEG",INT,Int);
		temp->addArg("this", INT);
		Int_methods.push_back(*temp);
		
		Int->methods = Int_methods;
		this->symtab.insert(std::make_pair("Int",Int));

	}
    
    void add(std::string name, SymData* data) {
    	this->symtab.insert(std::make_pair(name,data));
    }
};


#endif //AST_EVALCONTEXT_H