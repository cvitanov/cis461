#include "CodeGenerator.h"
#include <set>

std::string CodeGenerator::indent() {
	std::string str = "";
	for (int i = 0; i < indent_level; i++) {
		str += "";
	}
	return str;
}
std::string CodeGenerator::visit(ASTNode& n, Symtab& st) { return FAIL; }

std::string CodeGenerator::visit(Void&, Symtab& st) {
	return "VOID"; // return an empty string
}

// the main program block (root)
std::string CodeGenerator::visit(Program& n, Symtab& st) {

	// TODO: specify a better name for output file (perhaps name saved in root of AST?)
	cfile.open("test.c", std::ios::out | std::ios::trunc );

	// includes
	cfile << "#include <stdio.h>\n";
	cfile << "#include <stdlib.h>\n";
	cfile << "#include \"Builtins.h\"\n\n";
	cfile << "void quackmain();\n";
	cfile << "int main(int argc, char** argv) {\n";
	cfile << "  quackmain();\n";
	cfile << "  printf(\"\\n\\n--- Waddled through successfully (quack!) ---\\n\");\n";
	cfile << "  exit(0);\n";
	cfile << "}\n";
	

	// write class definitions/declarations here
	for (auto &c: n.classes_) {
		cfile << "\n\n\n";
		cfile << c->accept(*this, st);
		cfile << "\n\n\n";
	}

	
	// quackmain()
	cfile << "void quackmain() {\n";
	
	// declare all identifiers used in quackmain
	// Iterate over an unordered_map in symtab using range based for loop
	identData* id;
	SymData* datum;
	for (auto &sym : st.symtab) {
		datum = sym.second;
		if(datum->symtype == IDENT) {
			id = (identData*) st.symtab[datum->name];
			cfile << "  obj_" + id->type + " " + id->name + ";\n";
		}
	}
	
	// write statements for main program here
	for (auto &s: n.stmts_) {
		cfile << "  " + s->accept(*this, st);
		cfile << ";\n";
	}
	
	cfile << "}"; // end of program
	
	cfile.close();
	return "DONE";
}

std::string CodeGenerator::visit(Block& n, Symtab& st) {
	std::string str;
	for (auto &s: n.stmts_) {
		str += ";\n";
		str += s->accept(*this, st);
	}
	return str;
}

std::string CodeGenerator::visit(ClassBody& cb, Symtab& st) {
	std::string class_name = scope.back();
	std::string str;
	for (auto &s: cb.stmts_) {
		str += s->accept(*this, st);
	}
	stmts[class_name] = str;
	return "";
}

std::string CodeGenerator::visit(MethodBody& mb, Symtab& st) {
	std::string method_name = scope.back();
	std::string str;
	for (auto &s: mb.stmts_) {
		str += s->accept(*this, st) + ";\n";
	}
	stmts[method_name] = str;
	return "";
}

std::string CodeGenerator::visit(ArgBlock&, Symtab& st) { return ""; }

std::string CodeGenerator::visit(ActualArgsBlock&, Symtab& st) { return ""; }

// TODO: make sure type is indicated if new object being declared for the first time
std::string CodeGenerator::visit(Assign& n, Symtab& st) {
	auto &lexpr = n.lexpr_;
	auto &type = n.type_;
	auto &rexpr = n.rexpr_;
	std::string l = lexpr.accept(*this,st);
	std::string type_specifier = type.text_;
	std::string r = rexpr.accept(*this,st);
	std::string str = "";
	str += indent();
	if(type_specifier == VOID) {
		return l + " = " + r + ";";
	}
	return l + " = (obj_" + type_specifier + ") " + r + ";";
	
}

std::string CodeGenerator::visit(Class& c, Symtab& st) {
	std::string str;
	std::string sep;
	std::string name = c.getName();
	classData* cd;
	SymData* data = nullptr;
	
	// lookup class data in symtab
	if(st.symtab.count(name))
		 data = st.symtab[name];

	if(data == nullptr or data->symtype != CLASS) {
		std::cout << "Codegen Error: Class not in symbol table!" << std::endl;
		return "";
	} else {
		cd = (classData*) st.symtab[name];
	}

	// set scope to current class
	scope.push_back(name); // set scope to current class
	scope_type = "CLASS";


	// clear stmts buffer
	stmts.clear();
	

	// traverse methods, statements, etc., in class body and get output for these
	auto &body = c.class_body_;
	indent_level += 1;
	std::string temp = body.accept(*this,st);
	
	// class declarations
	str += "/* Class " + name + " */\n\n";
	str = "struct class_" + name + "_struct;\n";
	str += "typedef struct class_" + name + "_struct* class_" + name + ";\n\n";
	str += "typedef struct obj_" + name + "_struct {\n";
	str += indent();
	str += "class_" + name + " clazz;\n";
	
	// Insert instance variables here for class object struct
	identData* theVar;
	for (std::pair<std::string, identData*> v : cd->vars) {
		theVar = v.second;
		str += indent();
		str += "obj_" + theVar->type + " " + theVar->name + ";\n";
	}
	
	str += "} * obj_" + name + ";\n\n\n";
	str += "struct class_" + name + "_struct the_class_" + name + "_struct;\n\n";
	
	// the class struct
	str += "struct class_" + name + "_struct {\n";
	
	// Insert method table for class here
	str += indent();
	str += "/* Method Table */\n";
	
	// constructor
	str += indent();
	str += "obj_" + cd->name + " (*constructor) (";
	sep = "";
	for (auto &arg: cd->args) {
		str += sep + "obj_" + arg.back();
		sep = ", ";
	}
	str += ");\n"; // end constructor
	
	// TODO: Typealts as alternative constructors!
	
	std::set<std::string> mset;
	
	for (auto &m: cd->methods) {
		// add name of method to mset (keeping track)
		mset.insert(m.name);
		str += indent();
		str += "obj_" + m.type + " " + "(*" + m.name + ") (obj_" + cd->name;
		sep = ", ";
		for (auto &arg: m.args) {
			str += sep + "obj_" + arg.back();
		}
		str += ");\n";
	}
	
	// now check parent class(es) for inherited methods
	classData* parentClassPtr = (classData*) st.symtab[cd->type];
	int check_inherited_methods = 1;
	while(check_inherited_methods) {
		for (auto &m: parentClassPtr->methods) {
			if( mset.count(m.name) == 0 ) {
				mset.insert(m.name);
				str += indent();
				if (parentClassPtr->name != OBJ) {
					str += "obj_" + m.type + " " + "(*" + m.name + ") (obj_" + parentClassPtr->name;
					sep = ", ";
				} else {
					str += "obj_" + m.type + " " + "(*" + m.name + ") (";
					sep = "";
				}
				
				for (auto &arg: m.args) {
					str += sep + "obj_" + arg.back();
					sep = ", ";
				}
				str += ");\n";
			}
		}
		if(parentClassPtr->name != OBJ) {
			parentClassPtr = (classData*) parentClassPtr->parent;
		} else {
			check_inherited_methods = 0;
			break;
		}
	}
	
	str += "};\n\n"; // end class struct
	
	str += "extern class_" + name + " " + "the_class_" + name + ";\n\n";
	
	// definitions
	
	// constructor
	str += "obj_" + name + " new_" + name + "(";
	
	// constructor arguments go here
	sep = "";
	for (auto &arg: cd->args) {
		str += sep + "obj_" + arg.back() + " " + arg.at(0);
		sep = ", ";
	}
	str += " ) {\n";
	
	str += indent();
	str += "obj_" + name + " this = (obj_" + name + ")\n";
	str += indent();
	str += indent();
	str += "malloc(sizeof(struct obj_" + name + "_struct));\n";
	str += indent();
	str += "this->clazz = the_class_" + name + ";\n";
	
	// class body statements here
	str += stmts[name];
	
//	// initialize instance variables here
//	
//	identData* id;
//	for (auto &v: cd->vars) {
//		id = v.second;
//		str += indent();
//		str += "this->" + id->name + " = " + id->name + ";\n";
//	}
	
	str += indent();
	str += "return this;\n";
	str += "}\n";
	
	// loop through methods to write cfile definitions
	for (auto &m: cd->methods) {
		str += "\nobj_" + m.type + " " + name + "_method_" + m.name + "(obj_" + name + " this";
		sep = ", ";
		for (auto &arg: m.args) {
			str += sep + "obj_" + arg.back() + " " + arg.at(0);
		}
		str += ") {\n";
		// method body goes here
		// declare local variables
		identData* var_id = nullptr;
		for (std::pair<std::string, identData*> v : m.vars) {
			var_id = v.second;
			str += indent();
			str += "obj_" + var_id->type + " " + var_id->name + ";\n";
		}
		has_return = 0;
		str += stmts[m.name];
		if(not has_return) {
			str += indent();
			str += "return (obj_" + m.type + ") nothing;\n";
		}
		str += "};\n";
	} // end method definitions
	
	// singleton for the class
	
	str += "\nstruct class_" + name + "_struct the_class_" + name + "_struct = {\n";
	str += indent();
	str += "new_" + name;
	sep = ",\n";
	
	// keep track of which methods to write
	mset.clear();
	for (auto &m: cd->methods) {
		mset.insert(m.name);
		str += sep;
		str += indent();
		str += name + "_method_" + m.name;
	}
	
	// now check parent class(es) for inherited methods
	parentClassPtr = (classData*) st.symtab[cd->type];
	check_inherited_methods = 1;
	while(check_inherited_methods) {
		for (auto &m: parentClassPtr->methods) {
			if( mset.count(m.name) == 0 ) {
				mset.insert(m.name);
				str += sep;
				str += indent();
				str += parentClassPtr->name + "_method_" + m.name;
			}
		}
		if(parentClassPtr->name != OBJ) {
			parentClassPtr = (classData*) parentClassPtr->parent;
		} else {
			check_inherited_methods = 0;
			break;
		}
	}

	str += "\n};\n\n";
	
	str += "class_" + name + " the_class_" + name + " = &the_class_" + name + "_struct;\n\n";
	
	scope.pop_back();
	scope_type = "MAIN"; // return to main program scope
	indent_level -= 1;
	return str;
}

std::string CodeGenerator::visit(Method& m, Symtab& st) {
	// visit statements to write out method definition
	std::string method_name = m.getName();
	scope.push_back(method_name);
	scope_type = "METHOD";
	auto &body = m.stmt_block_;
	std::string str =  body.accept(*this,st);
	scope.pop_back();
	scope_type = "CLASS"; // return to class scope
	return ""; // not used?
}

std::string CodeGenerator::visit(Invoke& i, Symtab& st) {
	classData* cd = nullptr;
	identData* id = nullptr;
	methodData* md;
	int class_var = 0;
	
	auto &rexpr = i.rexpr_;
	auto &method = i.method_;
	
	// get name of object whose method we're invoking
	std::string r = rexpr.accept(*this, st);
	std::string r_id = r;
	
	// remove this-> for instance variables
	if (r.substr(0,6) == "this->") {
		r_id = r.substr(6,r.size());
		// flag r as an instance variable for the class
		class_var = 1;
	}
		
	
	// get the name of the method
	std::string m = method.accept(*this, st);

	int method_found = 0;
	// find out the type of r from symbol table
	if(scope_type == "CLASS") {
		// lookup class data
		cd = (classData*) st.symtab[scope.back()];
		// lookup instance variable for r in the class
		if(class_var) {
			id = (identData*) cd->vars[r_id];
		} else {
			id = (identData*) cd->vars[r];
		}
		// lookup the matching class data for this type
		cd = (classData*) st.symtab[id->type];
		
	} else if (scope_type == "METHOD") {
		// lookup class data
		cd = (classData*) st.symtab[scope.end()[-2]];
		// if r is an instance variable for the class look it up there
		if(class_var) {
			id = (identData*) cd->vars[r_id];
		} else {
			int found_it = 0;
			
			// r is probably an argument for the method or a variable in the method look up it's type there
			// find method data
			for (int i = 0; i < (int)cd->methods.size(); i++) {
				md = &cd->methods.at(i);
				if ( md->name == scope.back() )
					break;
			}
			
			// try to find the identifier in the method args
			for (auto &a: md->args) {
				// found a match look up type in symbol table
				if (a.at(0) == r) {
					id = (identData*) st.symtab[a.back()];
					found_it = 1;
					break;
				}
			}
			
			if ( not found_it ) {
				// lookup instance variables for r in the method
				if (md->vars.count(r) > 0)
					id = (identData*) md->vars[r];
			}

		}
		
		// lookup matching class data for type of r
		cd = (classData*) st.symtab[id->type];
		
	} else {
		// should be in main program
		if (st.symtab.count(r) > 0) {
			// lookup identifier for r
			
			id = (identData*) st.symtab[r];
			// get it's type and lookup class data for this type
			cd = (classData*) st.symtab[id->type];
		} else {
			// get the return type of rexpr and use that
			cd = (classData*) st.symtab[return_type];

		}
	}
	
	// search parent classes if necessary
	// until method found
	int count = 0;
	classData* orig_cd = cd;
	while ( not method_found ) {
		count += 1;
		for (int i = 0; i < (int)cd->methods.size(); i++) {
			md = &cd->methods.at(i);
			if ( md->name == m ) {
				method_found = 1;
				return_type = md->type;
				break;
			}
		}
		
		// method not found try parent class (unless we've reached Obj already)
		if (not method_found) {
			if (cd->name == "Obj") {
				return "/* method " + m + " not found for " + r + " !!! */"; 
			} else {
				cd = (classData*) cd->parent; // try the parent class
			}
		}

	}

	std::string str = "";
	str += indent();
	
	// check if r has a STR method for printing
	// if not we upcast
	if (m == "PRINT") {
		std::string ptype = "Obj";
		std::string stype = "";
		cd = orig_cd;
		int done = 0;
		while (not done) {
			if(cd->name == "Obj") {
				done = 1; break;
			}
			for(auto &m: cd->methods) {
				if (m.name == "STR") {
					done = 1; break;
				}
			}
			if(not done) cd = (classData*) cd->parent;
		}
		
		stype = cd->name;
		
		cd = orig_cd;
		done = 0;
		while (not done) {
			if(cd->name == "Obj") {
				done = 1; break;
			}
			for(auto &m: cd->methods) {
				if (m.name == "PRINT") {
					done = 1; break;
				}
			}
			if(not done) cd = (classData*) cd->parent;
		}
		
		ptype = cd->name;
		if (ptype == "Obj"){
				str += r + "->clazz->PRINT( (obj_" + ptype + ") ";
				str += r + "->clazz->STR( (obj_" + stype + ") " + r + ") ";
		} else if (return_type == ptype) {
			str += r + "->clazz->PRINT( (obj_" + ptype + ") " + r;
		} else {
			str += r + "->clazz->PRINT( (obj_" + ptype + ") " + r;
			str += "->clazz->STR( (obj_" + stype + ") " + r + ") ";
		}

		
	} else {
		// begin method call string

		if (cd->name != id->type) {
			str = r + "->clazz->" + m + "( (obj_" + cd->name + ") " + r;
		} else {
			str = r + "->clazz->" + m + "( (obj_" + id->type + ") " + r;
		}
		
	}

	
	// actual args to use
	ActualArgsBlock &args = i.args_;
	auto arg_list = args.actual_args_;
	
	// get arg types
	std::vector<std::string> arg_types;
	for (auto &a: md->args) {
		arg_types.push_back( a.back() ); // type of arg
	}
	
	std::string sep = ", ";
	// loop through list of arguments and add them to the output
	int count_args = 0;
	for(auto &a: arg_list) {
		str += sep;
		str += " (obj_" + arg_types.at(count_args++) + ") ";
		str += a->accept(*this,st); // the actual argument
	}
	str += " )";
	return str;

}

std::string CodeGenerator::visit(If& n, Symtab& st) {
	auto &cond = n.cond_;
	auto &truepart = n.truepart_;
	auto &falsepart = n.falsepart_;
	std::string c = cond.accept(*this,st);
	std::string tp = truepart.accept(*this,st);
	std::string fp = falsepart.accept(*this,st);
	std::string str = "";
	str += indent();
	indent_level -= 1;
	str += "if (" + c + " == lit_true ) {\n";
	indent_level += 2;
	str += tp;
	str += ";\n";
	indent_level -= 1;
	str += indent();
	str += "}\n";
	str += indent();
	str += "else\n";
	str += indent();
	str += "{\n";
	indent_level += 1;
	str += fp;
	str += ";\n";
	indent_level -= 1;
	str += indent();
	str += "}\n";
	return str;
}

std::string CodeGenerator::visit(While& n, Symtab& st) {
	auto &cond = n.cond_;
	auto &truepart = n.truepart_;
	std::string c = cond.accept(*this,st);
	std::string tp = truepart.accept(*this,st);
	std::string str = "";
	str += indent();
	indent_level -= 1;
	str += "while (" + c + " == lit_true ) {\n";
	indent_level += 2;
	str += tp;
	str += "\n";
	indent_level -= 1;
	str += indent();
	str += "}\n";
	return str;
}

std::string CodeGenerator::visit(Arg&, Symtab& st) { return "ARG"; }

std::string CodeGenerator::visit(And& n, Symtab& st) {
	auto &lhs = n.left_;
	auto &rhs = n.right_;
	std::string l = lhs.accept(*this,st);
	std::string r = rhs.accept(*this,st);
	std::string str;
	str += indent();
	return str + l + "->clazz->AND(" + l + ", " + r + ")";
}

std::string CodeGenerator::visit(Or& n, Symtab& st) {
	auto &lhs = n.left_;
	auto &rhs = n.right_;
	std::string l = lhs.accept(*this,st);
	std::string r = rhs.accept(*this,st);
	std::string str;
	str += indent();
	return str + l + "->clazz->OR(" + l + ", " + r + ")";
}

std::string CodeGenerator::visit(Not& n, Symtab& st) {
	auto &s = n.stmts_;
	std::string str = s.accept(*this,st);
	return str + "->clazz->NOT(" + str  + ")";
}

std::string CodeGenerator::visit(Neg& n, Symtab& st) {
	auto &e = n.expr_;
	std::string expr = e.accept(*this, st);
	std::string str;
	str += indent();
	return str + expr + "->clazz->NEG(" + expr  + ")";
}

std::string CodeGenerator::visit(ReturnVal& n, Symtab& st) {
	auto &e = n.expr_;
	std::string returnval = e.accept(*this, st);
	has_return = 1;
	std::string str;
	str += indent();
	return str + "return " + returnval;
}


// TODO: Type alternatives not generated yet
std::string CodeGenerator::visit(Typecase&, Symtab& st) { return ""; }
std::string CodeGenerator::visit(TypeAlt&, Symtab& st) { return ""; }


std::string CodeGenerator::visit(Construct& c, Symtab& st) {
	
	std::string str;

	// get class name
	auto &theClass = c.class_;
	std::string class_name = theClass.getName();
	
	// constructor format
	str = "the_class_" + class_name + "->constructor(";
	
	ActualArgsBlock &args = c.args_;
	auto arg_list = args.actual_args_;
	
	// loop through list of arguments and add them to output
	std::string sep = "";
	for (auto &a: arg_list) {
		// add argument to output
		str += sep;
		str += a->accept(*this,st);
		sep = ", ";
	}
	str += ")";
	
	
	return_type = class_name;
	return str;
	
}

std::string CodeGenerator::visit(Ident& id, Symtab& st) {
	std::string name = id.getName();
	std::string str;
	str += indent();
	
	if (name == "true")
		return str + "lit_true";
	
	if (name == "false")
		return str + "lit_false";
	
	return name;
}

std::string CodeGenerator::visit(Field& f, Symtab& st) {
	auto &obj = f.object_;
	auto &field = f.field_;
	std::string obj_str = obj.accept(*this, st);
	std::string field_str = field.accept(*this,st);
	std::string str;
	str += indent();
	return str + obj_str + "->" + field_str;
}

// return the integer constant as a string
std::string CodeGenerator::visit(IntConst& n, Symtab& st) {
	int v = n.value_;
	std::string str;
	str += indent();
	return str + "int_literal(" + std::to_string(v) + ")";
}

// return the string constant as a string with quotes
std::string CodeGenerator::visit(StrConst& n, Symtab& st) {
	std::string str;
	str += indent();
	return str + "str_literal(\"" + n.text_ + "\")"; // put in quotes
}

std::string CodeGenerator::visit(BinOp& b, Symtab& st) {
	auto &lhs = b.left_;
	auto &rhs = b.right_;
	std::string l = lhs.accept(*this,st);
	std::string r = rhs.accept(*this,st);
	std::string str;
	str += indent();
	return str + l + "->clazz->" + b.opsym + "(" + l + "," + r + ")";
}

std::string CodeGenerator::visit(RelOp& b, Symtab& st) {
	auto &lhs = b.left_;
	auto &rhs = b.right_;
	std::string l = lhs.accept(*this,st);
	std::string r = rhs.accept(*this,st);
	std::string str;
	str += indent();
	if (b.opsym == "EQUALS") {
		return str + l + "->clazz->" + b.opsym + "(" + l + ", (obj_Obj) " + r + ")";
	} else {
		return str + l + "->clazz->" + b.opsym + "(" + l + "," + r + ")";
	}
}
