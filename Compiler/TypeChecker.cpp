#include "TypeChecker.h"
#define DEBUG 0

void TypeChecker::terror(std::string msg) {
	error_count += 1;
	tc_error = "Typecheck Error: " + msg;
	throw -1;
}

std::string TypeChecker::check(ASTNode * root, Symtab& st) {
	std::string result;
	try {
		result = root->accept(*this,st);
		return result;
	} catch (int) {
		std::cerr << "Typecheck failed!" << std::endl;
		std::cerr << tc_error << std::endl; 
		return FAIL;
	}
}

std::string TypeChecker::visit(ASTNode& n, Symtab& st) { return FAIL; }

std::string TypeChecker::visit(Void& n, Symtab& st) {
	return VOID; // Void nodes don't have a specific type, so return a "void type" for bookkeeping
}

// type check the main program block
std::string TypeChecker::visit(Program& n, Symtab& st) {
	if (DEBUG) std::cout << "DEBUG: program" << std::endl;

	std::string result;
	
	scope.push_back("Obj"); // main program has scope "Obj"
	scope_type = "MAIN";
	
	// type check each class in the program
	for (auto &c: n.classes_) {
		result = c->accept(*this, st);
		if(result == FAIL) {
			return FAIL;
		}
	}
	
	// type check each statement in the program
	for (auto &s: n.stmts_) {
		result = s->accept(*this, st);
		if(result == FAIL) {
			return FAIL;
		}
	}
	return VOID; // Return VOID if everything passes typecheck  
}

std::string TypeChecker::visit(Block& n, Symtab& st) {
	if (DEBUG) std::cout << "DEBUG: block" << std::endl;

	std::string result;
	// type check each statement in the block
	for (auto &s: n.stmts_) {
		result = s->accept(*this, st);
		if(result == FAIL) {
			terror("Block in scope = " + scope.back());
			return FAIL; // one of the nodes in the block failed (fail upward)
		}
	}
	return VOID;
}

std::string TypeChecker::visit(ClassBody& cb, Symtab& st) {
	if (DEBUG) std::cout << "DEBUG: class body" << std::endl;

	return VOID;
}

std::string TypeChecker::visit(MethodBody& mb, Symtab& st) {
	if (DEBUG) std::cout << "DEBUG: method body" << std::endl;

	return VOID;
}

std::string TypeChecker::visit(ActualArgsBlock&, Symtab& st) {
	if (DEBUG) std::cout << "DEBUG: actual args block" << std::endl;

	return VOID;
}

std::string TypeChecker::visit(ArgBlock& n, Symtab& st) {
	if (DEBUG) std::cout << "DEBUG: arg block" << std::endl;

	return VOID;
}

std::string TypeChecker::visit(Assign& a, Symtab& st) {
	if (DEBUG) std::cout << "DEBUG: assign" << std::endl;
	auto &lexpr = a.lexpr_;
	auto &type = a.type_;
	auto &rexpr = a.rexpr_;

	// typecheck lexpr
	assignment_flag = 1;
	std::string lexpr_type = lexpr.accept(*this, st);
	assignment_flag = 0;

	// specified type of lexpr
	std::string type_specifier = type.text_;
	if (DEBUG) std::cout << "DEBUG: assign lexpr_type = " << lexpr_type << " type_specifier = " << type_specifier << std::endl;
	
	// type check rexpr
	if (DEBUG) std::cout << "DEBUG: assign (rexpr) " << std::endl;
	std::string rexpr_type = rexpr.accept(*this, st);
	if (DEBUG) std::cout << "DEBUG: assign (end rexpr) type = " << rexpr_type << std::endl;

	if(type_specifier != VOID) {
		if (DEBUG) std::cout << "DEBUG: type specifier = " << type_specifier << std::endl;
		// if type specified check if this is a valid type specification
		if(type_specifier != lexpr_type && lexpr_type != VOID) {
			terror("Cannot assign type " + type_specifier + " to object of type " + lexpr_type);
			return FAIL;
		}
		lexpr_data->type = type_specifier;
		if (DEBUG) std::cout << "DEBUG: setting " << lexpr_data->name << " in " << lexpr_data->parent->name << " to type " << type_specifier << std::endl;
		return type_specifier; 
	} else {
		
		if(lexpr_type == VOID) {
			// set the type of the left hand side to be the type of the right hand side (type inference)
			lexpr_data->type = rexpr_type;
			if (DEBUG) std::cout << "DEBUG: setting " << lexpr_data->name << " to type " << rexpr_type << std::endl;
			return rexpr_type;
		} else if(rexpr_type != lexpr_type) {
			// check if type of rexpr is derived from type of lexpr
			int failure = 0;
			std::string t = rexpr_type;
			while(t != lexpr_type) {
				if(t == OBJ) {
					failure = 1;
					break;
				}
				classData* tdata = (classData*) st.symtab[t];
				t = tdata->type;
			}
			if(failure) {
				t = lexpr_type;
				while(t != rexpr_type) {
					if(t == OBJ) {
						terror("Cannot assign type " + rexpr_type + " to object of type " + lexpr_type);
						return FAIL;
					}
					classData* tdata = (classData*) st.symtab[t];
					t = tdata->type;
				}
			}
			// update node for assignment with an implicit typecast
			type.setName(lexpr_type);
			return lexpr_type;
		}
		
		
	}


	
	return VOID;
}

std::string TypeChecker::visit(Class& c, Symtab& st) {
	if (DEBUG) std::cout << "DEBUG: class" << std::endl;

	// add class to symbol table 
	
	// get name and type of class from AST data
	std::string name = c.getName();
	std::string type = OBJ;
	if(c.hasExtends())
		type = c.getType();
	
	// initialize class in symbol table
	classData* cd = new classData(name, type);
	
	// check if redefinition
	if(st.symtab.count(name) > 0) {
		terror("Redefinition of class " + name);
	}
	// add class data to symbol table
	st.add(name, cd);
	
	// try to set class data parent pointer to the parent class (Obj if no extends declaration)
	// then check for cyclic dependencies
	if( st.symtab.find(type) != st.symtab.end() ) {
		auto match = st.symtab[type];
		
		// is the matching symbol an instance of a class in the symbol table?
		if(match->symtype == CLASS) {
			// get the superclass and set new class to point to it
			classData* parent_class = (classData*) st.symtab[type];
			cd->parent = parent_class;
			
			// check for a cycle in class dependency graph
			while(parent_class->name != "Obj") {
				// get parent's parent
				parent_class = (classData*) st.symtab[parent_class->type];
				// is there a cycle? (class has itself as superclass)
				if(parent_class->type == type) {
					terror("Cyclic class dependency detected for class " + name);
					return FAIL;
				}
			}
		}
	}

	

	

	

	
	scope.push_back(name); // set scope to current class
	scope_type = "CLASS";

	// visit formal args block (adds formal args to class data in symbol table)
	std::string arg_id;
	std::string fa_type;
	auto &fa = c.formal_args_;
	auto &args = fa.idents_;
	auto &arg_types = fa.types_;
	Ident* ident;
	for (unsigned i = 0; i < args.size(); i++) {
		ident = args.at(i);
		arg_id = ident->getName();
		ident = arg_types.at(i);
		fa_type = ident->getName();
		cd->addArg(arg_id,fa_type);
	}
	
	// visit class body
	auto &body = c.class_body_;
	std::string result;
	for (auto &stmt: body.stmts_) {
		result = stmt->accept(*this,st);
		if(result == FAIL) {
			std::cout << "Typecheck Failure: In body of class: " + name << std::endl;
			return FAIL;
		}
	}
	
	// successfully processed the class, return it's type
	scope.pop_back();
	scope_type = "MAIN"; // return to main program scope
	return type;
	
}

std::string TypeChecker::visit(Method& m, Symtab& st) {

	if(DEBUG) std::cout << "DEBUG METHOD 0" << std::endl;

	
	// add method to symbol table 
	// TODO: check if it already exists???
	
	// get name and type of method from AST data
	std::string name = m.getName();
	std::string return_type = m.getType();
	
	
	// first get class data entry for this method from symbol table			
	std::string parent = scope.back();
	

	if (DEBUG) std::cout << "DEBUG: method " << name << " in class " <<  parent << " returns type " << return_type << std::endl;

	if(return_type == VOID) {
		std::cout << "Typecheck Failure: No type for method " + name + " of class " + scope.back() << std::endl;
	}

	// lookup parent class in the Symbol Table and check if it's there
	if( st.symtab.find(parent) == st.symtab.end() ) {
		std::cout << "Typecheck Error: No class of type " << parent << " for method " << name << std::endl;
		return FAIL;
	}
	
	// class exists in symbol table so get pointer to parent class
	classData* parentClassPtr = (classData*) st.symtab[parent];
	
	if( parentClassPtr->name == "Obj" ) {
		std::cout << "Typecheck Failure: Attempt to declare method in scope of builtin Obj class" << std::endl;
		return FAIL;
	}
	
	if( parentClassPtr->symtype != CLASS ) {
		std::cout << "Typecheck Failure: Methods should be declared within the scope of a class." << std::endl;
		return FAIL;
	}
	
	// initialize method as a symbol table entry (name, type, parent class)
	methodData md(name, return_type, parentClassPtr);
   
	// set parent method to this method (for scope resolution)
	parent_method = &md;
	
	std::string arg_id;
	std::string fa_type;
	auto &fa = m.formal_args_;
	auto &args = fa.idents_;
	auto &arg_types = fa.types_;
	Ident* ident;
	for (unsigned i = 0; i < args.size(); i++) {
		ident = args.at(i);
		arg_id = ident->getName();
		ident = arg_types.at(i);
		fa_type = ident->getName();
		md.addArg(arg_id,fa_type);
	}
	
	// check if redefinition of method
	for(auto &m: parentClassPtr->methods) {
		if(m.name == name) {
			terror("Redefinition of method " + name);
			return FAIL;
		}
	}
	// add this method to the attributes of the parent class
	parentClassPtr->addMethod(md);
	
	scope.push_back(name); // set scope to current method
	scope_type = "METHOD";
	
	if(DEBUG) std::cout << "DEBUG METHOD 1" << std::endl;
	auto &body = m.stmt_block_;
	std::string result;
	if(DEBUG) std::cout << "DEBUG METHOD 2" << std::endl;

	for (auto &stmt: body.stmts_) {
		result = stmt->accept(*this,st);
		if(result == FAIL) {
			std::cout << "Typecheck Failure: In body of method: " + name << std::endl;
			return FAIL;
		}
	}
	if(DEBUG) std::cout << "DEBUG METHOD 3" << std::endl;

	
	// successfully processed the method, return it's type
	scope.pop_back();
	scope_type = "CLASS"; // return to class scope
	return return_type;
	
}

// TODO: Finish Invoke typecheck
std::string TypeChecker::visit(Invoke& i, Symtab& st) {
	if (DEBUG) std::cout << "DEBUG: invoke" << std::endl;
	auto &rexpr = i.rexpr_;
	//auto &method = i.method_;
	std::string rtype = rexpr.accept(*this, st); // type check rexpr (not aptly named)
	//invoke_flag = 1;
	//std::string md = method.accept(*this, st);
	//invoke_flag = 0;
	
	std::string method = i.method_.getName();
	if (DEBUG) std::cout << "DEBUG: invoke method " << method << " for class " << rtype << std::endl;
	
	
	// lookup rexpr type in the Symbol Table and check if it's there
	// TODO: fix this to work with more complex types?
	if( st.symtab.find(rtype) == st.symtab.end() ) {
		terror("Invalid call to method " + method + " of type " + rtype + ". No such type declared.");
		return FAIL;
	}
	
	// first get class data entry for this method from symbol table			
	
	auto parentClassPtr = st.symtab[rtype];
	
	// should be invoking a class
	if( parentClassPtr->symtype != CLASS ) {
		std::cout << "Typecheck Error: " << rtype << " is not a class. Cannot make call to method." << std::endl;
		return FAIL;
	}
	
	// check if this is a valid method call for the class
	classData* cd = (classData*) st.symtab[rtype];
	int done = 0;
	while(not done) {
		if (DEBUG) std::cout << "DEBUG: invoke, check methods for " << cd->name << std::endl;
		for (auto &m: cd->methods) {
			if (m.name == method) {
				// check if arguments pass typecheck and match
				
				// check if arg types are valid
				// visit each argument and type check
				// then check list of types against symtab data for the matching method
				ActualArgsBlock &args = i.args_;
				auto arg_list = args.actual_args_;
				std::vector<ASTNode*>::iterator iter, end;
				std::string valid_type;
				std::string type;
				int argn = 0;
				std::vector<std::vector<std::string> > method_args = m.args;
				// loop through list of arguments and type check each
				for(iter = arg_list.begin(), end = arg_list.end() ; iter != end; ++iter) {
					valid_type = method_args.at(argn).back();
					argn++;
					type = (*iter)->accept(*this,st);
					// check if these are declared types listed in symtab
					// if they are not check if types are equal
	
					if( valid_type == type || valid_type == OBJ ) {
						// same types or valid_type could be any type that inherits from OBJ
						continue;
					} else {
						
						// not the same type, but if type inherits from valid_type type check should still pass
						// check parent types of type
						// loop until reaching Obj parent type
						auto type_data = st.symtab[type];
						while(type_data->type != OBJ) {
							if(type_data->type == valid_type) {
								// successfully matched a parent type to valid_type
								break;
							}
							type_data = type_data->parent;
						}
						if(type_data->type == OBJ) {
							terror("Mismatched types for invocation of method " + method);
							return FAIL;
						}
					}
					
				}
				// passed invoke typecheck, return the return type associated with this method!
				if (DEBUG) std::cout << "DEBUG: Invoke method " << m.name << " for class " << rtype << ", return type " << m.type << std::endl;
				return m.type;
			}
		}
		
		// check if we should continue checking for superclasses with this method
		if (cd->name == OBJ) {
			done = 1;
		} else {
			// could not match method to this class, try superclass
			cd = (classData*) st.symtab[cd->parent->name];
		}

	}
	terror("Invalid method call to " + method + " for class " + rtype);
	return FAIL;
}

std::string TypeChecker::visit(If& n, Symtab& st) {
	if (DEBUG) std::cout << "DEBUG: if" << std::endl;

	auto &c = n.cond_;
	auto &t = n.truepart_;
	auto &f = n.falsepart_;
	std::string ctype = c.accept(*this,st);
	std::string ttype = t.accept(*this,st);
	std::string ftype = f.accept(*this,st);
	if(ctype != BOOL) {
		std::cout << "Typecheck Error: if statement conditional not boolean." << std::endl;
		return FAIL;
	} else {
		if(ttype == FAIL || ftype == FAIL) {
			std::cout << "Typecheck Error: if statement failure." << std::endl;
			return FAIL;
		} else {
			return VOID;
		}
	}
}

std::string TypeChecker::visit(While& n, Symtab& st) {
	if (DEBUG) std::cout << "DEBUG: while" << std::endl;

	auto &c = n.cond_;
	auto &t = n.truepart_;
	std::string ctype = c.accept(*this,st);
	std::string ttype = t.accept(*this,st);
	if(ctype != BOOL) {
		std::cout << "Typecheck Error: while loop conditional not boolean." << std::endl;
		return FAIL;
	} else {
		if(ttype == FAIL) {
			std::cout << "Typecheck Error: while loop statement failure." << std::endl;
			return FAIL;
		} else {
			return VOID;
		}
	}
}

std::string TypeChecker::visit(Arg& n, Symtab& st) {
	if (DEBUG) std::cout << "DEBUG: arg" << std::endl;
	auto &arg = n.arg_;
	auto &t = n.type_;
	
	std::string name = arg.getName();
	std::string type = t.getName();
	
	// FIXME: scope should be able to nest class -> method etc!
	std::string current_scope = scope.back();
	if( st.symtab.find(current_scope) == st.symtab.end() ) {
		std::cout << "Typecheck Error: Nothing declared of type " << current_scope << std::endl;
		return FAIL;
	} else {
		auto parent = st.symtab[current_scope];
		parent->addArg(name,type);
	}
	return type;
}

std::string TypeChecker::visit(And& n, Symtab& st) {
	if (DEBUG) std::cout << "DEBUG: and" << std::endl;
	auto &l = n.left_;
	auto &r = n.right_;
	std::string lhs = l.accept(*this, st);
	std::string rhs = r.accept(*this, st);
	if(lhs == BOOL && rhs == BOOL) {
		return BOOL;
	} else {
		std::cout << "Typecheck Error: Require boolean types for AND comparison." << std::endl;
		return FAIL;
	}
}

std::string TypeChecker::visit(Or& o, Symtab& st) {
	if (DEBUG) std::cout << "DEBUG: or" << std::endl;
	auto &l = o.left_;
	auto &r = o.right_;
	std::string lhs = l.accept(*this, st);
	std::string rhs = r.accept(*this, st);
	if(lhs == BOOL && rhs == BOOL) {
		return BOOL;
	} else {
		std::cout << "Typecheck Error: Require boolean types for OR comparison." << std::endl;
		return FAIL;
	}
}

std::string TypeChecker::visit(Not& n, Symtab& st) {
	if (DEBUG) std::cout << "DEBUG: not" << std::endl;
	auto &s = n.stmts_;
	std::string rt = s.accept(*this,st);
	// only accept boolean type
	if(rt == BOOL) {
		return rt;
	} else {
		std::cout << "Typecheck Error: Cannot perform logical not for type " + rt << std::endl;
		return FAIL;
	}
	return rt;
}

std::string TypeChecker::visit(Neg& n, Symtab& st) {
	if (DEBUG) std::cout << "DEBUG: neg" << std::endl;
	// return the type of the expression being negated
	// but if it is a non-negatable type print an error
	auto &e = n.expr_;
	std::string rt = e.accept(*this, st);
	if(rt == INT) {
		return rt;
	} else {
		std::cout << "Typecheck Error: Cannot negate expression of type " + rt << std::endl;
		return FAIL;
	}
}

std::string TypeChecker::visit(ReturnVal& n, Symtab& st) {
	if (DEBUG) std::cout << "DEBUG: return val" << std::endl;
	std::string parent_method;
	std::string parent_class;
	std::string method_return_type = FAIL;
	std::string return_statement_type = FAIL;
	
	// first get method data entry for this return statement from symbol table
	if(scope.size() > 1) {
		parent_method = scope.back();
		parent_class = scope.end()[-2];
	} else {
		std::cout << "Typecheck Error: Return statements outside method declarations are illegal." << std::endl;
		return FAIL;
	}
	
	// lookup parent class in the Symbol Table and check if it's there
	if( st.symtab.find(parent_class) == st.symtab.end() ) {
		std::cout << "Typecheck Error: Class not defined." << std::endl;
		return FAIL;
	}
	
	// class exists in symbol table so get pointer to parent class
	classData* parentClassPtr = (classData*) st.symtab[parent_class];
	
	// get method return type and check against type of return expression
	for (auto &m: parentClassPtr->methods) {
		if (m.name == parent_method) {
			method_return_type = m.type;
			break;
		}
	}
	if(method_return_type == FAIL) {
		std::cout << "Typecheck Error: Method return type undeclared." << std::endl;
		return FAIL;
	}
	
	// return the type of the return statement to the associated method
	auto &e = n.expr_;
	return_statement_type = e.accept(*this, st);
	if(return_statement_type != method_return_type) {
		std::cout << "Typecheck Error: Return statement type " << return_statement_type << " does not match method return type " << method_return_type << std::endl;
		return FAIL;
	} else {
		return method_return_type;
	}
}

// typecheck typecase statements, typecase gets added to parent class data in symtab
// returns void or fail if typecheck fails
std::string TypeChecker::visit(Typecase& r, Symtab& st) {
	if (DEBUG) std::cout << "DEBUG: typecase" << std::endl;
	
	// TODO: Is it necessary to typecheck the rexpr?
	// typecheck the rexpr
	//auto &rexpr = r.r_expr_;
	//std::string rexpr_type = rexpr.accept(*this, st);
	
	// TODO: check if this is a valid type for a typecase statement? How?
	
	
	// first get method data entry for this typecase statement from symbol table			
	std::string parent = scope.back();
	
	// lookup parent method in the Symbol Table and check if it's there
	if( st.symtab.find(parent) == st.symtab.end() ) {
		std::cout << "Typecheck Error: Typecase statement outside of method declaration illegal." << std::endl;
		return FAIL;
	}
	
	// method exists in symbol table so get pointer to parent method
	auto parentPtr = st.symtab[parent];
	
	if( parentPtr->symtype != METHOD ) {
		std::cout << "Typecheck Error: Typecase statement outside of method declaration illegal." << std::endl;
		return FAIL;
	}
	
	return VOID; // typecase statement does not have a type to return
}


std::string TypeChecker::visit(TypeAlt& ta, Symtab& st) { 
	if (DEBUG) std::cout << "DEBUG: type alt" << std::endl;
	return "OOPS";

}

// type check constructor ... returns class name as type
std::string TypeChecker::visit(Construct& c, Symtab& st) {
	if (DEBUG) std::cout << "DEBUG: construct" << std::endl;

	// get class name
	auto &theClass = c.class_;
	std::string class_name = theClass.getName();
	std::vector<std::string> arg_types;
	
	// check if class exists in symtab
	if( st.symtab.find(class_name) == st.symtab.end() ) {
		// no such class, invalid constructor
		std::cout << "Typecheck Error: No declared class for constructor " << class_name << "." << std::endl;
		return FAIL;
	}
	
	// lookup class data
	auto match = st.symtab[class_name];
	
	
	// is the matching object an instance of a class in the symbol table?
	if(match->symtype != CLASS) {
		std::cout << "Typecheck Error: Only classes have constructors." << std::endl;
		return FAIL;
	}
	
	classData* cd = (classData*) st.symtab[class_name];
	
	// check if arg types are valid
	// visit each argument and type check
	// then check list of types against symtab data for this class
	ActualArgsBlock &args = c.args_;
	auto arg_list = args.actual_args_;
	std::vector<ASTNode*>::iterator iter, end;
	std::string valid_type;
	std::string type;
	int argn = 0;
	std::vector<std::vector<std::string> > class_args = cd->args;
	// loop through list of arguments and type check each
	for(iter = arg_list.begin(), end = arg_list.end() ; iter != end; ++iter) {
		valid_type = class_args.at(argn).back();
		argn++;
		type = (*iter)->accept(*this,st);
		// check if these are declared types listed in symtab
		// if they are not check if types are equal
		if( st.symtab.find(valid_type) == st.symtab.end() && st.symtab.find(type) == st.symtab.end() ) {
			// not declared types so must be BOOL, INT, OBJ or STR, check for type equality
			if( type == BOOL || type == INT || type == STR || type == OBJ ) {
				if( valid_type != type ) {
					std::cout << "Typecheck Error: Invalid constructor types for class " << class_name << std::endl;
					return FAIL;
				}
			} else {
				std::cout << "Typecheck Error: Argument of type " << type << " invalid for constructor of type " << class_name << std::endl;
				return FAIL;
			}
		} else {
			if( valid_type == type || valid_type == OBJ ) {
				// same types or valid_type could be any type that inherits from OBJ
				continue;
			} else {
				
				// not the same type, but if type inherits from valid_type type check should still pass
				// check parent types of type
				// loop until reaching Obj parent type
				auto type_data = st.symtab[type];
				while(type_data->type != OBJ) {
					if( type_data->symtype != CLASS ) {
						// must be a declared class type or standard type
						std::cout << "Typecheck Error: Argument of type " << type << " invalid for constructor of type " << class_name << std::endl;
						return FAIL;
					} else {
						if(type_data->type == valid_type) {
							// successfully matched a parent type to valid_type
							break;
						}
					}
					type_data = type_data->parent;
				}
				if(type_data->type == OBJ) {
					std::cout << "Typecheck Error: Mismatched types for constructor of class " << class_name << std::endl;
					return FAIL;
				}
			}
		}
		
	}
	// passed all typechecks for this constructor, return class name as the type of the constructor
	return match->name;
}

// type check identifier. returns type if identifier exists in symbol table
std::string TypeChecker::visit(Ident& i, Symtab& st) {

	std::string name = i.getName();
	
	// check if bad assignment to predefined identifier
	if(predefined_identifiers.count(name) > 0 and assignment_flag) {
		terror("Cannot redefine builtin identifier " + name);
	}
	
	
	if(name == "true" || name == "false") {
		if (DEBUG) std::cout << "DEBUG: ident Boolean " << name << std::endl;
		return BOOL;
	}
	
	if(invoke_flag) {
		if (DEBUG) std::cout << "DEBUG: ident (invoke) " << name << std::endl;
		return VOID;
	}
	
	if(i.is_field) {
		if (DEBUG) std::cout << "DEBUG: ident (field) " << name << std::endl;
	} else {
		if (DEBUG) std::cout << "DEBUG: ident (not field) " << name << std::endl;

	}
		
	
	if(scope.size()) {
		if (DEBUG) std::cout << "DEBUG: Ident with name " << name << " in scope = " << scope.back() << std::endl;
	}

	// if scope is a class or method must take this into account
	if( scope.back() != "Obj" ) {
		
		// lookup parent scope information in the Symbol Table and check if identifier is there
		
		std::string parent = scope.back();
		std::vector<std::vector<std::string> > args;
		classData* parentClassPtr;
		
		// is it a class?
		if(scope_type == "CLASS") {
			
			
			// check if identifier is an argument of the parent class
			parentClassPtr = (classData*) st.symtab[parent];
			args = parentClassPtr->args;
			
			// if identifier is "this" return name of parent as it's type!
			if(name == "this") {
				return parentClassPtr->name;
			}
			
			for (auto &a: args) {
				if ( a.at(0) == name ) {
					if (DEBUG) std::cout << "DEBUG: ident " << name << " is arg of type " << a.at(1) << " for " << "class " << parent << std::endl;
					return a.at(1); // return type of matching argument
				}
			}
			
			std::cout << "Typecheck Error: Unknown identifier " << name << " for " << "class " << parent << std::endl;
			return FAIL;
			
			
		} else if (scope_type == "METHOD") {
			

			
			// check if identifier is an argument of the parent method
			if(scope.size() > 1) {
				std::string parent_class = scope.end()[-2];
				parentClassPtr = (classData*) st.symtab[parent_class];
			} else {
				std::cout << "Typecheck Error: Method " << parent << " declared outside of class scope " << std::endl;
				return FAIL;
			}
			
			// get the method and it's args
			methodData* theMethod;
			for (auto &m: parentClassPtr->methods) {
				if (m.name == parent) {
					theMethod = &m;
					args = theMethod->args;
					
					// if identifier is "this" return name of parent class as it's type!
					if(name == "this") {
						return parentClassPtr->name;
					}
					
					for (auto &a: args) {
						if ( a.at(0) == name ) {
							if (DEBUG) std::cout << "DEBUG: ident " << name << " is arg of type " << a.at(1) << " for " << "method " << parent << std::endl;
							return a.at(1); // return type of matching argument
						}
					}
				}
			}
			
			// check if this is a variable declared in this method
			if (theMethod->vars.count(name) > 0) {
				identData* id = theMethod->vars[name];
				return id->type;
			}
			
			// otherwise add a new variable for this method to symbol table
			if (DEBUG) std::cout << "DEBUG: new id for " << name << " with type " << VOID << " in method " << theMethod->name << " of class " << parentClassPtr->name << std::endl;
			identData* id = new identData(name, VOID);
			lexpr_data = id; // for type inference
			theMethod->vars.insert( {name,id} );
			return VOID;
			
		} else {
			
			std::cout << "Typecheck Error: Unknown scope for variable " << name << std::endl;
			return FAIL;
			
		}

	// main program scope:
		
	} else {
		// check if identifer is in symtab already... if not create new symtab data entry
		if( st.symtab.find(name) == st.symtab.end() ) {
			if (DEBUG) std::cout << "DEBUG: new id for " << name << " with type " << VOID << std::endl;
			identData* id = new identData(name, VOID);
			lexpr_data = id; // for type inference
			st.add(name,id);
			return VOID;
		} else {
			// identifier already exists
			// return it's type
			auto match = st.symtab[name];
			return match->type;
		}
	}
	
}

// type check field returns type of field if it is a valid field, errors otherwise
std::string TypeChecker::visit(Field& f, Symtab& st) {
	if (DEBUG) std::cout << "DEBUG: field " << std::endl;
	
	if (DEBUG) std::cout << "DEBUG: field (object_)" << std::endl;
	// typecheck the object (rexpr)
	auto &o = f.object_;
	std::string obj_type = o.accept(*this, st);
	
	// get field name
	auto &theField = f.field_;
	std::string field_name = theField.getName();
	if (DEBUG) std::cout << "DEBUG: field name = " << field_name << std::endl;

	// lookup object's type in the Symbol Table and check if it has this field
	if( st.symtab.find(obj_type) == st.symtab.end() ) {
		std::cout << "Typecheck Error: No object declared of type " << obj_type << std::endl;
		return FAIL;
	}
	
	// object's type exists in symbol table so get data for this type
	auto match = st.symtab[obj_type];
	
	// is the matching object an instance of a class in the symbol table?
	if(match->symtype != CLASS && match->symtype != METHOD) {
		std::cout << "Typecheck Error: Only classes or methods can have fields." << std::endl;
		return FAIL;
	}
	
	
	// check if field exists
	
	std::unordered_map<std::string, identData*> vars;
	
	if ( match->symtype == CLASS ) {
		classData* cd = (classData*) st.symtab[obj_type];
		vars = cd->vars;
	} else if ( match->symtype == METHOD ) {
		methodData* md = (methodData*) st.symtab[obj_type];
		vars = md->vars;
	}
	
	std::unordered_map<std::string, identData*>::const_iterator got = vars.find(field_name);
	if( got != vars.end()) {
		auto field = got->second;
		if( field->symtype != IDENT ) {
			std::cout << "Typecheck Error: " << field_name << " is not a field of class " << match->name << std::endl;
			return FAIL;
		} else {
			return field->type; // return type of the field (it exists!)
		}
	} else {
		// no such field
		
		// if we're in scope of class or method add this field to "instance variables" for class/method
		std::string parent = scope.back();
		identData* id;
					
		// lookup parent method in the Symbol Table and check if it's there
		if( st.symtab.find(parent) != st.symtab.end() ) {
			auto match = st.symtab[parent];
			
			// if parent scope is a class that matches obj_type add the field to the class's instance variables
			if( match->symtype == CLASS ) {
				classData* parentClassPtr = (classData*) st.symtab[obj_type];
				id = new identData(field_name, VOID, parentClassPtr);
				parentClassPtr->addVar(id);
				lexpr_data = id;
				return VOID;
			} else if( match->symtype == METHOD ) {
				methodData* parentMethodPtr = (methodData*) st.symtab[obj_type];
				id = new identData(field_name, VOID, parentMethodPtr);
				parentMethodPtr->addVar(id);
				lexpr_data = id;
				return VOID;
			} else {
				std::cout << "Typecheck Error: Cannot initialize instance variable outside of method or class declaration." << std::endl;
				return FAIL;
			}
			return id->type;
		}
		
		std::cout << "Typecheck Error: No field " << field_name << " for class " << match->name << std::endl;
		return FAIL;
	}

	
	std::cout << "Typecheck Error: Unknown error. Attempt to access field " << field_name << " for object type " << obj_type << std::endl; 
	return FAIL;
}

// Typecheck IntConst returns INT
std::string TypeChecker::visit(IntConst& i, Symtab& st) {
	if (DEBUG) std::cout << "DEBUG: Int " << std::endl;
	return INT; // returns string for Integer type
}

// Typecheck StrConst returns STR
std::string TypeChecker::visit(StrConst& s, Symtab& st) {
	if (DEBUG) std::cout << "DEBUG: Str  " << std::endl;
	return STR; // return string for String type
}

// Typecheck BinOp returns the type of left hand expression
std::string TypeChecker::visit(BinOp& n, Symtab& st) {
	if (DEBUG) std::cout << "DEBUG: BinOp " << n.opsym << std::endl;
	// lhs and rhs must be compatible types
	auto &l = n.left_;
	auto &r = n.right_;
	std::string lhs = l.accept(*this, st);
	std::string rhs = r.accept(*this, st);
		// compare types (both must be integers)
	if(lhs == rhs) {
		return lhs;
	} else {
		std::cout << "Typecheck Error: Binary operation with unmatched types." << std::endl;
		return FAIL; 
	}
	
}    

// Typecheck RelOp returns the BOOL if pass
std::string TypeChecker::visit(RelOp& n, Symtab& st) {
	if (DEBUG) std::cout << "DEBUG: BinOp " << n.opsym << std::endl;
	// lhs and rhs must be compatible types
	auto &l = n.left_;
	auto &r = n.right_;
	std::string lhs = l.accept(*this, st);
	std::string rhs = r.accept(*this, st);
		// compare types (both must be integers)
	if(lhs == INT && rhs == INT) {
		return BOOL;
	} else {
		std::cout << "Typecheck Error: Binary operation " << n.opsym << " valid only for integer type expressions." << std::endl;
		return FAIL; 
	}
	
}    

