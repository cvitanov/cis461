cmake_minimum_required(VERSION 3.12)
project(QuackParser)
set(CMAKE_CXX_STANDARD 14)

# Where are the reflex include and library files?
#
set(REFLEX_LIB "/usr/local/lib/libreflex.a")
set(REFLEX_INCLUDE "/usr/local/include/reflex")
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -g")

# Recipes for generating C++ source files from .lxx and .yxx files
#

add_custom_command(
        OUTPUT lex.yy.cpp lex.yy.h
        COMMAND   reflex --bison-cc --bison-locations --header-file ${CMAKE_CURRENT_SOURCE_DIR}/quack.lxx
        DEPENDS quack.lxx quack.tab.hxx
)
add_custom_command(
        OUTPUT quack.tab.cxx quack.tab.hxx location.hh position.hh stack.hh
        COMMAND bison -d ${CMAKE_CURRENT_SOURCE_DIR}/quack.yxx
        DEPENDS quack.yxx
)

include_directories(
        ${CMAKE_CURRENT_BINARY_DIR}
        ${REFLEX_INCLUDE}
        ${CMAKE_CURRENT_SOURCE_DIR}
)
        
add_executable(quack
        quack.tab.cxx lex.yy.cpp lex.yy.h
        quack.cpp 
        ASTNode.cpp ASTNode.h
        TypeChecker.cpp TypeChecker.h
        CodeGenerator.cpp CodeGenerator.h
        Messages.h Messages.cpp)

# add_executable(test_ast  test_ast.cpp ASTNode.cpp ASTNode.h CodegenContext.cpp CodegenContext.h)

target_link_libraries(quack ${REFLEX_LIB})
