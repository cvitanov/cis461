#include <stdio.h>
#include <stdlib.h>
#include "Builtins.h"

void quackmain();
int main(int argc, char** argv) {
  quackmain();
  printf("\n\n--- Waddled through successfully (quack!) ---\n");
  exit(0);
}



struct class_Pt_struct;
typedef struct class_Pt_struct* class_Pt;

typedef struct obj_Pt_struct {
class_Pt clazz;
obj_Int y;
obj_Int x;
} * obj_Pt;


struct class_Pt_struct the_class_Pt_struct;

struct class_Pt_struct {
/* Method Table */
obj_Pt (*constructor) (obj_Int, obj_Int);
obj_String (*STR) (obj_Pt);
obj_Pt (*PLUS) (obj_Pt, obj_Pt);
obj_Int (*_x) (obj_Pt);
obj_Int (*_y) (obj_Pt);
obj_Obj (*PRINT) (obj_Obj);
obj_Boolean (*EQUALS) (obj_Obj, obj_Obj);
};

extern class_Pt the_class_Pt;

obj_Pt new_Pt(obj_Int x, obj_Int y ) {
obj_Pt this = (obj_Pt)
malloc(sizeof(struct obj_Pt_struct));
this->clazz = the_class_Pt;
this->x = x;this->y = y;return this;
}

obj_String Pt_method_STR(obj_Pt this) {
return str_literal("(")->clazz->PLUS(str_literal("("),this->x->clazz->STR( (obj_Int) this->x ))->clazz->PLUS(str_literal("(")->clazz->PLUS(str_literal("("),this->x->clazz->STR( (obj_Int) this->x )),str_literal(","))->clazz->PLUS(str_literal("(")->clazz->PLUS(str_literal("("),this->x->clazz->STR( (obj_Int) this->x ))->clazz->PLUS(str_literal("(")->clazz->PLUS(str_literal("("),this->x->clazz->STR( (obj_Int) this->x )),str_literal(",")),this->y->clazz->STR( (obj_Int) this->y ))->clazz->PLUS(str_literal("(")->clazz->PLUS(str_literal("("),this->x->clazz->STR( (obj_Int) this->x ))->clazz->PLUS(str_literal("(")->clazz->PLUS(str_literal("("),this->x->clazz->STR( (obj_Int) this->x )),str_literal(","))->clazz->PLUS(str_literal("(")->clazz->PLUS(str_literal("("),this->x->clazz->STR( (obj_Int) this->x ))->clazz->PLUS(str_literal("(")->clazz->PLUS(str_literal("("),this->x->clazz->STR( (obj_Int) this->x )),str_literal(",")),this->y->clazz->STR( (obj_Int) this->y )),str_literal(")"));
return (obj_String) nothing;
};

obj_Pt Pt_method_PLUS(obj_Pt this, obj_Pt other) {
return the_class_Pt->constructor(this->x->clazz->PLUS(this->x,other->x), this->y->clazz->PLUS(this->y,other->y));
return (obj_Pt) nothing;
};

obj_Int Pt_method__x(obj_Pt this) {
return this->x;
return (obj_Int) nothing;
};

obj_Int Pt_method__y(obj_Pt this) {
return this->y;
return (obj_Int) nothing;
};

struct class_Pt_struct the_class_Pt_struct = {
new_Pt,
Pt_method_STR,
Pt_method_PLUS,
Pt_method__x,
Pt_method__y,
Obj_method_PRINT,
Obj_method_EQUALS
};

class_Pt the_class_Pt = &the_class_Pt_struct;







struct class_Rect_struct;
typedef struct class_Rect_struct* class_Rect;

typedef struct obj_Rect_struct {
class_Rect clazz;
obj_Pt ur;
obj_Pt ll;
} * obj_Rect;


struct class_Rect_struct the_class_Rect_struct;

struct class_Rect_struct {
/* Method Table */
obj_Rect (*constructor) (obj_Pt, obj_Pt);
obj_Rect (*translate) (obj_Rect, obj_Pt);
obj_String (*STR) (obj_Rect);
obj_Obj (*PRINT) (obj_Obj);
obj_Boolean (*EQUALS) (obj_Obj, obj_Obj);
};

extern class_Rect the_class_Rect;

obj_Rect new_Rect(obj_Pt ll, obj_Pt ur ) {
obj_Rect this = (obj_Rect)
malloc(sizeof(struct obj_Rect_struct));
this->clazz = the_class_Rect;
this->ll = ll;this->ur = ur;return this;
}

obj_Rect Rect_method_translate(obj_Rect this, obj_Pt delta) {
return the_class_Rect->constructor(this->ll->clazz->PLUS(this->ll,delta), this->ur->clazz->PLUS(this->ur,delta));
return (obj_Rect) nothing;
};

obj_String Rect_method_STR(obj_Rect this) {
obj_Pt ul;
obj_Pt lr;
lr = the_class_Pt->constructor(this->ur->clazz->_y( (obj_Pt) this->ur ), this->ll->clazz->_x( (obj_Pt) this->ll ));;
ul = the_class_Pt->constructor(this->ll->clazz->_x( (obj_Pt) this->ll ), this->ur->clazz->_y( (obj_Pt) this->ur ));;
return str_literal("(")->clazz->PLUS(str_literal("("),this->ll->clazz->STR( (obj_Pt) this->ll ))->clazz->PLUS(str_literal("(")->clazz->PLUS(str_literal("("),this->ll->clazz->STR( (obj_Pt) this->ll )),str_literal(", "))->clazz->PLUS(str_literal("(")->clazz->PLUS(str_literal("("),this->ll->clazz->STR( (obj_Pt) this->ll ))->clazz->PLUS(str_literal("(")->clazz->PLUS(str_literal("("),this->ll->clazz->STR( (obj_Pt) this->ll )),str_literal(", ")),ul->clazz->STR( (obj_Pt) ul ))->clazz->PLUS(str_literal("(")->clazz->PLUS(str_literal("("),this->ll->clazz->STR( (obj_Pt) this->ll ))->clazz->PLUS(str_literal("(")->clazz->PLUS(str_literal("("),this->ll->clazz->STR( (obj_Pt) this->ll )),str_literal(", "))->clazz->PLUS(str_literal("(")->clazz->PLUS(str_literal("("),this->ll->clazz->STR( (obj_Pt) this->ll ))->clazz->PLUS(str_literal("(")->clazz->PLUS(str_literal("("),this->ll->clazz->STR( (obj_Pt) this->ll )),str_literal(", ")),ul->clazz->STR( (obj_Pt) ul )),str_literal(","))->clazz->PLUS(str_literal("(")->clazz->PLUS(str_literal("("),this->ll->clazz->STR( (obj_Pt) this->ll ))->clazz->PLUS(str_literal("(")->clazz->PLUS(str_literal("("),this->ll->clazz->STR( (obj_Pt) this->ll )),str_literal(", "))->clazz->PLUS(str_literal("(")->clazz->PLUS(str_literal("("),this->ll->clazz->STR( (obj_Pt) this->ll ))->clazz->PLUS(str_literal("(")->clazz->PLUS(str_literal("("),this->ll->clazz->STR( (obj_Pt) this->ll )),str_literal(", ")),ul->clazz->STR( (obj_Pt) ul ))->clazz->PLUS(str_literal("(")->clazz->PLUS(str_literal("("),this->ll->clazz->STR( (obj_Pt) this->ll ))->clazz->PLUS(str_literal("(")->clazz->PLUS(str_literal("("),this->ll->clazz->STR( (obj_Pt) this->ll )),str_literal(", "))->clazz->PLUS(str_literal("(")->clazz->PLUS(str_literal("("),this->ll->clazz->STR( (obj_Pt) this->ll ))->clazz->PLUS(str_literal("(")->clazz->PLUS(str_literal("("),this->ll->clazz->STR( (obj_Pt) this->ll )),str_literal(", ")),ul->clazz->STR( (obj_Pt) ul )),str_literal(",")),this->ur->clazz->STR( (obj_Pt) this->ur ))->clazz->PLUS(str_literal("(")->clazz->PLUS(str_literal("("),this->ll->clazz->STR( (obj_Pt) this->ll ))->clazz->PLUS(str_literal("(")->clazz->PLUS(str_literal("("),this->ll->clazz->STR( (obj_Pt) this->ll )),str_literal(", "))->clazz->PLUS(str_literal("(")->clazz->PLUS(str_literal("("),this->ll->clazz->STR( (obj_Pt) this->ll ))->clazz->PLUS(str_literal("(")->clazz->PLUS(str_literal("("),this->ll->clazz->STR( (obj_Pt) this->ll )),str_literal(", ")),ul->clazz->STR( (obj_Pt) ul ))->clazz->PLUS(str_literal("(")->clazz->PLUS(str_literal("("),this->ll->clazz->STR( (obj_Pt) this->ll ))->clazz->PLUS(str_literal("(")->clazz->PLUS(str_literal("("),this->ll->clazz->STR( (obj_Pt) this->ll )),str_literal(", "))->clazz->PLUS(str_literal("(")->clazz->PLUS(str_literal("("),this->ll->clazz->STR( (obj_Pt) this->ll ))->clazz->PLUS(str_literal("(")->clazz->PLUS(str_literal("("),this->ll->clazz->STR( (obj_Pt) this->ll )),str_literal(", ")),ul->clazz->STR( (obj_Pt) ul )),str_literal(","))->clazz->PLUS(str_literal("(")->clazz->PLUS(str_literal("("),this->ll->clazz->STR( (obj_Pt) this->ll ))->clazz->PLUS(str_literal("(")->clazz->PLUS(str_literal("("),this->ll->clazz->STR( (obj_Pt) this->ll )),str_literal(", "))->clazz->PLUS(str_literal("(")->clazz->PLUS(str_literal("("),this->ll->clazz->STR( (obj_Pt) this->ll ))->clazz->PLUS(str_literal("(")->clazz->PLUS(str_literal("("),this->ll->clazz->STR( (obj_Pt) this->ll )),str_literal(", ")),ul->clazz->STR( (obj_Pt) ul ))->clazz->PLUS(str_literal("(")->clazz->PLUS(str_literal("("),this->ll->clazz->STR( (obj_Pt) this->ll ))->clazz->PLUS(str_literal("(")->clazz->PLUS(str_literal("("),this->ll->clazz->STR( (obj_Pt) this->ll )),str_literal(", "))->clazz->PLUS(str_literal("(")->clazz->PLUS(str_literal("("),this->ll->clazz->STR( (obj_Pt) this->ll ))->clazz->PLUS(str_literal("(")->clazz->PLUS(str_literal("("),this->ll->clazz->STR( (obj_Pt) this->ll )),str_literal(", ")),ul->clazz->STR( (obj_Pt) ul )),str_literal(",")),this->ur->clazz->STR( (obj_Pt) this->ur )),str_literal(","))->clazz->PLUS(str_literal("(")->clazz->PLUS(str_literal("("),this->ll->clazz->STR( (obj_Pt) this->ll ))->clazz->PLUS(str_literal("(")->clazz->PLUS(str_literal("("),this->ll->clazz->STR( (obj_Pt) this->ll )),str_literal(", "))->clazz->PLUS(str_literal("(")->clazz->PLUS(str_literal("("),this->ll->clazz->STR( (obj_Pt) this->ll ))->clazz->PLUS(str_literal("(")->clazz->PLUS(str_literal("("),this->ll->clazz->STR( (obj_Pt) this->ll )),str_literal(", ")),ul->clazz->STR( (obj_Pt) ul ))->clazz->PLUS(str_literal("(")->clazz->PLUS(str_literal("("),this->ll->clazz->STR( (obj_Pt) this->ll ))->clazz->PLUS(str_literal("(")->clazz->PLUS(str_literal("("),this->ll->clazz->STR( (obj_Pt) this->ll )),str_literal(", "))->clazz->PLUS(str_literal("(")->clazz->PLUS(str_literal("("),this->ll->clazz->STR( (obj_Pt) this->ll ))->clazz->PLUS(str_literal("(")->clazz->PLUS(str_literal("("),this->ll->clazz->STR( (obj_Pt) this->ll )),str_literal(", ")),ul->clazz->STR( (obj_Pt) ul )),str_literal(","))->clazz->PLUS(str_literal("(")->clazz->PLUS(str_literal("("),this->ll->clazz->STR( (obj_Pt) this->ll ))->clazz->PLUS(str_literal("(")->clazz->PLUS(str_literal("("),this->ll->clazz->STR( (obj_Pt) this->ll )),str_literal(", "))->clazz->PLUS(str_literal("(")->clazz->PLUS(str_literal("("),this->ll->clazz->STR( (obj_Pt) this->ll ))->clazz->PLUS(str_literal("(")->clazz->PLUS(str_literal("("),this->ll->clazz->STR( (obj_Pt) this->ll )),str_literal(", ")),ul->clazz->STR( (obj_Pt) ul ))->clazz->PLUS(str_literal("(")->clazz->PLUS(str_literal("("),this->ll->clazz->STR( (obj_Pt) this->ll ))->clazz->PLUS(str_literal("(")->clazz->PLUS(str_literal("("),this->ll->clazz->STR( (obj_Pt) this->ll )),str_literal(", "))->clazz->PLUS(str_literal("(")->clazz->PLUS(str_literal("("),this->ll->clazz->STR( (obj_Pt) this->ll ))->clazz->PLUS(str_literal("(")->clazz->PLUS(str_literal("("),this->ll->clazz->STR( (obj_Pt) this->ll )),str_literal(", ")),ul->clazz->STR( (obj_Pt) ul )),str_literal(",")),this->ur->clazz->STR( (obj_Pt) this->ur ))->clazz->PLUS(str_literal("(")->clazz->PLUS(str_literal("("),this->ll->clazz->STR( (obj_Pt) this->ll ))->clazz->PLUS(str_literal("(")->clazz->PLUS(str_literal("("),this->ll->clazz->STR( (obj_Pt) this->ll )),str_literal(", "))->clazz->PLUS(str_literal("(")->clazz->PLUS(str_literal("("),this->ll->clazz->STR( (obj_Pt) this->ll ))->clazz->PLUS(str_literal("(")->clazz->PLUS(str_literal("("),this->ll->clazz->STR( (obj_Pt) this->ll )),str_literal(", ")),ul->clazz->STR( (obj_Pt) ul ))->clazz->PLUS(str_literal("(")->clazz->PLUS(str_literal("("),this->ll->clazz->STR( (obj_Pt) this->ll ))->clazz->PLUS(str_literal("(")->clazz->PLUS(str_literal("("),this->ll->clazz->STR( (obj_Pt) this->ll )),str_literal(", "))->clazz->PLUS(str_literal("(")->clazz->PLUS(str_literal("("),this->ll->clazz->STR( (obj_Pt) this->ll ))->clazz->PLUS(str_literal("(")->clazz->PLUS(str_literal("("),this->ll->clazz->STR( (obj_Pt) this->ll )),str_literal(", ")),ul->clazz->STR( (obj_Pt) ul )),str_literal(","))->clazz->PLUS(str_literal("(")->clazz->PLUS(str_literal("("),this->ll->clazz->STR( (obj_Pt) this->ll ))->clazz->PLUS(str_literal("(")->clazz->PLUS(str_literal("("),this->ll->clazz->STR( (obj_Pt) this->ll )),str_literal(", "))->clazz->PLUS(str_literal("(")->clazz->PLUS(str_literal("("),this->ll->clazz->STR( (obj_Pt) this->ll ))->clazz->PLUS(str_literal("(")->clazz->PLUS(str_literal("("),this->ll->clazz->STR( (obj_Pt) this->ll )),str_literal(", ")),ul->clazz->STR( (obj_Pt) ul ))->clazz->PLUS(str_literal("(")->clazz->PLUS(str_literal("("),this->ll->clazz->STR( (obj_Pt) this->ll ))->clazz->PLUS(str_literal("(")->clazz->PLUS(str_literal("("),this->ll->clazz->STR( (obj_Pt) this->ll )),str_literal(", "))->clazz->PLUS(str_literal("(")->clazz->PLUS(str_literal("("),this->ll->clazz->STR( (obj_Pt) this->ll ))->clazz->PLUS(str_literal("(")->clazz->PLUS(str_literal("("),this->ll->clazz->STR( (obj_Pt) this->ll )),str_literal(", ")),ul->clazz->STR( (obj_Pt) ul )),str_literal(",")),this->ur->clazz->STR( (obj_Pt) this->ur )),str_literal(",")),lr->clazz->STR( (obj_Pt) lr ))->clazz->PLUS(str_literal("(")->clazz->PLUS(str_literal("("),this->ll->clazz->STR( (obj_Pt) this->ll ))->clazz->PLUS(str_literal("(")->clazz->PLUS(str_literal("("),this->ll->clazz->STR( (obj_Pt) this->ll )),str_literal(", "))->clazz->PLUS(str_literal("(")->clazz->PLUS(str_literal("("),this->ll->clazz->STR( (obj_Pt) this->ll ))->clazz->PLUS(str_literal("(")->clazz->PLUS(str_literal("("),this->ll->clazz->STR( (obj_Pt) this->ll )),str_literal(", ")),ul->clazz->STR( (obj_Pt) ul ))->clazz->PLUS(str_literal("(")->clazz->PLUS(str_literal("("),this->ll->clazz->STR( (obj_Pt) this->ll ))->clazz->PLUS(str_literal("(")->clazz->PLUS(str_literal("("),this->ll->clazz->STR( (obj_Pt) this->ll )),str_literal(", "))->clazz->PLUS(str_literal("(")->clazz->PLUS(str_literal("("),this->ll->clazz->STR( (obj_Pt) this->ll ))->clazz->PLUS(str_literal("(")->clazz->PLUS(str_literal("("),this->ll->clazz->STR( (obj_Pt) this->ll )),str_literal(", ")),ul->clazz->STR( (obj_Pt) ul )),str_literal(","))->clazz->PLUS(str_literal("(")->clazz->PLUS(str_literal("("),this->ll->clazz->STR( (obj_Pt) this->ll ))->clazz->PLUS(str_literal("(")->clazz->PLUS(str_literal("("),this->ll->clazz->STR( (obj_Pt) this->ll )),str_literal(", "))->clazz->PLUS(str_literal("(")->clazz->PLUS(str_literal("("),this->ll->clazz->STR( (obj_Pt) this->ll ))->clazz->PLUS(str_literal("(")->clazz->PLUS(str_literal("("),this->ll->clazz->STR( (obj_Pt) this->ll )),str_literal(", ")),ul->clazz->STR( (obj_Pt) ul ))->clazz->PLUS(str_literal("(")->clazz->PLUS(str_literal("("),this->ll->clazz->STR( (obj_Pt) this->ll ))->clazz->PLUS(str_literal("(")->clazz->PLUS(str_literal("("),this->ll->clazz->STR( (obj_Pt) this->ll )),str_literal(", "))->clazz->PLUS(str_literal("(")->clazz->PLUS(str_literal("("),this->ll->clazz->STR( (obj_Pt) this->ll ))->clazz->PLUS(str_literal("(")->clazz->PLUS(str_literal("("),this->ll->clazz->STR( (obj_Pt) this->ll )),str_literal(", ")),ul->clazz->STR( (obj_Pt) ul )),str_literal(",")),this->ur->clazz->STR( (obj_Pt) this->ur ))->clazz->PLUS(str_literal("(")->clazz->PLUS(str_literal("("),this->ll->clazz->STR( (obj_Pt) this->ll ))->clazz->PLUS(str_literal("(")->clazz->PLUS(str_literal("("),this->ll->clazz->STR( (obj_Pt) this->ll )),str_literal(", "))->clazz->PLUS(str_literal("(")->clazz->PLUS(str_literal("("),this->ll->clazz->STR( (obj_Pt) this->ll ))->clazz->PLUS(str_literal("(")->clazz->PLUS(str_literal("("),this->ll->clazz->STR( (obj_Pt) this->ll )),str_literal(", ")),ul->clazz->STR( (obj_Pt) ul ))->clazz->PLUS(str_literal("(")->clazz->PLUS(str_literal("("),this->ll->clazz->STR( (obj_Pt) this->ll ))->clazz->PLUS(str_literal("(")->clazz->PLUS(str_literal("("),this->ll->clazz->STR( (obj_Pt) this->ll )),str_literal(", "))->clazz->PLUS(str_literal("(")->clazz->PLUS(str_literal("("),this->ll->clazz->STR( (obj_Pt) this->ll ))->clazz->PLUS(str_literal("(")->clazz->PLUS(str_literal("("),this->ll->clazz->STR( (obj_Pt) this->ll )),str_literal(", ")),ul->clazz->STR( (obj_Pt) ul )),str_literal(","))->clazz->PLUS(str_literal("(")->clazz->PLUS(str_literal("("),this->ll->clazz->STR( (obj_Pt) this->ll ))->clazz->PLUS(str_literal("(")->clazz->PLUS(str_literal("("),this->ll->clazz->STR( (obj_Pt) this->ll )),str_literal(", "))->clazz->PLUS(str_literal("(")->clazz->PLUS(str_literal("("),this->ll->clazz->STR( (obj_Pt) this->ll ))->clazz->PLUS(str_literal("(")->clazz->PLUS(str_literal("("),this->ll->clazz->STR( (obj_Pt) this->ll )),str_literal(", ")),ul->clazz->STR( (obj_Pt) ul ))->clazz->PLUS(str_literal("(")->clazz->PLUS(str_literal("("),this->ll->clazz->STR( (obj_Pt) this->ll ))->clazz->PLUS(str_literal("(")->clazz->PLUS(str_literal("("),this->ll->clazz->STR( (obj_Pt) this->ll )),str_literal(", "))->clazz->PLUS(str_literal("(")->clazz->PLUS(str_literal("("),this->ll->clazz->STR( (obj_Pt) this->ll ))->clazz->PLUS(str_literal("(")->clazz->PLUS(str_literal("("),this->ll->clazz->STR( (obj_Pt) this->ll )),str_literal(", ")),ul->clazz->STR( (obj_Pt) ul )),str_literal(",")),this->ur->clazz->STR( (obj_Pt) this->ur )),str_literal(","))->clazz->PLUS(str_literal("(")->clazz->PLUS(str_literal("("),this->ll->clazz->STR( (obj_Pt) this->ll ))->clazz->PLUS(str_literal("(")->clazz->PLUS(str_literal("("),this->ll->clazz->STR( (obj_Pt) this->ll )),str_literal(", "))->clazz->PLUS(str_literal("(")->clazz->PLUS(str_literal("("),this->ll->clazz->STR( (obj_Pt) this->ll ))->clazz->PLUS(str_literal("(")->clazz->PLUS(str_literal("("),this->ll->clazz->STR( (obj_Pt) this->ll )),str_literal(", ")),ul->clazz->STR( (obj_Pt) ul ))->clazz->PLUS(str_literal("(")->clazz->PLUS(str_literal("("),this->ll->clazz->STR( (obj_Pt) this->ll ))->clazz->PLUS(str_literal("(")->clazz->PLUS(str_literal("("),this->ll->clazz->STR( (obj_Pt) this->ll )),str_literal(", "))->clazz->PLUS(str_literal("(")->clazz->PLUS(str_literal("("),this->ll->clazz->STR( (obj_Pt) this->ll ))->clazz->PLUS(str_literal("(")->clazz->PLUS(str_literal("("),this->ll->clazz->STR( (obj_Pt) this->ll )),str_literal(", ")),ul->clazz->STR( (obj_Pt) ul )),str_literal(","))->clazz->PLUS(str_literal("(")->clazz->PLUS(str_literal("("),this->ll->clazz->STR( (obj_Pt) this->ll ))->clazz->PLUS(str_literal("(")->clazz->PLUS(str_literal("("),this->ll->clazz->STR( (obj_Pt) this->ll )),str_literal(", "))->clazz->PLUS(str_literal("(")->clazz->PLUS(str_literal("("),this->ll->clazz->STR( (obj_Pt) this->ll ))->clazz->PLUS(str_literal("(")->clazz->PLUS(str_literal("("),this->ll->clazz->STR( (obj_Pt) this->ll )),str_literal(", ")),ul->clazz->STR( (obj_Pt) ul ))->clazz->PLUS(str_literal("(")->clazz->PLUS(str_literal("("),this->ll->clazz->STR( (obj_Pt) this->ll ))->clazz->PLUS(str_literal("(")->clazz->PLUS(str_literal("("),this->ll->clazz->STR( (obj_Pt) this->ll )),str_literal(", "))->clazz->PLUS(str_literal("(")->clazz->PLUS(str_literal("("),this->ll->clazz->STR( (obj_Pt) this->ll ))->clazz->PLUS(str_literal("(")->clazz->PLUS(str_literal("("),this->ll->clazz->STR( (obj_Pt) this->ll )),str_literal(", ")),ul->clazz->STR( (obj_Pt) ul )),str_literal(",")),this->ur->clazz->STR( (obj_Pt) this->ur ))->clazz->PLUS(str_literal("(")->clazz->PLUS(str_literal("("),this->ll->clazz->STR( (obj_Pt) this->ll ))->clazz->PLUS(str_literal("(")->clazz->PLUS(str_literal("("),this->ll->clazz->STR( (obj_Pt) this->ll )),str_literal(", "))->clazz->PLUS(str_literal("(")->clazz->PLUS(str_literal("("),this->ll->clazz->STR( (obj_Pt) this->ll ))->clazz->PLUS(str_literal("(")->clazz->PLUS(str_literal("("),this->ll->clazz->STR( (obj_Pt) this->ll )),str_literal(", ")),ul->clazz->STR( (obj_Pt) ul ))->clazz->PLUS(str_literal("(")->clazz->PLUS(str_literal("("),this->ll->clazz->STR( (obj_Pt) this->ll ))->clazz->PLUS(str_literal("(")->clazz->PLUS(str_literal("("),this->ll->clazz->STR( (obj_Pt) this->ll )),str_literal(", "))->clazz->PLUS(str_literal("(")->clazz->PLUS(str_literal("("),this->ll->clazz->STR( (obj_Pt) this->ll ))->clazz->PLUS(str_literal("(")->clazz->PLUS(str_literal("("),this->ll->clazz->STR( (obj_Pt) this->ll )),str_literal(", ")),ul->clazz->STR( (obj_Pt) ul )),str_literal(","))->clazz->PLUS(str_literal("(")->clazz->PLUS(str_literal("("),this->ll->clazz->STR( (obj_Pt) this->ll ))->clazz->PLUS(str_literal("(")->clazz->PLUS(str_literal("("),this->ll->clazz->STR( (obj_Pt) this->ll )),str_literal(", "))->clazz->PLUS(str_literal("(")->clazz->PLUS(str_literal("("),this->ll->clazz->STR( (obj_Pt) this->ll ))->clazz->PLUS(str_literal("(")->clazz->PLUS(str_literal("("),this->ll->clazz->STR( (obj_Pt) this->ll )),str_literal(", ")),ul->clazz->STR( (obj_Pt) ul ))->clazz->PLUS(str_literal("(")->clazz->PLUS(str_literal("("),this->ll->clazz->STR( (obj_Pt) this->ll ))->clazz->PLUS(str_literal("(")->clazz->PLUS(str_literal("("),this->ll->clazz->STR( (obj_Pt) this->ll )),str_literal(", "))->clazz->PLUS(str_literal("(")->clazz->PLUS(str_literal("("),this->ll->clazz->STR( (obj_Pt) this->ll ))->clazz->PLUS(str_literal("(")->clazz->PLUS(str_literal("("),this->ll->clazz->STR( (obj_Pt) this->ll )),str_literal(", ")),ul->clazz->STR( (obj_Pt) ul )),str_literal(",")),this->ur->clazz->STR( (obj_Pt) this->ur )),str_literal(",")),lr->clazz->STR( (obj_Pt) lr )),str_literal(")"));
return (obj_String) nothing;
};

struct class_Rect_struct the_class_Rect_struct = {
new_Rect,
Rect_method_translate,
Rect_method_STR,
Obj_method_PRINT,
Obj_method_EQUALS
};

class_Rect the_class_Rect = &the_class_Rect_struct;







struct class_Square_struct;
typedef struct class_Square_struct* class_Square;

typedef struct obj_Square_struct {
class_Square clazz;
obj_Pt ur;
obj_Pt ll;
} * obj_Square;


struct class_Square_struct the_class_Square_struct;

struct class_Square_struct {
/* Method Table */
obj_Square (*constructor) (obj_Pt, obj_Int);
obj_Rect (*translate) (obj_Rect, obj_Pt);
obj_String (*STR) (obj_Rect);
obj_Obj (*PRINT) (obj_Obj);
obj_Boolean (*EQUALS) (obj_Obj, obj_Obj);
};

extern class_Square the_class_Square;

obj_Square new_Square(obj_Pt ll, obj_Int side ) {
obj_Square this = (obj_Square)
malloc(sizeof(struct obj_Square_struct));
this->clazz = the_class_Square;
this->ll = ll;this->ur = the_class_Pt->constructor(this->ll->clazz->_x( (obj_Pt) this->ll )->clazz->PLUS(this->ll->clazz->_x( (obj_Pt) this->ll ),side), this->ll->clazz->_y( (obj_Pt) this->ll )->clazz->PLUS(this->ll->clazz->_y( (obj_Pt) this->ll ),side));return this;
}

struct class_Square_struct the_class_Square_struct = {
new_Square,
Rect_method_translate,
Rect_method_STR,
Obj_method_PRINT,
Obj_method_EQUALS
};

class_Square the_class_Square = &the_class_Square_struct;




void quackmain() {
  obj_Square a_square;
  a_square = the_class_Square->constructor(the_class_Pt->constructor(int_literal(3), int_literal(3)), int_literal(5));;
  a_square = (obj_Square) a_square->clazz->translate( (obj_Rect) a_square,  (obj_Pt) the_class_Pt->constructor(int_literal(2), int_literal(2)) );;
  a_square->clazz->PRINT( (obj_Obj) a_square->clazz->STR( (obj_Rect) a_square)  );
}