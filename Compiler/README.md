# Andrew Cvitanovich CIS 461

# Quack Compiler
I could not find enough time to fully implement all aspects of the compiler for code generation, but there are several demo quack programs in the "demos" folder that compile and run. The only demo that doesn't work is "bad_circular.qk" which is meant to demonstrate that my Typechecker can find circular dependencies.  

Everything is implemented for the lexer, parser, and AST. The program outputs the AST in json format in a file named ast.json. The Typechecker is nearly complete as well.  

I was not able to complete all aspects code generation phase, particularly typecase statements and certain aspects of method invocations that cause compiling to fail. But classes, methods, inheritance, math functions, boolean methods, string methods and other builtin methods all seem to work. There are a few bugs in the code generator that I couldn't resolve by the due date.   



# TO COMPILE
cmake .  
make  

# SHELL SCRIPT TO COMPILE AND RUN A QUACK PROGRAM (WARNINGS INHIBITED)
./quack.sh /path/to/quack/program

# IMPORTANT NOTE: When my quack compiler finishes code generation the resulting c code is always saved as "test.c".

# EXAMPLE USAGE:
./quack.sh demos/quack_math.qk  

# ALTERNATIVE METHOD: TO COMPILE AND RUN A QUACK PROGRAM (3 Steps)
./quack < input.qk  
gcc Builtins.c test.c -o test 
./test


# Important Files/Folders
#Compiler/ -- The main folder with my code (Using skeleton code provided to class)
#ASTNode.cpp -- The code for building the AST
#quack.lxx -- Lexer only
#parser.cpp, quack.yxx -- Parser only
#CodeGenerator.cpp -- Code Generation
#TypeChecker.cpp -- The Type Checker
#Builtins.c, Builtins.h -- Builtin classes, modified by me

