// A Bison parser, made by GNU Bison 3.1.

// Skeleton implementation for Bison LALR(1) parsers in C++

// Copyright (C) 2002-2015, 2018 Free Software Foundation, Inc.

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

// As a special exception, you may create a larger work that contains
// part or all of the Bison parser skeleton and distribute that work
// under terms of your choice, so long as that work isn't itself a
// parser generator using the skeleton or a modified version thereof
// as a parser skeleton.  Alternatively, if you modify or redistribute
// the parser skeleton itself, you may (at your option) remove this
// special exception, which will cause the skeleton and the resulting
// Bison output files to be licensed under the GNU General Public
// License without this special exception.

// This special exception was added by the Free Software Foundation in
// version 2.2 of Bison.


// First part of user declarations.

#line 37 "quack.tab.cxx" // lalr1.cc:407

# ifndef YY_NULLPTR
#  if defined __cplusplus && 201103L <= __cplusplus
#   define YY_NULLPTR nullptr
#  else
#   define YY_NULLPTR 0
#  endif
# endif

#include "quack.tab.hxx"

// User implementation prologue.

#line 51 "quack.tab.cxx" // lalr1.cc:415
// Unqualified %code blocks.
#line 20 "/home/andrew/cis461/Compiler/quack.yxx" // lalr1.cc:416

    #include "lex.yy.h"
    #undef yylex
    #define yylex lexer.yylex  /* Within bison's parse() we should invoke lexer.yylex(), not the global yylex() */
    void dump(AST::ASTNode* n);


#line 61 "quack.tab.cxx" // lalr1.cc:416


#ifndef YY_
# if defined YYENABLE_NLS && YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> // FIXME: INFRINGES ON USER NAME SPACE.
#   define YY_(msgid) dgettext ("bison-runtime", msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(msgid) msgid
# endif
#endif

// Whether we are compiled with exception support.
#ifndef YY_EXCEPTIONS
# if defined __GNUC__ && !defined __EXCEPTIONS
#  define YY_EXCEPTIONS 0
# else
#  define YY_EXCEPTIONS 1
# endif
#endif

#define YYRHSLOC(Rhs, K) ((Rhs)[K].location)
/* YYLLOC_DEFAULT -- Set CURRENT to span from RHS[1] to RHS[N].
   If N is 0, then set CURRENT to the empty location which ends
   the previous symbol: RHS[0] (always defined).  */

# ifndef YYLLOC_DEFAULT
#  define YYLLOC_DEFAULT(Current, Rhs, N)                               \
    do                                                                  \
      if (N)                                                            \
        {                                                               \
          (Current).begin  = YYRHSLOC (Rhs, 1).begin;                   \
          (Current).end    = YYRHSLOC (Rhs, N).end;                     \
        }                                                               \
      else                                                              \
        {                                                               \
          (Current).begin = (Current).end = YYRHSLOC (Rhs, 0).end;      \
        }                                                               \
    while (/*CONSTCOND*/ false)
# endif


// Suppress unused-variable warnings by "using" E.
#define YYUSE(E) ((void) (E))

// Enable debugging if requested.
#if YYDEBUG

// A pseudo ostream that takes yydebug_ into account.
# define YYCDEBUG if (yydebug_) (*yycdebug_)

# define YY_SYMBOL_PRINT(Title, Symbol)         \
  do {                                          \
    if (yydebug_)                               \
    {                                           \
      *yycdebug_ << Title << ' ';               \
      yy_print_ (*yycdebug_, Symbol);           \
      *yycdebug_ << '\n';                       \
    }                                           \
  } while (false)

# define YY_REDUCE_PRINT(Rule)          \
  do {                                  \
    if (yydebug_)                       \
      yy_reduce_print_ (Rule);          \
  } while (false)

# define YY_STACK_PRINT()               \
  do {                                  \
    if (yydebug_)                       \
      yystack_print_ ();                \
  } while (false)

#else // !YYDEBUG

# define YYCDEBUG if (false) std::cerr
# define YY_SYMBOL_PRINT(Title, Symbol)  YYUSE (Symbol)
# define YY_REDUCE_PRINT(Rule)           static_cast<void> (0)
# define YY_STACK_PRINT()                static_cast<void> (0)

#endif // !YYDEBUG

#define yyerrok         (yyerrstatus_ = 0)
#define yyclearin       (yyla.clear ())

#define YYACCEPT        goto yyacceptlab
#define YYABORT         goto yyabortlab
#define YYERROR         goto yyerrorlab
#define YYRECOVERING()  (!!yyerrstatus_)


namespace yy {
#line 156 "quack.tab.cxx" // lalr1.cc:491

  /// Build a parser object.
  parser::parser (yy::Lexer& lexer_yyarg, AST::ASTNode** root_yyarg)
    :
#if YYDEBUG
      yydebug_ (false),
      yycdebug_ (&std::cerr),
#endif
      lexer (lexer_yyarg),
      root (root_yyarg)
  {}

  parser::~parser ()
  {}


  /*---------------.
  | Symbol types.  |
  `---------------*/

  parser::syntax_error::syntax_error (const location_type& l, const std::string& m)
    : std::runtime_error (m)
    , location (l)
  {}

  // basic_symbol.
  template <typename Base>
  parser::basic_symbol<Base>::basic_symbol ()
    : value ()
    , location ()
  {}

  template <typename Base>
  parser::basic_symbol<Base>::basic_symbol (const basic_symbol& other)
    : Base (other)
    , value (other.value)
    , location (other.location)
  {
  }

  template <typename Base>
  parser::basic_symbol<Base>::basic_symbol (typename Base::kind_type t, const semantic_type& v, const location_type& l)
    : Base (t)
    , value (v)
    , location (l)
  {}


  /// Constructor for valueless symbols.
  template <typename Base>
  parser::basic_symbol<Base>::basic_symbol (typename Base::kind_type t, const location_type& l)
    : Base (t)
    , value ()
    , location (l)
  {}

  template <typename Base>
  parser::basic_symbol<Base>::~basic_symbol ()
  {
    clear ();
  }

  template <typename Base>
  void
  parser::basic_symbol<Base>::clear ()
  {
    Base::clear ();
  }

  template <typename Base>
  bool
  parser::basic_symbol<Base>::empty () const
  {
    return Base::type_get () == empty_symbol;
  }

  template <typename Base>
  void
  parser::basic_symbol<Base>::move (basic_symbol& s)
  {
    super_type::move (s);
    value = s.value;
    location = s.location;
  }

  // by_type.
  parser::by_type::by_type ()
    : type (empty_symbol)
  {}

  parser::by_type::by_type (const by_type& other)
    : type (other.type)
  {}

  parser::by_type::by_type (token_type t)
    : type (yytranslate_ (t))
  {}

  void
  parser::by_type::clear ()
  {
    type = empty_symbol;
  }

  void
  parser::by_type::move (by_type& that)
  {
    type = that.type;
    that.clear ();
  }

  int
  parser::by_type::type_get () const
  {
    return type;
  }


  // by_state.
  parser::by_state::by_state ()
    : state (empty_state)
  {}

  parser::by_state::by_state (const by_state& other)
    : state (other.state)
  {}

  void
  parser::by_state::clear ()
  {
    state = empty_state;
  }

  void
  parser::by_state::move (by_state& that)
  {
    state = that.state;
    that.clear ();
  }

  parser::by_state::by_state (state_type s)
    : state (s)
  {}

  parser::symbol_number_type
  parser::by_state::type_get () const
  {
    if (state == empty_state)
      return empty_symbol;
    else
      return yystos_[state];
  }

  parser::stack_symbol_type::stack_symbol_type ()
  {}

  parser::stack_symbol_type::stack_symbol_type (const stack_symbol_type& that)
    : super_type (that.state, that.location)
  {
    value = that.value;
  }

  parser::stack_symbol_type::stack_symbol_type (state_type s, symbol_type& that)
    : super_type (s, that.value, that.location)
  {
    // that is emptied.
    that.type = empty_symbol;
  }

  parser::stack_symbol_type&
  parser::stack_symbol_type::operator= (const stack_symbol_type& that)
  {
    state = that.state;
    value = that.value;
    location = that.location;
    return *this;
  }


  template <typename Base>
  void
  parser::yy_destroy_ (const char* yymsg, basic_symbol<Base>& yysym) const
  {
    if (yymsg)
      YY_SYMBOL_PRINT (yymsg, yysym);

    // User destructor.
    YYUSE (yysym.type_get ());
  }

#if YYDEBUG
  template <typename Base>
  void
  parser::yy_print_ (std::ostream& yyo,
                                     const basic_symbol<Base>& yysym) const
  {
    std::ostream& yyoutput = yyo;
    YYUSE (yyoutput);
    symbol_number_type yytype = yysym.type_get ();
    // Avoid a (spurious) G++ 4.8 warning about "array subscript is
    // below array bounds".
    if (yysym.empty ())
      std::abort ();
    yyo << (yytype < yyntokens_ ? "token" : "nterm")
        << ' ' << yytname_[yytype] << " ("
        << yysym.location << ": ";
    YYUSE (yytype);
    yyo << ')';
  }
#endif

  void
  parser::yypush_ (const char* m, state_type s, symbol_type& sym)
  {
    stack_symbol_type t (s, sym);
    yypush_ (m, t);
  }

  void
  parser::yypush_ (const char* m, stack_symbol_type& s)
  {
    if (m)
      YY_SYMBOL_PRINT (m, s);
    yystack_.push (s);
  }

  void
  parser::yypop_ (unsigned n)
  {
    yystack_.pop (n);
  }

#if YYDEBUG
  std::ostream&
  parser::debug_stream () const
  {
    return *yycdebug_;
  }

  void
  parser::set_debug_stream (std::ostream& o)
  {
    yycdebug_ = &o;
  }


  parser::debug_level_type
  parser::debug_level () const
  {
    return yydebug_;
  }

  void
  parser::set_debug_level (debug_level_type l)
  {
    yydebug_ = l;
  }
#endif // YYDEBUG

  parser::state_type
  parser::yy_lr_goto_state_ (state_type yystate, int yysym)
  {
    int yyr = yypgoto_[yysym - yyntokens_] + yystate;
    if (0 <= yyr && yyr <= yylast_ && yycheck_[yyr] == yystate)
      return yytable_[yyr];
    else
      return yydefgoto_[yysym - yyntokens_];
  }

  bool
  parser::yy_pact_value_is_default_ (int yyvalue)
  {
    return yyvalue == yypact_ninf_;
  }

  bool
  parser::yy_table_value_is_error_ (int yyvalue)
  {
    return yyvalue == yytable_ninf_;
  }

  int
  parser::parse ()
  {
    // State.
    int yyn;
    /// Length of the RHS of the rule being reduced.
    int yylen = 0;

    // Error handling.
    int yynerrs_ = 0;
    int yyerrstatus_ = 0;

    /// The lookahead symbol.
    symbol_type yyla;

    /// The locations where the error started and ended.
    stack_symbol_type yyerror_range[3];

    /// The return value of parse ().
    int yyresult;

#if YY_EXCEPTIONS
    try
#endif // YY_EXCEPTIONS
      {
    YYCDEBUG << "Starting parse\n";


    /* Initialize the stack.  The initial state will be set in
       yynewstate, since the latter expects the semantical and the
       location values to have been already stored, initialize these
       stacks with a primary value.  */
    yystack_.clear ();
    yypush_ (YY_NULLPTR, 0, yyla);

    // A new symbol was pushed on the stack.
  yynewstate:
    YYCDEBUG << "Entering state " << yystack_[0].state << '\n';

    // Accept?
    if (yystack_[0].state == yyfinal_)
      goto yyacceptlab;

    goto yybackup;

    // Backup.
  yybackup:
    // Try to take a decision without lookahead.
    yyn = yypact_[yystack_[0].state];
    if (yy_pact_value_is_default_ (yyn))
      goto yydefault;

    // Read a lookahead token.
    if (yyla.empty ())
      {
        YYCDEBUG << "Reading a token: ";
#if YY_EXCEPTIONS
        try
#endif // YY_EXCEPTIONS
          {
            yyla.type = yytranslate_ (yylex (&yyla.value, &yyla.location));
          }
#if YY_EXCEPTIONS
        catch (const syntax_error& yyexc)
          {
            error (yyexc);
            goto yyerrlab1;
          }
#endif // YY_EXCEPTIONS
      }
    YY_SYMBOL_PRINT ("Next token is", yyla);

    /* If the proper action on seeing token YYLA.TYPE is to reduce or
       to detect an error, take that action.  */
    yyn += yyla.type_get ();
    if (yyn < 0 || yylast_ < yyn || yycheck_[yyn] != yyla.type_get ())
      goto yydefault;

    // Reduce or error.
    yyn = yytable_[yyn];
    if (yyn <= 0)
      {
        if (yy_table_value_is_error_ (yyn))
          goto yyerrlab;
        yyn = -yyn;
        goto yyreduce;
      }

    // Count tokens shifted since error; after three, turn off error status.
    if (yyerrstatus_)
      --yyerrstatus_;

    // Shift the lookahead token.
    yypush_ ("Shifting", yyn, yyla);
    goto yynewstate;

  /*-----------------------------------------------------------.
  | yydefault -- do the default action for the current state.  |
  `-----------------------------------------------------------*/
  yydefault:
    yyn = yydefact_[yystack_[0].state];
    if (yyn == 0)
      goto yyerrlab;
    goto yyreduce;

  /*-----------------------------.
  | yyreduce -- Do a reduction.  |
  `-----------------------------*/
  yyreduce:
    yylen = yyr2_[yyn];
    {
      stack_symbol_type yylhs;
      yylhs.state = yy_lr_goto_state_ (yystack_[yylen].state, yyr1_[yyn]);
      /* If YYLEN is nonzero, implement the default value of the
         action: '$$ = $1'.  Otherwise, use the top of the stack.

         Otherwise, the following line sets YYLHS.VALUE to garbage.
         This behavior is undocumented and Bison users should not rely
         upon it.  */
      if (yylen)
        yylhs.value = yystack_[yylen - 1].value;
      else
        yylhs.value = yystack_[0].value;

      // Default location.
      {
        slice<stack_symbol_type, stack_type> slice (yystack_, yylen);
        YYLLOC_DEFAULT (yylhs.location, slice, yylen);
        yyerror_range[1].location = yylhs.location;
      }

      // Perform the reduction.
      YY_REDUCE_PRINT (yyn);
#if YY_EXCEPTIONS
      try
#endif // YY_EXCEPTIONS
        {
          switch (yyn)
            {
  case 2:
#line 87 "/home/andrew/cis461/Compiler/quack.yxx" // lalr1.cc:870
    { *root = (yystack_[0].value.programN); }
#line 580 "quack.tab.cxx" // lalr1.cc:870
    break;

  case 3:
#line 89 "/home/andrew/cis461/Compiler/quack.yxx" // lalr1.cc:870
    { (yylhs.value.programN) = new AST::Program(); }
#line 586 "quack.tab.cxx" // lalr1.cc:870
    break;

  case 4:
#line 90 "/home/andrew/cis461/Compiler/quack.yxx" // lalr1.cc:870
    { (yystack_[1].value.programN)->append((yystack_[0].value.classN)); *root = (yystack_[1].value.programN); }
#line 592 "quack.tab.cxx" // lalr1.cc:870
    break;

  case 5:
#line 91 "/home/andrew/cis461/Compiler/quack.yxx" // lalr1.cc:870
    { (yystack_[1].value.programN)->append((yystack_[0].value.nodeN)); (yylhs.value.programN) = (yystack_[1].value.programN); }
#line 598 "quack.tab.cxx" // lalr1.cc:870
    break;

  case 6:
#line 95 "/home/andrew/cis461/Compiler/quack.yxx" // lalr1.cc:870
    { (yylhs.value.identN) = new AST::Ident(std::string("Nothing") ); dump((yylhs.value.identN)); }
#line 604 "quack.tab.cxx" // lalr1.cc:870
    break;

  case 7:
#line 98 "/home/andrew/cis461/Compiler/quack.yxx" // lalr1.cc:870
    { (yylhs.value.identN) = new AST::Ident(std::string("Obj") ); dump((yylhs.value.identN)); }
#line 610 "quack.tab.cxx" // lalr1.cc:870
    break;

  case 8:
#line 101 "/home/andrew/cis461/Compiler/quack.yxx" // lalr1.cc:870
    { (yylhs.value.classN) = new AST::Class(*(yystack_[8].value.identN), *(yystack_[6].value.argblockN), *(yystack_[3].value.identN), *(yystack_[1].value.cbodyN), 1 ); }
#line 616 "quack.tab.cxx" // lalr1.cc:870
    break;

  case 9:
#line 104 "/home/andrew/cis461/Compiler/quack.yxx" // lalr1.cc:870
    { (yylhs.value.classN) = new AST::Class(*(yystack_[7].value.identN), *(yystack_[5].value.argblockN), *(yystack_[3].value.identN), *(yystack_[1].value.cbodyN), 1 ); }
#line 622 "quack.tab.cxx" // lalr1.cc:870
    break;

  case 10:
#line 106 "/home/andrew/cis461/Compiler/quack.yxx" // lalr1.cc:870
    { (yylhs.value.argblockN) = new AST::ArgBlock(); }
#line 628 "quack.tab.cxx" // lalr1.cc:870
    break;

  case 11:
#line 107 "/home/andrew/cis461/Compiler/quack.yxx" // lalr1.cc:870
    { (yystack_[4].value.argblockN)->addArg((yystack_[2].value.identN), (yystack_[0].value.identN) ); (yylhs.value.argblockN) = (yystack_[4].value.argblockN); }
#line 634 "quack.tab.cxx" // lalr1.cc:870
    break;

  case 12:
#line 108 "/home/andrew/cis461/Compiler/quack.yxx" // lalr1.cc:870
    { (yylhs.value.argblockN) = new AST::ArgBlock(); (yylhs.value.argblockN)->addArg((yystack_[2].value.identN), (yystack_[0].value.identN) ); }
#line 640 "quack.tab.cxx" // lalr1.cc:870
    break;

  case 13:
#line 111 "/home/andrew/cis461/Compiler/quack.yxx" // lalr1.cc:870
    { (yylhs.value.cbodyN) = new AST::ClassBody(); }
#line 646 "quack.tab.cxx" // lalr1.cc:870
    break;

  case 14:
#line 112 "/home/andrew/cis461/Compiler/quack.yxx" // lalr1.cc:870
    { (yystack_[1].value.cbodyN)->append((yystack_[0].value.nodeN)); (yylhs.value.cbodyN) = (yystack_[1].value.cbodyN); }
#line 652 "quack.tab.cxx" // lalr1.cc:870
    break;

  case 15:
#line 113 "/home/andrew/cis461/Compiler/quack.yxx" // lalr1.cc:870
    { (yystack_[1].value.cbodyN)->append((yystack_[0].value.methodN)); (yylhs.value.cbodyN) = (yystack_[1].value.cbodyN); }
#line 658 "quack.tab.cxx" // lalr1.cc:870
    break;

  case 16:
#line 114 "/home/andrew/cis461/Compiler/quack.yxx" // lalr1.cc:870
    { (yylhs.value.cbodyN) = new AST::ClassBody(); (yylhs.value.cbodyN)->append((yystack_[0].value.nodeN)); }
#line 664 "quack.tab.cxx" // lalr1.cc:870
    break;

  case 17:
#line 115 "/home/andrew/cis461/Compiler/quack.yxx" // lalr1.cc:870
    { (yylhs.value.cbodyN) = new AST::ClassBody(); (yylhs.value.cbodyN)->append((yystack_[0].value.methodN)); }
#line 670 "quack.tab.cxx" // lalr1.cc:870
    break;

  case 18:
#line 121 "/home/andrew/cis461/Compiler/quack.yxx" // lalr1.cc:870
    { (yylhs.value.methodN) = new AST::Method(*(yystack_[7].value.identN), *(yystack_[5].value.argblockN), *(yystack_[3].value.identN), *(yystack_[1].value.mbodyN)); }
#line 676 "quack.tab.cxx" // lalr1.cc:870
    break;

  case 19:
#line 124 "/home/andrew/cis461/Compiler/quack.yxx" // lalr1.cc:870
    { (yylhs.value.methodN) = new AST::Method(*(yystack_[8].value.identN), *(yystack_[6].value.argblockN), *(yystack_[3].value.identN), *(yystack_[1].value.mbodyN)); }
#line 682 "quack.tab.cxx" // lalr1.cc:870
    break;

  case 20:
#line 126 "/home/andrew/cis461/Compiler/quack.yxx" // lalr1.cc:870
    { (yylhs.value.mbodyN) = new AST::MethodBody(); }
#line 688 "quack.tab.cxx" // lalr1.cc:870
    break;

  case 21:
#line 127 "/home/andrew/cis461/Compiler/quack.yxx" // lalr1.cc:870
    { (yystack_[1].value.mbodyN)->append((yystack_[0].value.nodeN)); (yylhs.value.mbodyN) = (yystack_[1].value.mbodyN); }
#line 694 "quack.tab.cxx" // lalr1.cc:870
    break;

  case 22:
#line 128 "/home/andrew/cis461/Compiler/quack.yxx" // lalr1.cc:870
    { (yylhs.value.mbodyN) = new AST::MethodBody(); (yylhs.value.mbodyN)->append((yystack_[0].value.nodeN)); }
#line 700 "quack.tab.cxx" // lalr1.cc:870
    break;

  case 23:
#line 131 "/home/andrew/cis461/Compiler/quack.yxx" // lalr1.cc:870
    { (yylhs.value.blockN) = new AST::Block(); }
#line 706 "quack.tab.cxx" // lalr1.cc:870
    break;

  case 24:
#line 132 "/home/andrew/cis461/Compiler/quack.yxx" // lalr1.cc:870
    { (yystack_[1].value.blockN)->append((yystack_[0].value.nodeN)); (yylhs.value.blockN) = (yystack_[1].value.blockN); }
#line 712 "quack.tab.cxx" // lalr1.cc:870
    break;

  case 25:
#line 133 "/home/andrew/cis461/Compiler/quack.yxx" // lalr1.cc:870
    { (yylhs.value.blockN) = new AST::Block(); (yylhs.value.blockN)->append((yystack_[0].value.nodeN)); }
#line 718 "quack.tab.cxx" // lalr1.cc:870
    break;

  case 26:
#line 137 "/home/andrew/cis461/Compiler/quack.yxx" // lalr1.cc:870
    { (yylhs.value.nodeN) = (yystack_[0].value.strconstN); }
#line 724 "quack.tab.cxx" // lalr1.cc:870
    break;

  case 27:
#line 138 "/home/andrew/cis461/Compiler/quack.yxx" // lalr1.cc:870
    { (yylhs.value.nodeN) = (yystack_[0].value.intconstN); }
#line 730 "quack.tab.cxx" // lalr1.cc:870
    break;

  case 28:
#line 139 "/home/andrew/cis461/Compiler/quack.yxx" // lalr1.cc:870
    { (yylhs.value.nodeN) = (yystack_[0].value.lexprN); }
#line 736 "quack.tab.cxx" // lalr1.cc:870
    break;

  case 29:
#line 140 "/home/andrew/cis461/Compiler/quack.yxx" // lalr1.cc:870
    { (yylhs.value.nodeN) = new AST::Plus(*(yystack_[2].value.nodeN), *(yystack_[0].value.nodeN) ); dump((yylhs.value.nodeN)); }
#line 742 "quack.tab.cxx" // lalr1.cc:870
    break;

  case 30:
#line 141 "/home/andrew/cis461/Compiler/quack.yxx" // lalr1.cc:870
    { (yylhs.value.nodeN) = new AST::Minus(*(yystack_[2].value.nodeN), *(yystack_[0].value.nodeN) ); dump((yylhs.value.nodeN)); }
#line 748 "quack.tab.cxx" // lalr1.cc:870
    break;

  case 31:
#line 142 "/home/andrew/cis461/Compiler/quack.yxx" // lalr1.cc:870
    { (yylhs.value.nodeN) = new AST::Times(*(yystack_[2].value.nodeN), *(yystack_[0].value.nodeN) ); dump((yylhs.value.nodeN)); }
#line 754 "quack.tab.cxx" // lalr1.cc:870
    break;

  case 32:
#line 143 "/home/andrew/cis461/Compiler/quack.yxx" // lalr1.cc:870
    { (yylhs.value.nodeN) = new AST::Div(*(yystack_[2].value.nodeN), *(yystack_[0].value.nodeN) ); dump((yylhs.value.nodeN)); }
#line 760 "quack.tab.cxx" // lalr1.cc:870
    break;

  case 33:
#line 144 "/home/andrew/cis461/Compiler/quack.yxx" // lalr1.cc:870
    { (yylhs.value.nodeN) = new AST::Neg(*(yystack_[0].value.nodeN) ); dump((yylhs.value.nodeN)); }
#line 766 "quack.tab.cxx" // lalr1.cc:870
    break;

  case 34:
#line 145 "/home/andrew/cis461/Compiler/quack.yxx" // lalr1.cc:870
    { (yylhs.value.nodeN) = (yystack_[1].value.nodeN); }
#line 772 "quack.tab.cxx" // lalr1.cc:870
    break;

  case 35:
#line 146 "/home/andrew/cis461/Compiler/quack.yxx" // lalr1.cc:870
    { (yylhs.value.nodeN) = new AST::Eq(*(yystack_[2].value.nodeN), *(yystack_[0].value.nodeN) ); dump((yylhs.value.nodeN)); }
#line 778 "quack.tab.cxx" // lalr1.cc:870
    break;

  case 36:
#line 147 "/home/andrew/cis461/Compiler/quack.yxx" // lalr1.cc:870
    { (yylhs.value.nodeN) = new AST::AtMost(*(yystack_[2].value.nodeN), *(yystack_[0].value.nodeN) ); dump((yylhs.value.nodeN)); }
#line 784 "quack.tab.cxx" // lalr1.cc:870
    break;

  case 37:
#line 148 "/home/andrew/cis461/Compiler/quack.yxx" // lalr1.cc:870
    { (yylhs.value.nodeN) = new AST::LessThan(*(yystack_[2].value.nodeN), *(yystack_[0].value.nodeN) ); dump((yylhs.value.nodeN)); }
#line 790 "quack.tab.cxx" // lalr1.cc:870
    break;

  case 38:
#line 149 "/home/andrew/cis461/Compiler/quack.yxx" // lalr1.cc:870
    { (yylhs.value.nodeN) = new AST::AtLeast(*(yystack_[2].value.nodeN), *(yystack_[0].value.nodeN) ); dump((yylhs.value.nodeN)); }
#line 796 "quack.tab.cxx" // lalr1.cc:870
    break;

  case 39:
#line 150 "/home/andrew/cis461/Compiler/quack.yxx" // lalr1.cc:870
    { (yylhs.value.nodeN) = new AST::More(*(yystack_[2].value.nodeN), *(yystack_[0].value.nodeN) ); dump((yylhs.value.nodeN)); }
#line 802 "quack.tab.cxx" // lalr1.cc:870
    break;

  case 40:
#line 151 "/home/andrew/cis461/Compiler/quack.yxx" // lalr1.cc:870
    { (yylhs.value.nodeN) = new AST::And(*(yystack_[2].value.nodeN), *(yystack_[0].value.nodeN) ); dump((yylhs.value.nodeN)); }
#line 808 "quack.tab.cxx" // lalr1.cc:870
    break;

  case 41:
#line 152 "/home/andrew/cis461/Compiler/quack.yxx" // lalr1.cc:870
    { (yylhs.value.nodeN) = new AST::Or(*(yystack_[2].value.nodeN), *(yystack_[0].value.nodeN) ); dump((yylhs.value.nodeN)); }
#line 814 "quack.tab.cxx" // lalr1.cc:870
    break;

  case 42:
#line 153 "/home/andrew/cis461/Compiler/quack.yxx" // lalr1.cc:870
    { (yylhs.value.nodeN) = new AST::Not(*(yystack_[0].value.nodeN) ); dump((yylhs.value.nodeN)); }
#line 820 "quack.tab.cxx" // lalr1.cc:870
    break;

  case 43:
#line 154 "/home/andrew/cis461/Compiler/quack.yxx" // lalr1.cc:870
    { (yylhs.value.nodeN) = new AST::Invoke(*(yystack_[5].value.nodeN), *(yystack_[3].value.identN), *(yystack_[1].value.actualargsblockN) ); }
#line 826 "quack.tab.cxx" // lalr1.cc:870
    break;

  case 44:
#line 155 "/home/andrew/cis461/Compiler/quack.yxx" // lalr1.cc:870
    { (yylhs.value.nodeN) = new AST::Construct(*(yystack_[3].value.identN), *(yystack_[1].value.actualargsblockN) ); }
#line 832 "quack.tab.cxx" // lalr1.cc:870
    break;

  case 45:
#line 158 "/home/andrew/cis461/Compiler/quack.yxx" // lalr1.cc:870
    { (yylhs.value.nodeN) = new AST::If(*(yystack_[4].value.nodeN), *(yystack_[2].value.blockN), *(yystack_[0].value.blockN) ); }
#line 838 "quack.tab.cxx" // lalr1.cc:870
    break;

  case 46:
#line 160 "/home/andrew/cis461/Compiler/quack.yxx" // lalr1.cc:870
    { (yylhs.value.nodeN) = new AST::If(*(yystack_[7].value.nodeN), *(yystack_[5].value.blockN), *(yystack_[1].value.blockN) ); }
#line 844 "quack.tab.cxx" // lalr1.cc:870
    break;

  case 47:
#line 162 "/home/andrew/cis461/Compiler/quack.yxx" // lalr1.cc:870
    { (yylhs.value.nodeN) = new AST::If(*(yystack_[4].value.nodeN), *(yystack_[2].value.blockN), *(yystack_[0].value.nodeN) ); }
#line 850 "quack.tab.cxx" // lalr1.cc:870
    break;

  case 48:
#line 164 "/home/andrew/cis461/Compiler/quack.yxx" // lalr1.cc:870
    { (yylhs.value.nodeN) = new AST::While(*(yystack_[3].value.nodeN), *(yystack_[1].value.blockN) ); }
#line 856 "quack.tab.cxx" // lalr1.cc:870
    break;

  case 49:
#line 166 "/home/andrew/cis461/Compiler/quack.yxx" // lalr1.cc:870
    { (yylhs.value.nodeN) = new AST::Assign(*(yystack_[4].value.lexprN), *(yystack_[3].value.identN), *(yystack_[1].value.nodeN)); }
#line 862 "quack.tab.cxx" // lalr1.cc:870
    break;

  case 50:
#line 167 "/home/andrew/cis461/Compiler/quack.yxx" // lalr1.cc:870
    { (yylhs.value.nodeN) = (yystack_[1].value.nodeN); }
#line 868 "quack.tab.cxx" // lalr1.cc:870
    break;

  case 51:
#line 168 "/home/andrew/cis461/Compiler/quack.yxx" // lalr1.cc:870
    { (yylhs.value.nodeN) = new AST::ReturnVal(*(yystack_[1].value.identN) ); dump((yylhs.value.nodeN)); }
#line 874 "quack.tab.cxx" // lalr1.cc:870
    break;

  case 52:
#line 169 "/home/andrew/cis461/Compiler/quack.yxx" // lalr1.cc:870
    { (yylhs.value.nodeN) = new AST::ReturnVal(*(yystack_[1].value.nodeN) ); dump((yylhs.value.nodeN)); }
#line 880 "quack.tab.cxx" // lalr1.cc:870
    break;

  case 53:
#line 170 "/home/andrew/cis461/Compiler/quack.yxx" // lalr1.cc:870
    { (yylhs.value.nodeN) = (yystack_[0].value.typecaseN); }
#line 886 "quack.tab.cxx" // lalr1.cc:870
    break;

  case 54:
#line 173 "/home/andrew/cis461/Compiler/quack.yxx" // lalr1.cc:870
    { (yylhs.value.identN) = new AST::Ident("VOID"); }
#line 892 "quack.tab.cxx" // lalr1.cc:870
    break;

  case 55:
#line 174 "/home/andrew/cis461/Compiler/quack.yxx" // lalr1.cc:870
    { (yylhs.value.identN) = (yystack_[0].value.identN); }
#line 898 "quack.tab.cxx" // lalr1.cc:870
    break;

  case 56:
#line 176 "/home/andrew/cis461/Compiler/quack.yxx" // lalr1.cc:870
    { (yylhs.value.nodeN) = new AST::If(*(yystack_[4].value.nodeN), *(yystack_[2].value.blockN), *(yystack_[0].value.nodeN) ); }
#line 904 "quack.tab.cxx" // lalr1.cc:870
    break;

  case 57:
#line 177 "/home/andrew/cis461/Compiler/quack.yxx" // lalr1.cc:870
    { (yylhs.value.nodeN) = new AST::If(*(yystack_[7].value.nodeN), *(yystack_[5].value.blockN), *(yystack_[1].value.blockN) ); }
#line 910 "quack.tab.cxx" // lalr1.cc:870
    break;

  case 58:
#line 180 "/home/andrew/cis461/Compiler/quack.yxx" // lalr1.cc:870
    { (yylhs.value.lexprN) = (yystack_[0].value.identN); }
#line 916 "quack.tab.cxx" // lalr1.cc:870
    break;

  case 59:
#line 181 "/home/andrew/cis461/Compiler/quack.yxx" // lalr1.cc:870
    { (yylhs.value.lexprN) = new AST::Field(*(yystack_[2].value.nodeN), *(yystack_[0].value.identN) ); }
#line 922 "quack.tab.cxx" // lalr1.cc:870
    break;

  case 60:
#line 184 "/home/andrew/cis461/Compiler/quack.yxx" // lalr1.cc:870
    { (yylhs.value.actualargsblockN) = new AST::ActualArgsBlock(); }
#line 928 "quack.tab.cxx" // lalr1.cc:870
    break;

  case 61:
#line 186 "/home/andrew/cis461/Compiler/quack.yxx" // lalr1.cc:870
    { (yylhs.value.actualargsblockN) = new AST::ActualArgsBlock(); (yylhs.value.actualargsblockN)->append((yystack_[0].value.nodeN)); }
#line 934 "quack.tab.cxx" // lalr1.cc:870
    break;

  case 62:
#line 187 "/home/andrew/cis461/Compiler/quack.yxx" // lalr1.cc:870
    { (yystack_[2].value.actualargsblockN)->append((yystack_[0].value.nodeN)); }
#line 940 "quack.tab.cxx" // lalr1.cc:870
    break;

  case 63:
#line 190 "/home/andrew/cis461/Compiler/quack.yxx" // lalr1.cc:870
    { (yylhs.value.typecaseN) = new AST::Typecase(*(yystack_[3].value.nodeN), *(yystack_[1].value.typealtN) ); }
#line 946 "quack.tab.cxx" // lalr1.cc:870
    break;

  case 64:
#line 192 "/home/andrew/cis461/Compiler/quack.yxx" // lalr1.cc:870
    { new AST::Block(); }
#line 952 "quack.tab.cxx" // lalr1.cc:870
    break;

  case 65:
#line 194 "/home/andrew/cis461/Compiler/quack.yxx" // lalr1.cc:870
    { new AST::TypeAlt(); }
#line 958 "quack.tab.cxx" // lalr1.cc:870
    break;

  case 66:
#line 195 "/home/andrew/cis461/Compiler/quack.yxx" // lalr1.cc:870
    { (yylhs.value.typealtN) = new AST::TypeAlt(); (yylhs.value.typealtN)->addAlt((yystack_[5].value.identN), (yystack_[3].value.identN), (yystack_[1].value.blockN) ); }
#line 964 "quack.tab.cxx" // lalr1.cc:870
    break;

  case 67:
#line 196 "/home/andrew/cis461/Compiler/quack.yxx" // lalr1.cc:870
    { (yystack_[6].value.typealtN)->addAlt((yystack_[5].value.identN), (yystack_[3].value.identN), (yystack_[1].value.blockN) ); (yylhs.value.typealtN) = (yystack_[6].value.typealtN); }
#line 970 "quack.tab.cxx" // lalr1.cc:870
    break;

  case 68:
#line 199 "/home/andrew/cis461/Compiler/quack.yxx" // lalr1.cc:870
    { (yylhs.value.identN) = new AST::Ident(std::string((yystack_[0].value.str)) ); dump((yylhs.value.identN)); }
#line 976 "quack.tab.cxx" // lalr1.cc:870
    break;

  case 69:
#line 201 "/home/andrew/cis461/Compiler/quack.yxx" // lalr1.cc:870
    { (yylhs.value.intconstN) = new AST::IntConst((yystack_[0].value.num) ); dump((yylhs.value.intconstN)); }
#line 982 "quack.tab.cxx" // lalr1.cc:870
    break;

  case 70:
#line 203 "/home/andrew/cis461/Compiler/quack.yxx" // lalr1.cc:870
    { (yylhs.value.strconstN) = new AST::StrConst((yystack_[0].value.str) ); dump((yylhs.value.strconstN)); }
#line 988 "quack.tab.cxx" // lalr1.cc:870
    break;


#line 992 "quack.tab.cxx" // lalr1.cc:870
            default:
              break;
            }
        }
#if YY_EXCEPTIONS
      catch (const syntax_error& yyexc)
        {
          error (yyexc);
          YYERROR;
        }
#endif // YY_EXCEPTIONS
      YY_SYMBOL_PRINT ("-> $$ =", yylhs);
      yypop_ (yylen);
      yylen = 0;
      YY_STACK_PRINT ();

      // Shift the result of the reduction.
      yypush_ (YY_NULLPTR, yylhs);
    }
    goto yynewstate;

  /*--------------------------------------.
  | yyerrlab -- here on detecting error.  |
  `--------------------------------------*/
  yyerrlab:
    // If not already recovering from an error, report this error.
    if (!yyerrstatus_)
      {
        ++yynerrs_;
        error (yyla.location, yysyntax_error_ (yystack_[0].state, yyla));
      }


    yyerror_range[1].location = yyla.location;
    if (yyerrstatus_ == 3)
      {
        /* If just tried and failed to reuse lookahead token after an
           error, discard it.  */

        // Return failure if at end of input.
        if (yyla.type_get () == yyeof_)
          YYABORT;
        else if (!yyla.empty ())
          {
            yy_destroy_ ("Error: discarding", yyla);
            yyla.clear ();
          }
      }

    // Else will try to reuse lookahead token after shifting the error token.
    goto yyerrlab1;


  /*---------------------------------------------------.
  | yyerrorlab -- error raised explicitly by YYERROR.  |
  `---------------------------------------------------*/
  yyerrorlab:

    /* Pacify compilers like GCC when the user code never invokes
       YYERROR and the label yyerrorlab therefore never appears in user
       code.  */
    if (false)
      goto yyerrorlab;
    /* Do not reclaim the symbols of the rule whose action triggered
       this YYERROR.  */
    yypop_ (yylen);
    yylen = 0;
    goto yyerrlab1;

  /*-------------------------------------------------------------.
  | yyerrlab1 -- common code for both syntax error and YYERROR.  |
  `-------------------------------------------------------------*/
  yyerrlab1:
    yyerrstatus_ = 3;   // Each real token shifted decrements this.
    {
      stack_symbol_type error_token;
      for (;;)
        {
          yyn = yypact_[yystack_[0].state];
          if (!yy_pact_value_is_default_ (yyn))
            {
              yyn += yyterror_;
              if (0 <= yyn && yyn <= yylast_ && yycheck_[yyn] == yyterror_)
                {
                  yyn = yytable_[yyn];
                  if (0 < yyn)
                    break;
                }
            }

          // Pop the current state because it cannot handle the error token.
          if (yystack_.size () == 1)
            YYABORT;

          yyerror_range[1].location = yystack_[0].location;
          yy_destroy_ ("Error: popping", yystack_[0]);
          yypop_ ();
          YY_STACK_PRINT ();
        }

      yyerror_range[2].location = yyla.location;
      YYLLOC_DEFAULT (error_token.location, yyerror_range, 2);

      // Shift the error token.
      error_token.state = yyn;
      yypush_ ("Shifting", error_token);
    }
    goto yynewstate;

    // Accept.
  yyacceptlab:
    yyresult = 0;
    goto yyreturn;

    // Abort.
  yyabortlab:
    yyresult = 1;
    goto yyreturn;

  yyreturn:
    if (!yyla.empty ())
      yy_destroy_ ("Cleanup: discarding lookahead", yyla);

    /* Do not reclaim the symbols of the rule whose action triggered
       this YYABORT or YYACCEPT.  */
    yypop_ (yylen);
    while (1 < yystack_.size ())
      {
        yy_destroy_ ("Cleanup: popping", yystack_[0]);
        yypop_ ();
      }

    return yyresult;
  }
#if YY_EXCEPTIONS
    catch (...)
      {
        YYCDEBUG << "Exception caught: cleaning lookahead and stack\n";
        // Do not try to display the values of the reclaimed symbols,
        // as their printers might throw an exception.
        if (!yyla.empty ())
          yy_destroy_ (YY_NULLPTR, yyla);

        while (1 < yystack_.size ())
          {
            yy_destroy_ (YY_NULLPTR, yystack_[0]);
            yypop_ ();
          }
        throw;
      }
#endif // YY_EXCEPTIONS
  }

  void
  parser::error (const syntax_error& yyexc)
  {
    error (yyexc.location, yyexc.what ());
  }

  // Generate an error message.
  std::string
  parser::yysyntax_error_ (state_type, const symbol_type&) const
  {
    return YY_("syntax error");
  }


  const signed char parser::yypact_ninf_ = -116;

  const signed char parser::yytable_ninf_ = -55;

  const short
  parser::yypact_[] =
  {
    -116,     5,   601,  -116,    15,    15,    15,    -4,    15,    15,
    -116,  -116,  -116,    15,    15,  -116,   166,  -116,   -21,  -116,
       4,  -116,  -116,   396,  -116,   250,   -11,   197,    18,   281,
     312,    -1,   343,    15,    15,    15,    15,    15,    15,    15,
      15,    15,    15,    15,    -4,  -116,    -4,    23,    15,    -4,
    -116,  -116,    -4,   630,   630,  -116,   396,   396,   396,   396,
     396,    -1,    -1,    92,    92,   396,   396,    37,  -116,    15,
     396,   -14,   -17,    38,    13,    40,   453,  -116,   473,    15,
     228,  -116,    15,  -116,    41,    -4,    63,    -4,    -4,  -116,
    -116,    11,    30,  -116,   396,    -4,    48,    -4,    55,    46,
    -116,    56,    15,  -116,  -116,  -116,    57,   630,    58,   618,
      -4,   630,   365,   630,   485,   618,    -4,   421,  -116,  -116,
    -116,   505,   630,   517,  -116,   441,    61,  -116,  -116,  -116,
    -116,   537,  -116,  -116,    -4,    49,    32,    65,  -116,    60,
     630,    -4,    66,   549,    74,   630,  -116,   630,   569,  -116,
     581,  -116,  -116,  -116
  };

  const unsigned char
  parser::yydefact_[] =
  {
       3,     0,     2,     1,     0,     0,     6,     0,     0,     0,
      68,    69,    70,     0,     0,     4,     0,     5,    28,    53,
      58,    27,    26,    42,    28,     0,     0,     0,     0,     0,
       0,    33,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,    50,     0,     0,    60,    65,
      51,    52,    10,    23,    23,    34,    40,    41,    35,    38,
      36,    29,    30,    31,    32,    37,    39,    59,    55,     0,
      61,     0,     0,     0,     0,     0,     0,    25,     0,    60,
       0,    44,     0,    63,     0,     0,     7,     0,     0,    48,
      24,    64,     0,    49,    62,     0,     0,     0,     0,     0,
      12,     0,     0,    47,    45,    43,     0,    23,     0,    13,
       0,    23,     0,    23,     0,    13,     0,     0,    17,    16,
      11,     0,    23,     0,    66,     0,     0,     9,    15,    14,
      46,     0,    67,     8,    10,     0,     0,     0,    56,     6,
      23,     0,     0,     0,     0,    20,    57,    20,     0,    22,
       0,    18,    21,    19
  };

  const short
  parser::yypgoto_[] =
  {
    -116,  -116,  -116,   -49,  -116,  -116,   -29,    -9,  -115,   -39,
     -42,    14,    -2,  -116,   -26,   145,    35,  -116,  -116,  -116,
      -6,  -116,  -116
  };

  const short
  parser::yydefgoto_[] =
  {
      -1,     1,     2,    26,    98,    15,    74,   117,   118,   148,
      76,    16,    77,    47,   103,    18,    71,    19,   104,    72,
      20,    21,    22
  };

  const short
  parser::yytable_[] =
  {
      17,    28,   128,    10,    33,     3,    34,    35,    36,    37,
     128,    46,    78,    83,    81,   -54,    10,    82,    23,    25,
      27,     4,    29,    30,    40,    41,    50,    31,    32,   101,
     102,    48,    42,    43,    44,    10,    11,    12,    67,    13,
      68,    86,    14,    73,    87,    52,    75,    56,    57,    58,
      59,    60,    61,    62,    63,    64,    65,    66,   105,    69,
     139,    82,    70,    87,    79,   114,    84,   137,   102,   121,
      85,   123,    88,    95,    90,    97,    90,   107,   110,    96,
     131,    99,   100,    80,   109,   111,   113,   115,   134,   106,
     142,   108,   141,    70,   140,   145,    94,    33,   143,    34,
      35,    36,    37,   147,   120,   136,   125,   119,   150,   138,
     126,     0,    90,   119,    92,   129,   112,     0,     0,    90,
       0,    90,     0,   129,     0,    42,    43,    44,    75,    90,
       0,     0,     0,     0,     0,   144,     0,     0,     0,     0,
       0,    90,     0,   149,     0,   149,   152,     0,   152,    24,
      24,    24,     0,    24,    24,     0,     0,     0,    24,    24,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,    33,     0,    34,    35,    36,    37,     0,    24,    24,
      24,    24,    24,    24,    24,    24,    24,    24,    24,    38,
      39,    40,    41,    24,     0,     0,     0,     0,     0,    42,
      43,    44,    33,    45,    34,    35,    36,    37,     0,     0,
       0,     0,     0,     0,    24,     0,     0,     0,     0,     0,
      38,    39,    40,    41,    24,     0,     0,    24,     0,     0,
      42,    43,    44,    33,    51,    34,    35,    36,    37,     0,
       0,     0,     0,     0,     0,     0,     0,    24,     0,     0,
       0,    38,    39,    40,    41,    33,     0,    34,    35,    36,
      37,    42,    43,    44,     0,    93,     0,     0,     0,     0,
       0,     0,     0,    38,    39,    40,    41,     0,     0,    49,
       0,     0,     0,    42,    43,    44,    33,     0,    34,    35,
      36,    37,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,    38,    39,    40,    41,     0,     0,
      53,     0,     0,     0,    42,    43,    44,    33,     0,    34,
      35,    36,    37,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,    38,    39,    40,    41,     0,
       0,    54,     0,     0,     0,    42,    43,    44,    33,     0,
      34,    35,    36,    37,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,    38,    39,    40,    41,
      33,    55,    34,    35,    36,    37,    42,    43,    44,     0,
       0,     0,     0,     0,     0,     0,     0,     0,    38,    39,
      40,    41,     0,     0,   122,     0,     0,     0,    42,    43,
      44,    33,     0,    34,    35,    36,    37,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,    38,
      39,    40,    41,     0,     0,     0,     0,     4,     0,    42,
      43,    44,     5,     0,     6,     0,   116,     8,     9,     0,
       0,    10,    11,    12,     0,    13,     0,     4,    14,     0,
       0,   127,     5,     0,     6,     0,   116,     8,     9,     4,
       0,    10,    11,    12,     5,    13,     6,     0,    14,     8,
       9,   133,     0,    10,    11,    12,     0,    13,     0,     4,
      14,     0,     0,    89,     5,     0,     6,     0,     0,     8,
       9,     4,     0,    10,    11,    12,     5,    13,     6,     0,
      14,     8,     9,    91,     0,    10,    11,    12,     0,    13,
       0,     4,    14,     0,     0,   124,     5,     0,     6,     0,
       0,     8,     9,     4,     0,    10,    11,    12,     5,    13,
       6,     0,    14,     8,     9,   130,     0,    10,    11,    12,
       0,    13,     0,     4,    14,     0,     0,   132,     5,     0,
       6,     0,     0,     8,     9,     4,     0,    10,    11,    12,
       5,    13,     6,     0,    14,     8,     9,   135,     0,    10,
      11,    12,     0,    13,     0,     4,    14,     0,     0,   146,
       5,     0,     6,     0,     0,     8,     9,     4,     0,    10,
      11,    12,     5,    13,     6,     0,    14,     8,     9,   151,
       0,    10,    11,    12,     0,    13,     0,     4,    14,     0,
       0,   153,     5,     0,     6,     7,     0,     8,     9,     0,
       0,    10,    11,    12,     4,    13,     0,     0,    14,     5,
       0,     6,     0,   116,     8,     9,     4,     0,    10,    11,
      12,     5,    13,     6,     0,    14,     8,     9,     0,     0,
      10,    11,    12,     0,    13,     0,     0,    14
  };

  const short
  parser::yycheck_[] =
  {
       2,     7,   117,    20,     5,     0,     7,     8,     9,    10,
     125,    32,    54,    30,    28,    36,    20,    31,     4,     5,
       6,     6,     8,     9,    25,    26,    37,    13,    14,    18,
      19,    27,    33,    34,    35,    20,    21,    22,    44,    24,
      46,    28,    27,    49,    31,    27,    52,    33,    34,    35,
      36,    37,    38,    39,    40,    41,    42,    43,    28,    36,
      28,    31,    48,    31,    27,   107,    72,    18,    19,   111,
      32,   113,    32,    32,    76,    12,    78,    29,    32,    85,
     122,    87,    88,    69,    29,    29,    29,    29,    27,    95,
     139,    97,    32,    79,    29,    29,    82,     5,   140,     7,
       8,     9,    10,    29,   110,   134,   115,   109,   147,   135,
     116,    -1,   114,   115,    79,   117,   102,    -1,    -1,   121,
      -1,   123,    -1,   125,    -1,    33,    34,    35,   134,   131,
      -1,    -1,    -1,    -1,    -1,   141,    -1,    -1,    -1,    -1,
      -1,   143,    -1,   145,    -1,   147,   148,    -1,   150,     4,
       5,     6,    -1,     8,     9,    -1,    -1,    -1,    13,    14,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,     5,    -1,     7,     8,     9,    10,    -1,    33,    34,
      35,    36,    37,    38,    39,    40,    41,    42,    43,    23,
      24,    25,    26,    48,    -1,    -1,    -1,    -1,    -1,    33,
      34,    35,     5,    37,     7,     8,     9,    10,    -1,    -1,
      -1,    -1,    -1,    -1,    69,    -1,    -1,    -1,    -1,    -1,
      23,    24,    25,    26,    79,    -1,    -1,    82,    -1,    -1,
      33,    34,    35,     5,    37,     7,     8,     9,    10,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,   102,    -1,    -1,
      -1,    23,    24,    25,    26,     5,    -1,     7,     8,     9,
      10,    33,    34,    35,    -1,    37,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    23,    24,    25,    26,    -1,    -1,    29,
      -1,    -1,    -1,    33,    34,    35,     5,    -1,     7,     8,
       9,    10,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    23,    24,    25,    26,    -1,    -1,
      29,    -1,    -1,    -1,    33,    34,    35,     5,    -1,     7,
       8,     9,    10,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    23,    24,    25,    26,    -1,
      -1,    29,    -1,    -1,    -1,    33,    34,    35,     5,    -1,
       7,     8,     9,    10,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    23,    24,    25,    26,
       5,    28,     7,     8,     9,    10,    33,    34,    35,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    23,    24,
      25,    26,    -1,    -1,    29,    -1,    -1,    -1,    33,    34,
      35,     5,    -1,     7,     8,     9,    10,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    23,
      24,    25,    26,    -1,    -1,    -1,    -1,     6,    -1,    33,
      34,    35,    11,    -1,    13,    -1,    15,    16,    17,    -1,
      -1,    20,    21,    22,    -1,    24,    -1,     6,    27,    -1,
      -1,    30,    11,    -1,    13,    -1,    15,    16,    17,     6,
      -1,    20,    21,    22,    11,    24,    13,    -1,    27,    16,
      17,    30,    -1,    20,    21,    22,    -1,    24,    -1,     6,
      27,    -1,    -1,    30,    11,    -1,    13,    -1,    -1,    16,
      17,     6,    -1,    20,    21,    22,    11,    24,    13,    -1,
      27,    16,    17,    30,    -1,    20,    21,    22,    -1,    24,
      -1,     6,    27,    -1,    -1,    30,    11,    -1,    13,    -1,
      -1,    16,    17,     6,    -1,    20,    21,    22,    11,    24,
      13,    -1,    27,    16,    17,    30,    -1,    20,    21,    22,
      -1,    24,    -1,     6,    27,    -1,    -1,    30,    11,    -1,
      13,    -1,    -1,    16,    17,     6,    -1,    20,    21,    22,
      11,    24,    13,    -1,    27,    16,    17,    30,    -1,    20,
      21,    22,    -1,    24,    -1,     6,    27,    -1,    -1,    30,
      11,    -1,    13,    -1,    -1,    16,    17,     6,    -1,    20,
      21,    22,    11,    24,    13,    -1,    27,    16,    17,    30,
      -1,    20,    21,    22,    -1,    24,    -1,     6,    27,    -1,
      -1,    30,    11,    -1,    13,    14,    -1,    16,    17,    -1,
      -1,    20,    21,    22,     6,    24,    -1,    -1,    27,    11,
      -1,    13,    -1,    15,    16,    17,     6,    -1,    20,    21,
      22,    11,    24,    13,    -1,    27,    16,    17,    -1,    -1,
      20,    21,    22,    -1,    24,    -1,    -1,    27
  };

  const unsigned char
  parser::yystos_[] =
  {
       0,    39,    40,     0,     6,    11,    13,    14,    16,    17,
      20,    21,    22,    24,    27,    43,    49,    50,    53,    55,
      58,    59,    60,    49,    53,    49,    41,    49,    58,    49,
      49,    49,    49,     5,     7,     8,     9,    10,    23,    24,
      25,    26,    33,    34,    35,    37,    32,    51,    27,    29,
      37,    37,    27,    29,    29,    28,    49,    49,    49,    49,
      49,    49,    49,    49,    49,    49,    49,    58,    58,    36,
      49,    54,    57,    58,    44,    58,    48,    50,    48,    27,
      49,    28,    31,    30,    58,    32,    28,    31,    32,    30,
      50,    30,    54,    37,    49,    32,    58,    12,    42,    58,
      58,    18,    19,    52,    56,    28,    58,    29,    58,    29,
      32,    29,    49,    29,    48,    29,    15,    45,    46,    50,
      58,    48,    29,    48,    30,    45,    58,    30,    46,    50,
      30,    48,    30,    30,    27,    30,    44,    18,    52,    28,
      29,    32,    41,    48,    58,    29,    30,    29,    47,    50,
      47,    30,    50,    30
  };

  const unsigned char
  parser::yyr1_[] =
  {
       0,    38,    39,    40,    40,    40,    41,    42,    43,    43,
      44,    44,    44,    45,    45,    45,    45,    45,    46,    46,
      47,    47,    47,    48,    48,    48,    49,    49,    49,    49,
      49,    49,    49,    49,    49,    49,    49,    49,    49,    49,
      49,    49,    49,    49,    49,    50,    50,    50,    50,    50,
      50,    50,    50,    50,    51,    51,    52,    52,    53,    53,
      54,    54,    54,    55,    56,    57,    57,    57,    58,    59,
      60
  };

  const unsigned char
  parser::yyr2_[] =
  {
       0,     2,     1,     0,     2,     2,     0,     0,    10,     9,
       0,     5,     3,     0,     2,     2,     1,     1,     9,    10,
       0,     2,     1,     0,     2,     1,     1,     1,     1,     3,
       3,     3,     3,     2,     3,     3,     3,     3,     3,     3,
       3,     3,     2,     6,     4,     6,     9,     6,     5,     5,
       2,     3,     3,     1,     0,     2,     6,     9,     1,     3,
       0,     1,     3,     5,     0,     0,     6,     7,     1,     1,
       1
  };


#if YYDEBUG
  // YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
  // First, the terminals, then, starting at \a yyntokens_, nonterminals.
  const char*
  const parser::yytname_[] =
  {
  "$end", "error", "$undefined", "CONST_NUMBER", "CONST_STRING", "AND",
  "NOT", "OR", "EQUALS", "ATLEAST", "ATMOST", "TYPECASE", "EXTENDS",
  "RETURN", "CLASS", "DEF", "WHILE", "IF", "ELSE", "ELIF", "IDENT",
  "INT_LIT", "STRING_LIT", "'+'", "'-'", "'*'", "'/'", "'('", "')'", "'{'",
  "'}'", "','", "':'", "'<'", "'>'", "'.'", "'='", "';'", "$accept",
  "program", "program_block", "nothing_", "obj_", "class_", "args",
  "class_body", "method_", "method_body", "statement_block", "r_expr",
  "statement", "type_specifier", "elif_list", "l_expr", "actual_args",
  "typecase_", "empty_", "type_alternative", "ident_", "int_", "str_", YY_NULLPTR
  };


  const unsigned char
  parser::yyrline_[] =
  {
       0,    87,    87,    89,    90,    91,    95,    98,   100,   103,
     106,   107,   108,   111,   112,   113,   114,   115,   120,   123,
     126,   127,   128,   131,   132,   133,   137,   138,   139,   140,
     141,   142,   143,   144,   145,   146,   147,   148,   149,   150,
     151,   152,   153,   154,   155,   158,   160,   162,   164,   166,
     167,   168,   169,   170,   173,   174,   176,   177,   180,   181,
     184,   186,   187,   190,   192,   194,   195,   196,   199,   201,
     203
  };

  // Print the state stack on the debug stream.
  void
  parser::yystack_print_ ()
  {
    *yycdebug_ << "Stack now";
    for (stack_type::const_iterator
           i = yystack_.begin (),
           i_end = yystack_.end ();
         i != i_end; ++i)
      *yycdebug_ << ' ' << i->state;
    *yycdebug_ << '\n';
  }

  // Report on the debug stream that the rule \a yyrule is going to be reduced.
  void
  parser::yy_reduce_print_ (int yyrule)
  {
    unsigned yylno = yyrline_[yyrule];
    int yynrhs = yyr2_[yyrule];
    // Print the symbols being reduced, and their result.
    *yycdebug_ << "Reducing stack by rule " << yyrule - 1
               << " (line " << yylno << "):\n";
    // The symbols being reduced.
    for (int yyi = 0; yyi < yynrhs; yyi++)
      YY_SYMBOL_PRINT ("   $" << yyi + 1 << " =",
                       yystack_[(yynrhs) - (yyi + 1)]);
  }
#endif // YYDEBUG

  // Symbol number corresponding to token number t.
  parser::token_number_type
  parser::yytranslate_ (int t)
  {
    static
    const token_number_type
    translate_table[] =
    {
     0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
      27,    28,    25,    23,    31,    24,    35,    26,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,    32,    37,
      33,    36,    34,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,    29,     2,    30,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22
    };
    const unsigned user_token_number_max_ = 277;
    const token_number_type undef_token_ = 2;

    if (static_cast<int> (t) <= yyeof_)
      return yyeof_;
    else if (static_cast<unsigned> (t) <= user_token_number_max_)
      return translate_table[t];
    else
      return undef_token_;
  }


} // yy
#line 1522 "quack.tab.cxx" // lalr1.cc:1181
#line 205 "/home/andrew/cis461/Compiler/quack.yxx" // lalr1.cc:1182


#include "Messages.h"

void yy::parser::error(const location_type& loc, const std::string& msg)
{
  report::error_at(loc, msg);
}

void dump(AST::ASTNode* n) {
    // std::cout << "*** Building: " << n->str() << std::endl;
}
