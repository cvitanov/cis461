#ifndef CODEGEN_H
#define CODEGEN_H



#include "ASTNode.h"
#include <string>
#include <sstream>
#include <iostream>
#include <fstream>

using namespace AST;

// CodeGenerator is a subclass of NVisitor
// Visits each node and generates code from AST
class CodeGenerator : public NVisitor {

public:
	std::ofstream cfile; // output file stream
	std::vector<std::string> scope; // keep track of current scope (in a class, method, etc.)
	std::string scope_type; // CLASS, METHOD, ETC
	std::string return_type; // for method calls
	int has_return;
	int indent_level = 0;
	
	std::string indent();

	std::unordered_map<std::string, std::string> stmts; // temporary buffer for statements in methods, class body, etc.

	std::string visit(ASTNode& n, Symtab& st) override;

	std::string visit(Void&, Symtab& st) override;
	
	// the main program block (root)
	std::string visit(Program& n, Symtab& st) override;
	
	std::string visit(Block& n, Symtab& st) override;
	
	std::string visit(ClassBody& cb, Symtab& st) override;
	
	std::string visit(MethodBody& mb, Symtab& st) override;
	
	std::string visit(ArgBlock&, Symtab& st) override;
	
	std::string visit(ActualArgsBlock&, Symtab& st) override;
	
	// TODO: make sure type is indicated if new object being declared for the first time
	std::string visit(Assign& n, Symtab& st) override;
	
	std::string visit(Class& c, Symtab& st) override;
	
	std::string visit(Method& m, Symtab& st) override;
	
	std::string visit(Invoke& i, Symtab& st) override;
	
	std::string visit(If& n, Symtab& st) override;
	
	std::string visit(While& n, Symtab& st) override;
	
	std::string visit(Arg&, Symtab& st) override;
	
	std::string visit(And& n, Symtab& st) override;
	
	std::string visit(Or& n, Symtab& st) override;
	
	std::string visit(Not& n, Symtab& st) override;
	
	std::string visit(Neg& n, Symtab& st) override;
	
	std::string visit(ReturnVal& n, Symtab& st) override;
	
	std::string visit(Typecase&, Symtab& st) override;
	std::string visit(TypeAlt&, Symtab& st) override;
	
	
	std::string visit(Construct& c, Symtab& st) override;
	
	std::string visit(Ident& id, Symtab& st) override;
	
	std::string visit(Field& f, Symtab& st) override;
	
	// return the integer constant as a string
	std::string visit(IntConst& n, Symtab& st) override;
	
	// return the string constant as a string with quotes
	std::string visit(StrConst& n, Symtab& st) override;
	
	std::string visit(BinOp& b, Symtab& st) override;
	
	std::string visit(RelOp& b, Symtab& st) override;
	
};

#endif