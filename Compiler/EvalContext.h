//
// A context object for 'eval' methods.
// We need to carry a symbol table for variables that have
// been assigned values.  We could optionally carry around
// additional information, but for now just the symbols.
//

#ifndef AST_EVALCONTEXT_H
#define AST_EVALCONTEXT_H

//#include <string>
#include <unordered_map>
//#include <vector>

enum Type { Int, String, Boolean, Obj, Nothing, Error }

// EvalContext is really just a struct for passing around the
// context.  There is no attempt at information hiding here.
//
class EvalContext {
public:
    std::unordered_map<std::string, SymData*> symtab;
    explicit EvalContext() { }
};


#endif //AST_EVALCONTEXT_H
