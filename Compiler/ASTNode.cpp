// Adapted from code shared with the class


#include <string>
#include <vector>
#include "ASTNode.h"

namespace AST {

    // Abstract syntax tree.  ASTNode is abstract base class for all other nodes.

    // JSON representation of all the concrete node types.
    // This might be particularly useful if I want to do some
    // tree manipulation in Python or another language.  We'll
    // do this by emitting into a stream.

    // --- Utility functions used by node-specific json output methods


    /* Indent to a given level */
    void ASTNode::json_indent(std::ostream& out, AST_print_context& ctx) {
        if (ctx.indent_ > 0) {
            out << std::endl;
        }
        for (int i=0; i < ctx.indent_; ++i) {
            out << "    ";
        }
    }

    /* The head element looks like { "kind" : "block", */
    void ASTNode::json_head(std::string node_kind, std::ostream& out, AST_print_context& ctx) {
        json_indent(out, ctx);
        out << "{ \"kind\" : \"" << node_kind << "\", " ;
        ctx.indent();  // one level more for children
        return;
    }

    void ASTNode::json_close(std::ostream& out, AST_print_context& ctx) {
        json_indent(out, ctx);
        out << "}";
        ctx.dedent();
    }

    void ASTNode::json_child(std::string field, ASTNode& child, std::ostream& out, AST_print_context& ctx, char sep) {
        json_indent(out, ctx);
        out << "\"" << field << "\" : ";
        child.json(out, ctx);
        out << sep;
    }


    void Block::json(std::ostream& out, AST_print_context& ctx) {
        json_head("Block", out, ctx);
        // Special case for list of children, but we probably we need to generalize this
        // for other "list of X" nodes, such as parameter lists in Quack.
        out << "\"stmts_\" : [";
        auto sep = "";
        
        for (ASTNode *stmt: stmts_) {
        	if(size==0)
        		break;
            out << sep;
            stmt->json(out, ctx);
            sep = ", ";
            size--;
        }
        out << "]";
        json_close(out, ctx);
    }
    
    void Program::json(std::ostream& out, AST_print_context& ctx) {
        json_head("Program", out, ctx);
        // Special case for list of children, but we probably we need to generalize this
        // for other "list of X" nodes, such as parameter lists in Quack.
        auto sep = "";
        int size = nClasses;
        if(size) {
			out << "\"classes_\" : [";
			sep = "";
			
	
			for (Class *c: classes_) {
				if(size==0)
					break;
				out << sep;
				c->json(out, ctx);
				sep = ", ";
				size--;
			}
			out << "]";
        
        } else {
        	out << "\"classes_\" : null, ";
        }
        
        size = nStatements;
        if(size) {
			out << "\"stmts_\" : [";
			sep = "";
			for (ASTNode *stmt: stmts_) {
				if(size==0)
					break;
				out << sep;
				stmt->json(out, ctx);
				sep = ", ";
				size--;
			}
			out << "]";
        } else {
        	out << "\"stmts_\" : null ";
        }
        json_close(out, ctx);
    }
    
    void ClassBody::json(std::ostream& out, AST_print_context& ctx) {
        json_head("ClassBody", out, ctx);
        // Special case for list of children, but we probably we need to generalize this
        // for other "list of X" nodes, such as parameter lists in Quack.
        out << "\"stmts_\" : [";
        auto sep = "";
        
        for (ASTNode *stmt: stmts_) {
        	if(size==0)
        		break;
            out << sep;
            stmt->json(out, ctx);
            sep = ", ";
            size--;
        }
        out << "]";
        json_close(out, ctx);
    }
    
    void MethodBody::json(std::ostream& out, AST_print_context& ctx) {
        json_head("MethodBody", out, ctx);
        // Special case for list of children, but we probably we need to generalize this
        // for other "list of X" nodes, such as parameter lists in Quack.
        out << "\"stmts_\" : [";
        auto sep = "";
        
        for (ASTNode *stmt: stmts_) {
        	if(size==0)
        		break;
            out << sep;
            stmt->json(out, ctx);
            sep = ", ";
            size--;
        }
        out << "]";
        json_close(out, ctx);
    }
    
    void ArgBlock::json(std::ostream& out, AST_print_context& ctx) {
        json_head("Args", out, ctx);
        out << "\"stmts_\" :";
        auto sep = " { ";
        Ident* id;
		Ident* type;
    	for (int i=0; i < argCount; i++) {
    		out << sep;
    		id = idents_.at(i);
    		type = types_.at(i);
      		out << "\"Arg\" : " + id->getName() + ",$ \"Type\" : ";
    		out << type->getName();
    		sep = " } , ";
    	}
        out << " } ";
        json_close(out, ctx);
    }
    
    // TODO: fix this?
    void ActualArgsBlock::json(std::ostream& out, AST_print_context& ctx) {
        json_head("ActualArgs", out, ctx);
        // Special case for list of children, but we probably we need to generalize this
        // for other "list of X" nodes, such as parameter lists in Quack.
        if (count) {
            out << "\"actual_args_\" : ";
        	out << " [";
            auto sep = " ";
            
            int size = count;
            for (ASTNode *arg: actual_args_) {
            	if(size==0)
            		break;
                out << sep;
                arg->json(out, ctx);
                sep = ",* ";
                size--;
            }
            out << "]";
        } else {
        	out << "\"actual_args_\" : null ";
        }

        json_close(out, ctx);
    }

    void Void::json(std::ostream& out, AST_print_context& ctx) {
    	json_head("Void", out, ctx);
    	json_close(out, ctx);
    }
    
    void Assign::json(std::ostream& out, AST_print_context& ctx) {
        json_head("Assign", out, ctx);
        json_child("lexpr_", lexpr_, out, ctx);
        json_child("type_", type_, out, ctx);
        json_child("rexpr_", rexpr_, out, ctx, ' ');
        json_close(out, ctx);
     }

    void Class::json(std::ostream& out, AST_print_context& ctx) {
        json_head("Class", out, ctx);
        json_child("ident_", ident_, out, ctx);
        json_child("formal_args_", formal_args_, out, ctx);
        if(hasOptions_)
        	json_child("options_", options_, out, ctx);
        json_child("class_body_", class_body_, out, ctx, ' ');
        json_close(out, ctx);
     }

    void Method::json(std::ostream& out, AST_print_context& ctx) {
        json_head("Method", out, ctx);
        json_child("ident_", ident_, out, ctx);
        json_child("formal_args_", formal_args_, out, ctx);
        json_child("type_", type_, out, ctx);
        json_child("stmt_block_", stmt_block_, out, ctx, ' ');
        json_close(out, ctx);
     }

    void Invoke::json(std::ostream& out, AST_print_context& ctx) {
        json_head("Invoke", out, ctx);
        json_child("rexpr_", rexpr_, out, ctx);
        json_child("method_", method_, out, ctx);
        json_child("args_", args_, out, ctx, ' ');
        json_close(out, ctx);
     }
    
    void If::json(std::ostream& out, AST_print_context& ctx) {
        json_head("If", out, ctx);
        json_child("cond_", cond_, out, ctx);
        json_child("truepart_", truepart_, out, ctx);
        json_child("falsepart_", falsepart_, out, ctx, ' ');
        json_close(out, ctx);
    }
    
    void While::json(std::ostream& out, AST_print_context& ctx) {
        json_head("While", out, ctx);
        json_child("cond_", cond_, out, ctx);
        json_child("truepart_", truepart_, out, ctx, ' ');
        json_close(out, ctx);
    }
    
    void Arg::json(std::ostream& out, AST_print_context& ctx) {
        json_head("Arg", out, ctx);
        json_child("arg_", arg_, out, ctx);
        json_child("type_", type_, out, ctx, ' ');
        json_close(out, ctx);
    }
    
    void And::json(std::ostream& out, AST_print_context& ctx) {
        json_head("And", out, ctx);
        json_child("left_", left_, out, ctx);
        json_child("right_", right_, out, ctx, ' ');
        json_close(out, ctx);
    }

    void Or::json(std::ostream& out, AST_print_context& ctx) {
        json_head("Or", out, ctx);
        json_child("left_", left_, out, ctx);
        json_child("right_", right_, out, ctx, ' ');
        json_close(out, ctx);
    }

    void Not::json(std::ostream& out, AST_print_context& ctx) {
        json_head("Not", out, ctx);
        json_child("stmts_", stmts_, out, ctx, ' ');
        json_close(out, ctx);
    }
    
    void Neg::json(std::ostream& out, AST_print_context& ctx) {
        json_head("Neg", out, ctx);
        json_child("expr_", expr_, out, ctx, ' ');
        json_close(out, ctx);
     }
    
    void ReturnVal::json(std::ostream& out, AST_print_context& ctx) {
        json_head("ReturnVal", out, ctx);
        json_child("expr_", expr_, out, ctx, ' ');
        json_close(out, ctx);
     }
    
    void Typecase::json(std::ostream& out, AST_print_context& ctx) {
        json_head("Typecase", out, ctx);
        json_child("r_expr_", r_expr_, out, ctx);
        json_child("type_alts_", type_alts_, out, ctx, ' ');
        json_close(out, ctx);
     }
    
    void TypeAlt::json(std::ostream& out, AST_print_context& ctx) {
    	json_head("TypeAlt", out, ctx);
    	for (int i=0; i < altCount; i++) {
    		std::string label = idents_.at(i) + " | ";
    		label += alt_idents_.at(i);
    		Block* stmt_blk = stmt_blks_.at(i);
    		stmt_blk->json(out,ctx);
            //json_child(label, stmt_blk, out, ctx);
    	}
    	json_close(out, ctx);
     }

    void Construct::json(std::ostream& out, AST_print_context& ctx) {
        json_head("Constructor", out, ctx);
        json_child("class_", class_, out, ctx);
        json_child("args_", args_, out, ctx, ' ');
        json_close(out, ctx);
     }

    void Ident::json(std::ostream& out, AST_print_context& ctx) {
        json_head("Ident", out, ctx);
        out << "\"text_\" : \"" << text_ << "\"";
        json_close(out, ctx);
    }
    
    void Field::json(std::ostream& out, AST_print_context& ctx) {
        json_head("Field", out, ctx);
        json_child("object_", object_, out, ctx);
        json_child("field_", field_, out, ctx, ' ');
        json_close(out, ctx);
     }


    void IntConst::json(std::ostream& out, AST_print_context& ctx) {
        json_head("IntConst", out, ctx);
        out << "\"value_\" : " << value_ ;
        json_close(out, ctx);
    }

    void StrConst::json(std::ostream& out, AST_print_context& ctx) {
	    json_head("StrConst", out, ctx);
	    out << "\"text_\" : " << "\"" << text_ << "\"";
	    json_close(out, ctx);
    }

    void BinOp::json(std::ostream& out, AST_print_context& ctx) {
        json_head(opsym, out, ctx);
        json_child("left_", left_, out, ctx);
        json_child("right_", right_, out, ctx, ' ');
        json_close(out, ctx);
    }
    
    void RelOp::json(std::ostream& out, AST_print_context& ctx) {
        json_head(opsym, out, ctx);
        json_child("left_", left_, out, ctx);
        json_child("right_", right_, out, ctx, ' ');
        json_close(out, ctx);
    }

}
