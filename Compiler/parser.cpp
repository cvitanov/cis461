//
// The parser driver just glues together a parser object
// and a lexer object.
//

#include "lex.yy.h"
#include "ASTNode.h"
#include "Messages.h"

using namespace AST;

class Driver {
public:
    explicit Driver(reflex::Input in) : lexer(in), parser(new yy::parser(lexer, &root))
       { root = nullptr; }
    ~Driver() { delete parser; }
    ASTNode* parse() {
        // parser->set_debug_level(1); // 0 = no debugging, 1 = full tracing
        // std::cout << "Running parser\n";
        int result = parser->parse();
        if (result == 0 && report::ok()) {  // 0 == success, 1 == failure
            // std::cout << "Extracting result\n";
            if (root == nullptr) {
                std::cout << "But I got a null result!  How?!\n";
            }
            return root;
        } else {
            std::cout << "Parse failed, no tree\n";
            return nullptr;
        }
    }
private:
    yy::Lexer   lexer;
    yy::parser *parser;
    ASTNode *root;
};

int main()
{
    Driver driver(std::cin);
    ASTNode* root = driver.parse();
    if (root != nullptr) {
        std::cout << "Parsed!\n";
        AST_print_context context;
        root->json(std::cout, context);
        std::cout << std::endl << "That's all folks!" << std::endl;
        //std::cout << "Successfully parsed " << root->str() << std::endl;
    } else {
        std::cout << "Extracted root was nullptr" << std::endl;
    }
}
