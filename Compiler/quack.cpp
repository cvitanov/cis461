// full compiler

#include "lex.yy.h"
#include "ASTNode.h"
#include "Messages.h"
#include "TypeChecker.h"
#include "CodeGenerator.h"
#include <iostream>
#include <fstream>

using namespace AST;

class Driver {
public:
    explicit Driver(reflex::Input in) : lexer(in), parser(new yy::parser(lexer, &root))
       { root = nullptr; }
    ~Driver() { delete parser; }
    AST::ASTNode* parse() {
        // parser->set_debug_level(1); // 0 = no debugging, 1 = full tracing
        // std::cout << "Running parser\n";
        int result = parser->parse();
        if (result == 0 && report::ok()) {  // 0 == success, 1 == failure
            // std::cout << "Extracting result\n";
            if (root == nullptr) {
                std::cout << "But I got a null result!  How?!\n";
            }
            return root;
        } else {
            std::cout << "Parse failed, no tree\n";
            return nullptr;
        }
    }
private:
    yy::Lexer   lexer;
    yy::parser *parser;
    AST::ASTNode *root;
};

int main()
{
    Driver driver(std::cin);
    AST::ASTNode* root = driver.parse();
    if (root != nullptr) {
    	
    	// parse phase
        std::cout << "Parsed!\n";
        AST_print_context context;
        std::ofstream ASTFile;
        ASTFile.open("ast.json", std::ios::out | std::ios::trunc);
        root->json(ASTFile, context);
        //std::cout << std::endl << "That's all folks!" << std::endl;
        std::cout << "\nAST build complete! (JSON saved to file ast.json)" << std::endl;
        ASTFile.close();
        
        // begin Typechecking phase (uses visitor design pattern to traverse AST depth-first left-to-right)
        std::string result; // typecheck result (should be VOID if successfully completes type checking)
        TypeChecker tc; // Typechecking visitor object
        Symtab symbol_table; // symbol table (for storing attributes of classes, and other objects in the program
        result = tc.check(root,symbol_table);
        //result = root->accept(tc, symbol_table);
        if(result == VOID) {
        	std::cout << "\nTypechecking phase completed successfully\n" << std::endl;
        } else {
        	std::cout << "\nTypechecking phase could not finish checking the entire program. Aborting.\n" << std::endl;
        	return -1;
        }
        
        
        
        CodeGenerator cg;
        result = root->accept(cg, symbol_table);
        std::cout << "Code Generator Complete\n" << std::endl;
        
        std::cout << "Compiled c code saved as test.c in the working directory." << std::endl;
        return 0;
    } else {
        std::cout << "Extracted root was nullptr" << std::endl;
        return -1;
    }
}
