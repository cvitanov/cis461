// Adapted from code shared with the class

#ifndef REFLEXIVE_ASTNODE_H
#define REFLEXIVE_ASTNODE_H

#include "SymTab.h"
#include <string>
#include <sstream>
#include <vector>
#include <iostream>


class NVisitor; // node visitor base class
	
namespace AST {

	// forward declarations

	class ASTNode; // ASTNode base class
	class Void;
	class Program;
	class Block;
	class ClassBody;
	class MethodBody;
	class ArgBlock;
	class ActualArgsBlock;
	class Assign;
	class Class;
	class Method;
	class Invoke;
	class If;
	class While;
	class Arg;
	class And;
	class Or;
	class Not;
	class Neg;
	class ReturnVal;
	class Typecase;
	class TypeAlt;
	class Construct;
	class LExpr;
	class Ident;
	class Field;
	class IntConst;
	class StrConst;
	class BinOp;
	class RelOp;

}

// visitor base class (Using "Visitor Design Pattern": Walks AST Depth First Left-To-Right)
// Arguments: ASTNode, Symbol Table
// Returns: result of visiting node as string (e.g. return type of node, string for printing AST, or code gen string, etc)
class NVisitor {
public:
	virtual std::string visit(AST::ASTNode&, Symtab& ) = 0;
	virtual std::string visit(AST::Void&, Symtab& ) = 0;
	virtual std::string visit(AST::Program&, Symtab& ) = 0;
	virtual std::string visit(AST::Block&, Symtab& ) = 0;
	virtual std::string visit(AST::ClassBody& cb, Symtab& ) = 0;
	virtual std::string visit(AST::MethodBody& mb, Symtab& ) = 0;
	virtual std::string visit(AST::ArgBlock&, Symtab& ) = 0;
	virtual std::string visit(AST::ActualArgsBlock&, Symtab& ) = 0;
	virtual std::string visit(AST::Assign&, Symtab& ) = 0;
	virtual std::string visit(AST::Class&, Symtab& ) = 0;
	virtual std::string visit(AST::Method&, Symtab& ) = 0;
	virtual std::string visit(AST::Invoke&, Symtab& ) = 0;
	virtual std::string visit(AST::If&, Symtab& ) = 0;
	virtual std::string visit(AST::While&, Symtab& ) = 0;
	virtual std::string visit(AST::Arg&, Symtab& ) = 0;
	virtual std::string visit(AST::And&, Symtab& ) = 0;
	virtual std::string visit(AST::Or&, Symtab& ) = 0;
	virtual std::string visit(AST::Not&, Symtab& ) = 0;
	virtual std::string visit(AST::Neg&, Symtab& ) = 0;
	virtual std::string visit(AST::ReturnVal&, Symtab& ) = 0;
	virtual std::string visit(AST::Typecase&, Symtab& ) = 0;
	virtual std::string visit(AST::TypeAlt&, Symtab& ) = 0;
	virtual std::string visit(AST::Construct&, Symtab& ) = 0;
	virtual std::string visit(AST::Ident&, Symtab& ) = 0;
	virtual std::string visit(AST::Field&, Symtab& ) = 0;
	virtual std::string visit(AST::IntConst&, Symtab& ) = 0;
	virtual std::string visit(AST::StrConst&, Symtab& ) = 0;
	virtual std::string visit(AST::BinOp&, Symtab& ) = 0;
	virtual std::string visit(AST::RelOp&, Symtab& ) = 0;
};

namespace AST {
	
    // Abstract syntax tree.  ASTNode is abstract base class for all other nodes.

    // Json conversion and pretty-printing can pass around a print context object
    // to keep track of indentation, and possibly other things.
    class AST_print_context {
    public:
        int indent_; // Number of spaces to place on left, after each newline
        AST_print_context() : indent_{0} {};
        void indent() { ++indent_; }
        void dedent() { --indent_; }
    };

    class ASTNode {
    public:
    	int lineno_;
        virtual void json(std::ostream& out, AST_print_context& ctx) = 0;  // Json string representation
        std::string str() {
            std::stringstream ss;
            AST_print_context ctx;
            json(ss, ctx);
            return ss.str();
        }
        // for a visitor class to walk the AST and perform actions (TypeChecker, CodeGen, etc)
        virtual std::string accept(NVisitor&, Symtab&) = 0;
        virtual std::string getName() { return VOID; }
    protected:
        void json_indent(std::ostream& out, AST_print_context& ctx);
        void json_head(std::string node_kind, std::ostream& out, AST_print_context& ctx);
        void json_close(std::ostream& out, AST_print_context& ctx);
        void json_child(std::string field, ASTNode& child, std::ostream& out, AST_print_context& ctx, char sep=',');

    };

    /* A block is a sequence of statements or expressions.
     * For simplicity we'll just make it a sequence of ASTNode,
     * and leave it to the parser to build valid structures.
     */
    class Block : public ASTNode {
    public:
    	int lineno_;
        std::vector<ASTNode*> stmts_;
        int size = 0;
        explicit Block() {}
        void append(ASTNode* stmt) { stmts_.push_back(stmt); size++; }
        void json(std::ostream& out, AST_print_context& ctx) override;
        std::string accept(NVisitor& v, Symtab& st) override {
        	return v.visit(*this, st); 
        }
    };
    
    /* special block for the main program */
    class Program : public ASTNode {
    public:
    	int lineno_;
    	std::vector<Class*> classes_;
        std::vector<ASTNode*> stmts_;
        int nClasses = 0;
        int nStatements = 0;
        explicit Program() {}
        void append(Class* c) { classes_.push_back(c); nClasses++; }
        void append(ASTNode* stmt) { stmts_.push_back(stmt); nStatements++; }
        void json(std::ostream& out, AST_print_context& ctx) override;
        std::string accept(NVisitor& v, Symtab& st) override {
        	return v.visit(*this, st); 
        }
    };
    
    /* special block for the class statements */
    class ClassBody : public ASTNode {
    public:
    	int lineno_;
        std::vector<ASTNode*> stmts_;
        int size = 0;
    	explicit ClassBody() {}
        void append(ASTNode* stmt) { stmts_.push_back(stmt); size++; }
    	void json(std::ostream& out, AST_print_context& ctx) override;
        std::string accept(NVisitor& v, Symtab& st) override {
        	return v.visit(*this, st); 
        }
    };
    
    /* special block for the method statements */
    class MethodBody : public ASTNode {
    public:
    	int lineno_;
        std::vector<ASTNode*> stmts_;
        int size = 0;
    	explicit MethodBody() {}
        void append(ASTNode* stmt) { stmts_.push_back(stmt); size++; }
    	void json(std::ostream& out, AST_print_context& ctx) override;
        std::string accept(NVisitor& v, Symtab& st) override {
        	return v.visit(*this, st); 
        }
    };
    
    /* arg block is sequence of arguments */
    class ArgBlock : public ASTNode {
    public:
    	int lineno_;
        std::vector<Ident*> idents_;
        std::vector<Ident*> types_;
        int argCount = 0;
        explicit ArgBlock() {}
        void json(std::ostream& out, AST_print_context& ctx) override;
        void addArg(Ident* ident, Ident* t) {
        	idents_.push_back(ident);
        	types_.push_back(t);
        	argCount++;
        }
        int getCount() {return argCount;}
        std::string accept(NVisitor& v, Symtab& st) override {
        	return v.visit(*this, st); 
        }
     };
    
    /* actual args block is sequence of actual arguments (for constructor, invoking, etc.) */
    class ActualArgsBlock : public ASTNode {
    public:
    	int lineno_;
        std::vector<ASTNode*> actual_args_;
        int count = 0;
        explicit ActualArgsBlock() {}
        void append(ASTNode* arg) { actual_args_.push_back(arg); count++; }
        void json(std::ostream& out, AST_print_context& ctx) override;
        int getCount() {return count;}
        std::string accept(NVisitor& v, Symtab& st) override {
        	return v.visit(*this, st); 
        }
     };


    class LExpr : public ASTNode {
    public:
    };

    /* Identifiers like x and literals like 42 are the
     * leaves of the AST.
     */
    class Ident : public LExpr {
    public:
    	int lineno_;
        std::string text_;
        int is_field = 0;
    
        explicit Ident(std::string txt) : text_{txt} {}
        void json(std::ostream& out, AST_print_context& ctx) override;
        std::string getName() override { return text_; }
        void setName(std::string new_text) { text_ = new_text; }
        std::string accept(NVisitor& v, Symtab& st) override {
        	return v.visit(*this, st); 
        }

    };
    
    /* empty AST node (useful for variable argument statements in quack) */
    
    class Void : public Ident {
    public:
    	int lineno_;
    	std::string text_;
    	
        explicit Void(std::string t) : Ident(VOID) {}
        void json(std::ostream& out, AST_print_context& ctx) override;
        std::string getName() override { return text_; }
        std::string accept(NVisitor& v, Symtab& st) override {
        	return v.visit(*this, st); 
        }
    };

    class Assign : public ASTNode {
    public:
    	int lineno_;
        LExpr &lexpr_;
        Ident &type_;
        ASTNode &rexpr_;
    
        explicit Assign(LExpr &lexpr, Ident &type, ASTNode &rexpr) :
           lexpr_{lexpr}, type_{type}, rexpr_{rexpr} {}
        void json(std::ostream& out, AST_print_context& ctx) override;
        std::string accept(NVisitor& v, Symtab& st) override {
        	return v.visit(*this, st); 
        }
    };
    
    /* class declaration */
    
    class Class : public ASTNode {
    public:
    	int lineno_;
        Ident &ident_;
        ArgBlock &formal_args_;
        Ident &options_;
        ClassBody &class_body_;
        int hasOptions_;
        
        explicit Class(Ident &ident, ArgBlock &formal_args, Ident &options, ClassBody &class_body, int hasOptions) :
           ident_{ident}, formal_args_{formal_args}, options_{options}, class_body_{class_body}, hasOptions_{hasOptions} {}
        void json(std::ostream& out, AST_print_context& ctx) override;
        std::string getName() override { return ident_.getName(); }
        bool hasExtends() {
        	if(hasOptions_)
        		return true;
        	return false;
        }
        std::string getType() {
        	if(hasOptions_) {
        		return options_.getName();
        	} else {
        		return OBJ;
        	}
        }
        std::string accept(NVisitor& v, Symtab& st) override {
        	return v.visit(*this, st); 
        }
    };
    
    /* method declaration */
    
    class Method : public ASTNode {
    public:
    	int lineno_;
        Ident &ident_;
        ArgBlock &formal_args_;
        Ident &type_;
        MethodBody &stmt_block_;
        explicit Method(Ident &ident, ArgBlock &formal_args, Ident &type, MethodBody &stmt_block) :
           ident_{ident}, formal_args_{formal_args}, type_{type}, stmt_block_{stmt_block} {}
        void json(std::ostream& out, AST_print_context& ctx) override;
        std::string getName() override { return ident_.getName(); }
        std::string getType() { return type_.getName(); }
        std::string accept(NVisitor& v, Symtab& st) override {
        	return v.visit(*this, st); 
        }
    };
    
    /* method invocation */
    
    class Invoke : public ASTNode {
    public:
    	int lineno_;
        ASTNode &rexpr_;
        Ident &method_;
        ActualArgsBlock &args_;

        explicit Invoke(ASTNode &rexpr, Ident &method, ActualArgsBlock &args) :
           rexpr_{rexpr}, method_{method}, args_{args} {}
        void json(std::ostream& out, AST_print_context& ctx) override;
        std::string accept(NVisitor& v, Symtab& st) override {
        	return v.visit(*this, st); 
        }
    };

    /* if statements */
    
    class If : public ASTNode {
    public:
    	int lineno_;
        ASTNode &cond_; // The boolean expression to be evaluated
        Block &truepart_; // Execute this block if the condition is true
        ASTNode &falsepart_; // Execute this block if the condition is false

        explicit If(ASTNode &cond, Block &truepart, ASTNode &falsepart) :
            cond_{cond}, truepart_{truepart}, falsepart_{falsepart} { }
        void json(std::ostream& out, AST_print_context& ctx) override;
        std::string accept(NVisitor& v, Symtab& st) override {
        	return v.visit(*this, st); 
        }
    };
    
    /* while loops */
    
    class While : public ASTNode {
    public:
    	int lineno_;
        ASTNode &cond_; // The boolean expression to be evaluated
        Block &truepart_; // Execute this block while the condition is true

        explicit While(ASTNode &cond, Block &truepart) :
            cond_{cond}, truepart_{truepart} { };
        void json(std::ostream& out, AST_print_context& ctx) override;
        std::string accept(NVisitor& v, Symtab& st) override {
        	return v.visit(*this, st); 
        }
    };
    
    /* argument */
    class Arg : public ASTNode {
    public:
    	int lineno_;
        ASTNode &arg_;
        ASTNode &type_;
        
        explicit Arg(ASTNode &arg, ASTNode &type) :
			arg_{arg}, type_{type} { };
        void json(std::ostream& out, AST_print_context& ctx) override;
        std::string accept(NVisitor& v, Symtab& st) override {
        	return v.visit(*this, st); 
        }
    };
    
    /* boolean operations */

    class And : public ASTNode {
    public:
    	int lineno_;
        ASTNode &left_; // left side
        ASTNode &right_; // right side
    
        explicit And(ASTNode &left, ASTNode &right) :
            left_{left}, right_{right} { };
        void json(std::ostream& out, AST_print_context& ctx) override;
        std::string accept(NVisitor& v, Symtab& st) override {
        	return v.visit(*this, st); 
        }
    };

    class Or : public ASTNode {
    public:
    	int lineno_;
        ASTNode &left_; // left side
        ASTNode &right_; // right side
    
        explicit Or(ASTNode &left, ASTNode &right) :
            left_{left}, right_{right} { };
        void json(std::ostream& out, AST_print_context& ctx) override;
        std::string accept(NVisitor& v, Symtab& st) override {
        	return v.visit(*this, st); 
        }
    };
    
    class Not : public ASTNode {
    public:
    	int lineno_;

        ASTNode &stmts_; // Boolean statement/statement_list to be inverted
    
        explicit Not( ASTNode &stmts) :
            stmts_{stmts} {};
        void json(std::ostream& out, AST_print_context& ctx) override;
        std::string accept(NVisitor& v, Symtab& st) override {
        	return v.visit(*this, st); 
        }
    };
    
    /* negate expression */
    
    class Neg : public ASTNode {
    public:
    	int lineno_;

        ASTNode &expr_; // The expression to be negated
    
        explicit Neg(ASTNode &expr) :
            expr_{expr} {};
        void json(std::ostream& out, AST_print_context& ctx) override;
        std::string accept(NVisitor& v, Symtab& st) override {
        	return v.visit(*this, st); 
        }
    };
    
    /* return statements */
    
    class ReturnVal : public ASTNode {
    public:
    	int lineno_;

        ASTNode &expr_; // The expression to be returned
    
        explicit ReturnVal(ASTNode &expr) :
            expr_{expr} {};
        void json(std::ostream& out, AST_print_context& ctx) override;
        std::string accept(NVisitor& v, Symtab& st) override {
        	return v.visit(*this, st); 
        }
    };
    
    /* typecase statement */
    class Typecase : public ASTNode {
    public:
    	int lineno_;

        ASTNode &r_expr_;
        TypeAlt &type_alts_;
    
        explicit Typecase(ASTNode &r_expr, TypeAlt &type_alts) :
          r_expr_{r_expr}, type_alts_{type_alts} {}
        void json(std::ostream& out, AST_print_context& ctx) override;
        std::string accept(NVisitor& v, Symtab& st) override {
        	return v.visit(*this, st); 
        }
    };
    
    /* type alternative statement */
    class TypeAlt : public ASTNode {
    public:
    	int lineno_;

        std::vector<std::string> idents_;
        std::vector<std::string> alt_idents_;
        std::vector<Block*> stmt_blks_;
        int altCount = 0;
    
        explicit TypeAlt() {}
        void json(std::ostream& out, AST_print_context& ctx) override;
        void addAlt(Ident* ident, Ident* alt_ident, Block* s_blk) {
        	std::string id = ident->getName();
        	idents_.push_back(id);
        	std::string alt = alt_ident->getName();
        	alt_idents_.push_back(alt);
        	stmt_blks_.push_back(s_blk);
        	altCount++;
        }
        int getCount() {return altCount;}
        std::string accept(NVisitor& v, Symtab& st) override {
        	return v.visit(*this, st); 
        }
    };
    
    /* Constructor */
    class Construct : public ASTNode {
    public:
    	int lineno_;
    	Ident &class_;
    	ActualArgsBlock &args_;
    
        explicit Construct(Ident &theClass, ActualArgsBlock &args) :
        	class_{theClass}, args_{args} {}
        void json(std::ostream& out, AST_print_context& ctx) override;
        std::string accept(NVisitor& v, Symtab& st) override {
        	return v.visit(*this, st); 
        }
    };
    
    /* access a field of an object */
    
    class Field : public LExpr {
    public:

    	int lineno_;
        ASTNode &object_;
        Ident &field_;
    
        explicit Field(ASTNode &object, Ident &field) : 
        		object_{object}, field_{field} {field_.is_field = 1;}
        void json(std::ostream& out, AST_print_context& ctx) override;
        std::string accept(NVisitor& v, Symtab& st) override {
        	return v.visit(*this, st); 
        }
    };

    /* integer constants */
    
    class IntConst : public ASTNode {
    public:
    	int lineno_;
        int value_;
    
        explicit IntConst(int v) : value_{v} {}
        void json(std::ostream& out, AST_print_context& ctx) override;
        std::string accept(NVisitor& v, Symtab& st) override {
        	return v.visit(*this, st); 
        }
    };
    
    /* string constants */

    class StrConst : public ASTNode {
    public:

    	int lineno_;
	    std::string text_;
    
	    explicit StrConst(std::string txt) : text_{txt} {}
	    void json(std::ostream& out, AST_print_context& ctx) override;
        std::string accept(NVisitor& v, Symtab& st) override {
        	return v.visit(*this, st); 
        }
    };

    // Virtual base class for +, -, *, /, comparisons, etc
    class BinOp : public ASTNode {
    public:

    	int lineno_;
        std::string opsym;
        ASTNode &left_;
        ASTNode &right_;
    
        BinOp(std::string sym, ASTNode &l, ASTNode &r) :
                opsym{sym}, left_{l}, right_{r} {};
        void json(std::ostream& out, AST_print_context& ctx) override;
        std::string accept(NVisitor& v, Symtab& st) override {
        	return v.visit(*this, st); 
        }
    };


    class RelOp : public ASTNode {
    public:
    	
    	int lineno_;
        std::string opsym;
        ASTNode &left_;
        ASTNode &right_;
    	
        RelOp(std::string sym, ASTNode &l, ASTNode &r) :
                opsym{sym}, left_{l}, right_{r} {};
        void json(std::ostream& out, AST_print_context& ctx) override;
        std::string accept(NVisitor& v, Symtab& st) override {
        	return v.visit(*this, st); 
        }
    };
    
    class Plus : public BinOp {
    public:
        Plus( ASTNode &l, ASTNode &r) :
                BinOp( std::string("PLUS"),  l, r) {};
    };

    class Minus : public BinOp {
    public:
        Minus( ASTNode &l, ASTNode &r) :
                BinOp( std::string("MINUS"),  l, r) {};
    };

    class Times : public BinOp {
    public:
        Times( ASTNode &l, ASTNode &r) :
                BinOp( std::string("TIMES"),  l, r) {};
    };

    class Div : public BinOp {
    public:
        Div ( ASTNode &l, ASTNode &r) :
                BinOp( std::string("DIVIDE"),  l, r) {};
    };

    class Eq : public RelOp {
    public:
        Eq ( ASTNode &l, ASTNode &r) :
        	RelOp( std::string("EQUALS"),  l, r) {};
    };

    class AtMost : public RelOp {
    public:
        AtMost ( ASTNode &l, ASTNode &r) :
        	RelOp( std::string("ATMOST"),  l, r) {};
    };

    class LessThan : public RelOp {
    public:
        LessThan ( ASTNode &l, ASTNode &r) :
        	RelOp( std::string("LESS"),  l, r) {};
    };

    class AtLeast : public RelOp {
    public:
        AtLeast ( ASTNode &l, ASTNode &r) :
        	RelOp( std::string("ATLEAST"),  l, r) {};
    };
    
    class More : public RelOp {
    public:
        More ( ASTNode &l, ASTNode &r) :
        	RelOp( std::string("MORE"),  l, r) {};
    };


}

#endif //REFLEXIVE_ASTNODE_H
