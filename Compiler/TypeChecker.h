#ifndef TYPECHECKER_H
#define TYPECHECKER_H



#include "ASTNode.h"
#include <vector>
#include <string>
#include <sstream>
#include <iostream>
#include <exception>
#include <set>


using namespace AST;

// TypeChecker is a subclass of NVisitor
// Visits each node in AST and checks types
// Behavior: Each visit method visits an ASTNode passing in the Symbol Table
// visit methods return a string with return type information (to propagate up the tree)

class TypeChecker : public NVisitor {
	
public:
	std::vector<std::string> scope; // keep track of current scope (in a class, method, etc.)
	std::string scope_type; // CLASS, METHOD, ETC
	identData* lexpr_data; // for assignments
	methodData* parent_method; // pointer to parent method (for scope resolution)
	std::string tc_error = "Unknown error occurred.";
	std::set<std::string> predefined_identifiers {"String", "Boolean", "and", "Nothing", "Int", "true", "or", "none", "Obj", "false", "not"};
	int invoke_flag = 0;
	int error_count = 0;
	int assignment_flag = 0;
	
	std::string check(ASTNode * root, Symtab& st);
	
	// type check error messages
	void terror(std::string msg);
	
	std::string visit(ASTNode& n, Symtab& st) override;

	std::string visit(Void& n, Symtab& st) override;
	
	// type check the main program block
	// TODO: finish  
	std::string visit(Program& n, Symtab& st) override;
	
	std::string visit(Block& n, Symtab& st) override;
	
	std::string visit(ClassBody& cb, Symtab& st) override;
	
	std::string visit(MethodBody& mb, Symtab& st) override;
	
	// TODO: type check actual args against argument types in symbol table for invoked or constructed object!
	std::string visit(ActualArgsBlock&, Symtab& st) override;
	
	std::string visit(ArgBlock&, Symtab& st) override;
	
	std::string visit(Assign& a, Symtab& st) override;
	
	std::string visit(Class& c, Symtab& st) override;
	
	std::string visit(Method& m, Symtab& st) override;
	
	// TODO: Finish Invoke typecheck
	std::string visit(Invoke& i, Symtab& st) override;
	
	std::string visit(If& n, Symtab& st) override;
	
	std::string visit(While& n, Symtab& st) override;
	
	std::string visit(Arg& n, Symtab& st) override;
	
	std::string visit(And& n, Symtab& st) override;
	
	std::string visit(Or& o, Symtab& st) override;
	
	std::string visit(Not& n, Symtab& st) override;
	
	std::string visit(Neg& n, Symtab& st) override;
	
	std::string visit(ReturnVal& n, Symtab& st) override;
	
	// typecheck typecase statements, typecase gets added to parent class data in symtab
	// returns void or fail if typecheck fails
	std::string visit(Typecase& r, Symtab& st) override;
	
	std::string visit(TypeAlt& ta, Symtab& st) override;
	
	// type check constructor ... returns class name as type
	std::string visit(Construct& c, Symtab& st) override;
	
	// type check identifier. returns type if identifier exists in symbol table
	std::string visit(Ident& i, Symtab& st) override;
	
	// type check field returns type of field if it is a valid field, errors otherwise
	std::string visit(Field& f, Symtab& st) override;
	
	// Typecheck IntConst returns INT
	std::string visit(IntConst& i, Symtab& st) override;
	
	// Typecheck StrConst returns STR
	std::string visit(StrConst& s, Symtab& st) override;
	
	// Typecheck BinOp returns the type of left hand expression
	std::string visit(BinOp& n, Symtab& st) override;
	
	std::string visit(RelOp& n, Symtab& st) override;
	
};



#endif